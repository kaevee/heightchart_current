package com.dole.heightchart;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Session.NewPermissionsRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DoleCoinFragment extends Fragment {
	
	private static final String TAG = DoleCoinFragment.class.getName();
	private RequestAuthSessionStatusCallback mSessionCallback = new RequestAuthSessionStatusCallback();
	
	private static final List<String> mFriendsPermission = new ArrayList<String>() {
        {
            add("user_friends");
            add("public_profile");
        }
    };

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view =  inflater.inflate(R.layout.dole_coin_main, null);
		
		return view;
	}

	@Override
	public void onViewCreated(View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		TextView coinNum = (TextView)view.findViewById(R.id.dole_coin_number);
		ImageView coinBefor = (ImageView)view.findViewById(R.id.dole_coin_before_login);
		Button facebookInviteButton = (Button) view.findViewById(R.id.dole_coin_invite_facebook);
		
		int userNo = HeightChartPreference.getInt(Constants.PREF_USER_NO, 0);
		if(userNo > 0) {
			int doleCoin = HeightChartPreference.getInt(Constants.PREF_LAST_DOLE_COIN, 0); 
			coinBefor.setVisibility(View.GONE);
			String coinText = String.valueOf(doleCoin);
			DLog.e(TAG, "coinText:"+coinText);
			int len = coinText.length();
			int textSize;
			DLog.e(TAG, "len:"+len);
			switch(len) {
			case 1:
			case 2:
				textSize = getResources().getDimensionPixelSize(R.dimen.dole_coin_size_length_1_2);
				break;
			case 3:
				textSize = getResources().getDimensionPixelSize(R.dimen.dole_coin_size_length_3);
				break;
			case 4:
				textSize = getResources().getDimensionPixelSize(R.dimen.dole_coin_size_length_4);
				break;
			case 5:
			default:
				textSize = getResources().getDimensionPixelSize(R.dimen.dole_coin_size_length_5);
				break;
			}
			DLog.e(TAG, "textSize:"+textSize);
			coinNum.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
			coinNum.setText(coinText);
		}
		else {
			coinNum.setVisibility(View.GONE);
		}
		
		facebookInviteButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				Session session = Session.getActiveSession();
				DLog.e(TAG, "session:"+session);
				if (session == null) {
					if (savedInstanceState != null) {
						DLog.e(TAG, "savedInstanceState:"+savedInstanceState);
						session = Session.restoreSession(getActivity(), null, mSessionCallback, savedInstanceState);
					}
					if (session == null) {
						session = new Session(getActivity());
					}
					Session.setActiveSession(session);
				}
				
				if(ensureOpenSession()) {
					requestFriendsList(session);
				}
			}
		});
	}
	
	private boolean ensureOpenSession() {
		DLog.e(TAG, "ensureOpenSession:");
        if (Session.getActiveSession() == null ||
                !Session.getActiveSession().isOpened()) {
            Session.openActiveSession(
                    getActivity(), DoleCoinFragment.this, 
                    true, 
                    Arrays.asList("user_friends"),
                    mSessionCallback);
            return false;
        }
        return true;
    }
	
	private class RequestAuthSessionStatusCallback implements Session.StatusCallback {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
        	DLog.d(TAG, " - session state = " + state);
        	
        	onSessionStateChanged(session, state, exception);
        	
        	if(state.isOpened() && sessionHasNecessaryPerms(session))
        		session.removeCallback(this);
        }
    }
	
	private boolean sessionHasNecessaryPerms(Session session) {
        if (session != null && session.getPermissions() != null) {
            for (String requestedPerm : mFriendsPermission) {
                if (!session.getPermissions().contains(requestedPerm)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
	
	private List<String> getMissingPermissions(Session session) {
        List<String> missingPerms = new ArrayList<String>(mFriendsPermission);
        if (session != null && session.getPermissions() != null) {
            for (String requestedPerm : mFriendsPermission) {
                if (session.getPermissions().contains(requestedPerm)) {
                    missingPerms.remove(requestedPerm);
                }
            }
        }
        return missingPerms;
    }
	
	private void onSessionStateChanged(Session session, SessionState state, Exception exception) {
		 if (state.isOpened() && !sessionHasNecessaryPerms(session)) {
			 //Session.openActiveSessionFromCache(getActivity());
			 session.requestNewReadPermissions(
                     new NewPermissionsRequest(
                    		 DoleCoinFragment.this, 
                             getMissingPermissions(session)));
		 } else if(state.isOpened()){
			 requestFriendsList(session);
		 }
	}
	
	private void requestFriendsList(final Session session) {
		 Util.replaceFragment(getFragmentManager(), new InviteFacebookFragment(), 
				 R.id.container, Constants.TAG_FRAGMENT_ABOUT, null, true);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		DLog.d(TAG, " requestCode = " + requestCode + " resultCode = " + resultCode + " data = " + data);
		switch (requestCode) {
		case Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE:
			DLog.d(TAG, " Return from facebook login activity");
            Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);
            break;
		default:
			break;
		}
	}
}
