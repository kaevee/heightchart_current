package com.dole.heightchart;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class ChartActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chart_main);
		
		final Bundle args = getIntent().getExtras();
		Util.replaceFragment(getFragmentManager(), new ChartFragment(), R.id.container, Constants.TAG_FRAGMENT_CHART, args, true);
	}

	@Override
	public void onBackPressed() {
		//super.onBackPressed();
		finish();
	}
	
	public void backPress(View view) {
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
	    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		onBackPressed();
	}
}
