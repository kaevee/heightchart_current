package com.dole.heightchart;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Toast;

import com.dole.heightchart.server.RequestChargeCoin;
import com.dole.heightchart.server.RequestNewAuthKey;
import com.dole.heightchart.server.ServerTask;
import com.dole.heightchart.server.ServerTask.IServerResponseCallback;
import com.dole.heightchart.ui.ChartCloudBg;
import com.dole.heightchart.ui.ChartView;
import com.dole.heightchart.ui.CustomDialog;
import com.facebook.FacebookRequestError;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.NewPermissionsRequest;
import com.facebook.SessionState;
import com.facebook.internal.Utility;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class ChartFragment extends Fragment implements IServerResponseCallback {
	
	private static final String TAG = ChartFragment.class.getName();
	
	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	
	private NickNameInfo mNickNameInfo;
	private ProgressDialog mProgressDialog;
	private Bitmap mChartShareBitmap;
	
	RequestStatusCallback mSessionCallback = new RequestStatusCallback();
	
	private class RequestStatusCallback implements Session.StatusCallback {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
        	if (state.isOpened() && !sessionHasNecessaryPerms(session)) {
        		DLog.d(TAG, " - session state = " + state + " req new publish");
        		session.requestNewPublishPermissions(new NewPermissionsRequest(ChartFragment.this, PERMISSIONS));
        	} else if(state.isOpened()){
        		DLog.d(TAG, " - session state = " + state + " not req new publish");
        		if(mChartShareBitmap != null)
        			publishPicture(session, mChartShareBitmap);
        	}
        }
    }
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.chart, null);
	}
	
	@Override
	public void onViewCreated(View view, final Bundle savedInstanceState) {

		super.onViewCreated(view, savedInstanceState);
		
		final ChartCloudBg clougBg = (ChartCloudBg) view.findViewById(R.id.chart_cloud_container);
		clougBg.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
			
			@Override
			public boolean onPreDraw() {
				clougBg.animationStart();
				clougBg.getViewTreeObserver().removeOnPreDrawListener(this);
				return true;
			}
		});
		
		View shareView = view.findViewById(R.id.chart_btn_share);
		shareView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Util.showDialogFragment(
						getFragmentManager(), 
						new ShareDialog(), 
						Constants.TAG_DIALOG_SHARE, 
						null, 
						ChartFragment.this, 
						Constants.REQUEST_CONFIRM_SHARE);
			}
		});

		final Bundle args = getArguments();
		if(args == null)
			return;
		
		final long nickId = args.getLong(Constants.NICKNAME_ID);
		mNickNameInfo = Util.getNickNameInfoById(getActivity(), nickId);
		
		
		final ChartView chartBg = (ChartView) getView().findViewById(R.id.chart_bg);
		Collections.sort(mNickNameInfo.getHeightList(), Collections.reverseOrder(mHeightInfoDateComparator));
		chartBg.initChart(mNickNameInfo.getHeightList());
		
		CheckBox avgCheck = (CheckBox) view.findViewById(R.id.chart_btn_show_standart_height);
		avgCheck.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				chartBg.setShowAverageHeight(isChecked);
				int textRes;
				if(isChecked)
					textRes = R.string.show_average_height;
				else
					textRes = R.string.not_show_average_height;
				Toast.makeText(getActivity(), getResources().getString(textRes), Toast.LENGTH_SHORT).show();
			}
		});
	}
	
	private Comparator<HeightInfo> mHeightInfoDateComparator = new Comparator<HeightInfo>() {
		
		@Override
		public int compare(HeightInfo object1, HeightInfo object2) {
			return object2.mInputDate.compareTo(object1.mInputDate);
		}
	};
	
	static int[] sChartIDs = {
		R.id.chart_cloud_container,
		R.id.chart_bottom_grass,
		R.id.chart_bottom_grass_upper
	};
	
	//TODO
	public Bitmap getChartBitmap() {
		final ViewGroup baseView = (ViewGroup) getView();
		final ChartView chartView = (ChartView) baseView.findViewById(R.id.chart_bg);
		final View chartContainer = baseView.findViewById(R.id.chart_view_container);
		int imageWidth = Math.max(baseView.getWidth(), chartView.getChartWidth());
		int imageHeight = baseView.getHeight();

		final Bitmap bitmap = Bitmap.createBitmap(imageWidth, imageHeight, Bitmap.Config.ARGB_8888);
		final Canvas canvas = new Canvas();
		canvas.setBitmap(bitmap);

		final Drawable bg = getResources().getDrawable(R.drawable.sky_bg);

		// Draw bg
		int bgDrawLoop = (int) Math.ceil((float)imageWidth / (float)baseView.getWidth());
		for(int i = 0; i < bgDrawLoop; i++) {
			int offset = i * imageWidth;
			canvas.save();
			canvas.translate(-offset, 0);
			bg.setBounds(0, 0, imageWidth, imageHeight);
			bg.draw(canvas);
			
			for(int id : sChartIDs) {
				if(id == R.id.chart_title_mini && i != 0)
					continue;
				
				final View childView = baseView.findViewById(id);
				if(childView != null) {
					int childSave = canvas.save();	
					canvas.translate(childView.getLeft(), childView.getTop());
					childView.draw(canvas);
					canvas.restoreToCount(childSave);
				}
			}
			canvas.restore();
		}
		
		canvas.save();
		canvas.translate(chartContainer.getLeft(), chartContainer.getTop());
		chartView.drawChart(canvas);
		canvas.restore();
		
		Drawable titleImage = getResources().getDrawable(R.drawable.height_viewer_height_chart_logo);

		int titleW = getResources().getDimensionPixelSize(R.dimen.chart_title_print_width);
		int titleH = getResources().getDimensionPixelSize(R.dimen.chart_title_print_height);
		int titleR = getResources().getDimensionPixelSize(R.dimen.chart_title_print_right);
		int titleT = getResources().getDimensionPixelSize(R.dimen.chart_title_print_top);
		titleImage.setBounds(0, 0, titleW, titleH);
		canvas.save();
		canvas.translate(imageWidth - titleR - titleW, titleT);
		titleImage.draw(canvas);
		canvas.restore();
		
		Drawable logoImage = getResources().getDrawable(R.drawable.title_height_dole_logo);
		int logoW = getResources().getDimensionPixelSize(R.dimen.chart_logo_print_width);
		int logoH = getResources().getDimensionPixelSize(R.dimen.chart_logo_print_height);
		logoImage.setBounds(0, 0, logoW, logoH);
		canvas.save();
		logoImage.draw(canvas);
		canvas.restore();

		return bitmap;
	}
	
	private boolean sessionHasNecessaryPerms(Session session) {
		for(String p : session.getPermissions()) {
			DLog.d(TAG, "aaaaaaaaaa = " + p);
		}
        if (session != null && session.getPermissions() != null) {
            for (String requestedPerm : PERMISSIONS) {
                if (!session.getPermissions().contains(requestedPerm)) {
                	DLog.d(TAG, "bbbbbbbbb = " + requestedPerm);
                    return false;
                }
            }
            return true;
        }
        return false;
    }
	
	private boolean ensureOpenSession() {
		DLog.e(TAG, "ensureOpenSession:");
        if (Session.getActiveSession() == null ||
                !Session.getActiveSession().isOpened()) {
            Session.openActiveSession(
                    getActivity(), ChartFragment.this, 
                    true, 
                    Arrays.asList("user_friends"),
                    mSessionCallback);
            return false;
        }
        return true;
    }
	
	private void publishPicture(Session session, Bitmap bitmap) {
		
		DLog.e(TAG, "publishPicture");
    	
    	Request.Callback callback= new Request.Callback() {
            public void onCompleted(Response response) {
            	
            	DLog.e(TAG, response.toString());
                FacebookRequestError error = response.getError();
                if (error != null) {
                	if(mProgressDialog != null)
                		mProgressDialog.dismiss();
                    Toast.makeText(getActivity(),
                         error.getErrorMessage(),
                         Toast.LENGTH_SHORT).show();
                } else {
                	Util.requestChargeCoin(getActivity(), ChartFragment.this, 100);
                }
            }
        };

        String applicationId = Utility.getMetadataApplicationId(getActivity());

        Bundle parameters = new Bundle(1);
        parameters.putParcelable("picture", bitmap);
        parameters.putString("message", getResources().getString(R.string.growth_graph_of_nick, mNickNameInfo.mNickName));
        parameters.putString("link", "http://www.doleapps.com");
        parameters.putString("app_id", applicationId);
        
		Request request = Request.newUploadPhotoRequest(session, bitmap, callback);
		request.setParameters(parameters);
		RequestAsyncTask task = new RequestAsyncTask(request);
		task.execute();
		mProgressDialog = Util.openProgressDialog(getActivity());
    	
	}
	
	private void share() {
		final Bitmap chartBitmap = getChartBitmap();
		
		Session session = Session.getActiveSession();
		DLog.e(TAG, "session:"+session);
		if (session == null) {
//			if (savedInstanceState != null) {
//				DLog.e(TAG, "savedInstanceState:"+savedInstanceState);
//				session = Session.restoreSession(getActivity(), null, mSessionCallback, savedInstanceState);
//			}
			if (session == null) {
				session = new Session(getActivity());
			}
			Session.setActiveSession(session);
		}
		
		if(ensureOpenSession()) {
			publishPicture(session, chartBitmap);
		}
	}

	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		if(parser instanceof RequestNewAuthKey) {
	    	if(Constants.DEBUG)
	    		Toast.makeText(getActivity(), result.get(Constants.TAG_AUTHKEY), Toast.LENGTH_LONG).show();
			HeightChartPreference.putString(Constants.PREF_AUTH_KEY, result.get(Constants.TAG_AUTHKEY));
			Util.requestChargeCoin(getActivity(), ChartFragment.this, 100);
		}
		else if(parser instanceof RequestChargeCoin) {
			int coin = HeightChartPreference.getInt(Constants.PREF_LAST_DOLE_COIN, 0);
			coin+=100;
			HeightChartPreference.putInt(Constants.PREF_LAST_DOLE_COIN, coin);
			if(mProgressDialog != null)
				mProgressDialog.dismiss();
			
			Bundle args = new Bundle();
			args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_coin);
			args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.congratulation_for_coin);
			args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES_PARAM1, 100);
			args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
			
			Util.showDialogFragment(
					getFragmentManager(), 
					new CustomDialog(), 
					Constants.TAG_DIALOG_CUSTOM, 
					args, 
					ChartFragment.this, 
					Constants.REQUEST_DOLE_COIN);
		}
	}

	@Override
	public void onFailed(ServerTask parser, Map<String, String> result,
			int returnCode) {
		if(parser instanceof RequestChargeCoin && returnCode == Constants.ERROR_CODE_AUTHKEY_EXPIRED) {
			Util.requestNewAuthKey(getActivity(), this);
		}
		else if(parser instanceof RequestNewAuthKey) {
			Toast.makeText(getActivity(), R.string.error_code_default, Toast.LENGTH_SHORT).show();
			Util.clearUserInfo();
			getActivity().finish();
		}
		else {
			int failAmount = HeightChartPreference.getInt(Constants.PREF_COIN_TO_ADDED, 0);
			failAmount+=100;
			HeightChartPreference.putInt(Constants.PREF_COIN_TO_ADDED, failAmount);
			if(mProgressDialog != null)
				mProgressDialog.dismiss();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE:
			DLog.d(TAG, " Return from facebook publish chart	 activity");
			Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);
			break;

		case Constants.REQUEST_CONFIRM_SHARE:
			share();
			break;
		}
	}
}
