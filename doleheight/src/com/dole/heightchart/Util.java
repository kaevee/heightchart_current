package com.dole.heightchart;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.dole.heightchart.Constants.Characters;
import com.dole.heightchart.HeightInfo.FaceImageMode;
import com.dole.heightchart.camera.CameraHolder;
import com.dole.heightchart.server.RequestChargeCoin;
import com.dole.heightchart.server.RequestNewAuthKey;
import com.dole.heightchart.server.RequestUseQRCode;
import com.dole.heightchart.server.ServerTask;
import com.dole.heightchart.server.ServerTask.IServerResponseCallback;
import com.facebook.Session;
import com.facebook.model.GraphUser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {
	
	public static final String MIXPANEL_TOKEN = "764d9434fe4840b650fd2a9c5da25b1d";
	public static final String GCM_SENDERID = "AIzaSyBvwxBGXTIvunCYPahOY7omfzSQ578KBKY";
	
//	private static final String LOG_TAG = "DoleHeight";
	private static final String TAG = "Util";

    private static final float DEFAULT_CAMERA_BRIGHTNESS = 0.7f;

    public static final int ORIENTATION_HYSTERESIS = 5;
    
    public static void Assert(boolean cond) {
        if (!cond) {
            throw new AssertionError();
        }
    }
    
    public static boolean isInternetAvailable(Context context) {
    	final ConnectivityManager cm =
    	        (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
    	 
    	final NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    	return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
    	
    public static boolean isCriticalVersionUpdateRequired(String appVersion, String serverVserion) {
    	String[] appParts = appVersion.split("\\.");
    	String[] serverParts = serverVserion.split("\\.");
    	int length = Math.max(appParts.length, serverParts.length);

    	for(int i = 0; i < length; i++) {

    		int appPart = i < appParts.length ? Integer.parseInt(appParts[i]) : 0;
    		int serverPart = i < serverParts.length ? Integer.parseInt(serverParts[i]) : 0;

    		if(appPart < serverPart)
    			return i < 2 ? true:false; // MAJOR(0), MINOR(1), FIX(2)

    		if(appPart > serverPart)
    			return false;
    	}
    	return false;
    }
    
    public static void showDialogFragment(FragmentManager manager, DialogFragment dialogFragment, 
    		String tag, Bundle args) {
    	
    	showDialogFragment(manager, dialogFragment, tag, args, null, -1);
    }
    
    public static void showDialogFragment(FragmentManager manager, DialogFragment dialogFragment, 
    		String tag, Bundle args, Fragment targetFragment, int requestCode) {
    	
    	final FragmentTransaction ft = manager.beginTransaction();
		DialogFragment prevDialogFrag = (DialogFragment) manager.findFragmentByTag(tag);
		if(prevDialogFrag != null) {
			prevDialogFrag.dismiss();
		}
		ft.addToBackStack(tag);
		
		if(args != null)
			dialogFragment.setArguments(args);

		if(targetFragment != null)
			dialogFragment.setTargetFragment(targetFragment, requestCode);

		dialogFragment.show(ft, Constants.TAG_DIALOG_ADD_NICK);
    }
    
    public static void replaceFragment(FragmentManager manager, Fragment fragment, 
    		int viewId, String tag, Bundle args, boolean addToStack) {
    	
    	replaceFragment(manager, fragment, viewId, tag, args, null, -1, addToStack);
    }
    
    public static void replaceFragment(FragmentManager manager, Fragment fragment,
    		int viewId, String tag, Bundle args, Fragment targetFragment, int requestCode, boolean addToStack) {
    	
    	final FragmentTransaction ft = manager.beginTransaction();
    	
    	Fragment splash = manager.findFragmentByTag(Constants.TAG_FRAGMENT_SPLASH);
    	if(splash != null && !splash.isHidden()) {
    		ft.hide(splash);
    	}
		
		if(args != null)
			fragment.setArguments(args);
		
		if(targetFragment != null)
			fragment.setTargetFragment(targetFragment, requestCode);
		
		ft.addToBackStack(tag);
		
		ft.replace(viewId, fragment, tag);
		ft.commit();
    }
    
    public static void startChartActivity(Context context, Bundle args) {
    	Intent intent = new Intent(context, ChartActivity.class);
    	intent.putExtras(args);
    	context.startActivity(intent);
    }
    
    public static NickNameInfo getNickNameInfoById(Activity activity, long nickNameId) {
    	final Model model = ((ApplicationImpl)activity.getApplication()).getModel();
    	return model.getNickNameInfoById(nickNameId);
    }
    
    public static HeightInfo getHeightInfoById(Activity activity, long heightId) {
    	final Model model = ((ApplicationImpl)activity.getApplication()).getModel();
    	return model.getHeightInfoById(heightId);
    }

    

    public static int clamp(int x, int min, int max) {
        if (x > max) return max;
        if (x < min) return min;
        return x;
    }

    public static int getDisplayRotation(Activity activity) {
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        switch (rotation) {
            case Surface.ROTATION_0: return 0;
            case Surface.ROTATION_90: return 90;
            case Surface.ROTATION_180: return 180;
            case Surface.ROTATION_270: return 270;
        }
        return 0;
    }

    public static int getDisplayOrientation(int degrees, int cameraId) {
        // See android.hardware.Camera.setDisplayOrientation for
        // documentation.
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        return result;
    }
    
    public static int getJpegRotation(int cameraId, int orientation) {
        // See android.hardware.Camera.Parameters.setRotation for
        // documentation.
        int rotation = 0;
        if (orientation != OrientationEventListener.ORIENTATION_UNKNOWN) {
            CameraInfo info = CameraHolder.instance().getCameraInfo()[cameraId];
            if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
                rotation = (info.orientation - orientation + 360) % 360;
            } else {  // back-facing camera
                rotation = (info.orientation + orientation) % 360;
            }
        }
        return rotation;
    }


    public static int roundOrientation(int orientation, int orientationHistory) {
        boolean changeOrientation = false;
        if (orientationHistory == OrientationEventListener.ORIENTATION_UNKNOWN) {
            changeOrientation = true;
        } else {
            int dist = Math.abs(orientation - orientationHistory);
            dist = Math.min( dist, 360 - dist );
            changeOrientation = ( dist >= 45 + ORIENTATION_HYSTERESIS );
        }
        if (changeOrientation) {
            return ((orientation + 45) / 90 * 90) % 360;
        }
        return orientationHistory;
    }

    public static void initializeScreenBrightness(Window win, ContentResolver resolver) {
        // Overright the brightness settings if it is automatic
        int mode = Settings.System.getInt(resolver, Settings.System.SCREEN_BRIGHTNESS_MODE,
                Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
        if (mode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
            WindowManager.LayoutParams winParams = win.getAttributes();
            winParams.screenBrightness = DEFAULT_CAMERA_BRIGHTNESS;
            win.setAttributes(winParams);
        }
    }

    public static Size getOptimalPreviewSize(Activity currentActivity,
            List<Size> sizes, double targetRatio) {
        // Use a very small tolerance because we want an exact match.
        final double ASPECT_TOLERANCE = 0.001;
        if (sizes == null) return null;

        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        // Because of bugs of overlay and layout, we sometimes will try to
        // layout the viewfinder in the portrait orientation and thus get the
        // wrong size of mSurfaceView. When we change the preview size, the
        // new overlay will be created before the old one closed, which causes
        // an exception. For now, just get the screen size

        Display display = currentActivity.getWindowManager().getDefaultDisplay();
        int targetHeight = Math.max(display.getHeight(), display.getWidth());
        
//        targetRatio = (double) display.getHeight() / (double) display.getWidth();

        if (targetHeight <= 0) {
            // We don't know the size of SurfaceView, use screen height
            targetHeight = display.getHeight();
        }

        // Try to find an size match aspect ratio and size
        for (Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        // Cannot find the one match the aspect ratio. This should not happen.
        // Ignore the requirement.
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    public static void rectFToRect(RectF rectF, Rect rect) {
        rect.left = Math.round(rectF.left);
        rect.top = Math.round(rectF.top);
        rect.right = Math.round(rectF.right);
        rect.bottom = Math.round(rectF.bottom);
    }

    public static Rect rectFToRect(RectF rectF) {
        Rect rect = new Rect();
        rectFToRect(rectF, rect);
        return rect;
    }

    public static RectF rectToRectF(Rect r) {
        return new RectF(r.left, r.top, r.right, r.bottom);
    }

    public static void prepareMatrix(Matrix matrix, int displayOrientation,
            int viewWidth, int viewHeight) {
        matrix.setScale(1, 1);
        matrix.postRotate(displayOrientation);
        matrix.postScale(viewWidth / 2000f, viewHeight / 2000f);
        matrix.postTranslate(viewWidth / 2f, viewHeight / 2f);
    }

    public static void prepareMatrix(Matrix matrix, int displayOrientation,
                                     Rect previewRect) {
        matrix.setScale(1, 1);
        matrix.postRotate(displayOrientation);
        matrix.setRectToRect(rectToRectF(previewRect), new RectF(-1000, -1000, 1000, 1000),
                Matrix.ScaleToFit.FILL);
    }

    public static void setRotationParameter(Parameters parameters, int cameraId, int orientation) {
        // See android.hardware.Camera.Parameters.setRotation for
        // documentation.
        int rotation = 0;
        if (orientation != OrientationEventListener.ORIENTATION_UNKNOWN) {
            CameraInfo info = CameraHolder.instance().getCameraInfo()[cameraId];
            if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
                rotation = (info.orientation - orientation + 360) % 360;
            } else {  // back-facing camera
                rotation = (info.orientation + orientation) % 360;
            }
        }
        parameters.setRotation(rotation);
    }

    public static void broadcastNewPicture(Context context, Uri uri) {
        context.sendBroadcast(new Intent(android.hardware.Camera.ACTION_NEW_PICTURE, uri));
        // Keep compatibility
        context.sendBroadcast(new Intent("com.android.camera.NEW_PICTURE", uri));
    }
	
	public static Bitmap createFaceImageWithCircleMask(
			Activity activity,
			Bitmap originalImage, 
			FaceImageMode mode
			) {
		
		final Resources res = activity.getResources();
//		final int originalImageWidth = originalImage.getWidth();
//		final int maskOriginX = res.getDimensionPixelSize(R.dimen.face_mask_position_x); //TODO Left/Right/Center has all different values need to be changed
        final int maskOriginY = res.getDimensionPixelSize(R.dimen.face_mask_position_y);
        final int maskArrowWidth = res.getDimensionPixelSize(R.dimen.face_mask_arrow_width);
        final int maskImageWidth = res.getDimensionPixelSize(R.dimen.face_mask_width);
        

		final Display display = activity.getWindowManager().getDefaultDisplay();
		final Point size = new Point();
		final DisplayMetrics dm = new DisplayMetrics();
		display.getMetrics(dm);
		
		
		display.getSize(size);
		final int displayWidth = size.x;
		
		int shiftWidth = 0;
		int maskImageResourceId = 0;
		int maskImageShadowResourceId = 0;
		
		switch (mode) {
		case RIGHT:
			shiftWidth = maskArrowWidth;
			maskImageResourceId = R.drawable.detail_right_arrow_normal;
			maskImageShadowResourceId = R.drawable.detail_right_arrow_shadow;
			break;
			
		case RIGHT_NO_ARROW:
			shiftWidth = maskArrowWidth;
			maskImageResourceId = R.drawable.detail_right_arrow_over_press;
			maskImageShadowResourceId = R.drawable.detail_right_arrow_shadow;
			break;

		case CHART:
			maskImageResourceId = R.drawable.summary_sticker;
			maskImageShadowResourceId = R.drawable.summary_sticker_shadow;
			break;
			
		case LEFT:
			maskImageResourceId = R.drawable.detail_left_arrow_normal;
			maskImageShadowResourceId = R.drawable.detail_left_arrow_shadow;
    		break;
    		
		case LEFT_NO_ARROW:
			maskImageResourceId = R.drawable.detail_left_arrow_over_press;
			maskImageShadowResourceId = R.drawable.detail_left_over_shadow;
			break;

		default:
			break;
		}
		
		final Bitmap mask = BitmapFactory.decodeResource(activity.getResources(), maskImageResourceId);

		final int maskH = mask.getHeight();
		final int maskW = mask.getWidth();
		float scale = (float) maskH / (float) maskImageWidth; //Height is equal on all mask image and it's circle
		
		final float leftOnView = (float) (displayWidth - maskImageWidth) / 2;
		final float leftOnImage = leftOnView; 
		Bitmap clip = Bitmap.createBitmap(
				originalImage, 
				(int)leftOnImage - shiftWidth, 
				(int)(maskOriginY), 
				(int)(maskW / scale),
				(int)(maskH / scale));

		// Apply values to new HeightInfo
		Bitmap result = Bitmap.createBitmap(maskW, maskH, Config.ARGB_8888);
		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
		
		final Canvas canvas = new Canvas(result);
		canvas.save();
		canvas.scale(scale, scale);
		canvas.drawBitmap(clip, 0, 0, null); 
		canvas.restore();
		
		canvas.drawBitmap(mask, 0, 0, paint);
		paint.setXfermode(null);
		final Bitmap maskShadow = BitmapFactory.decodeResource(activity.getResources(), maskImageShadowResourceId);
		canvas.drawBitmap(maskShadow, 0, 0, null);
		maskShadow.recycle();
		System.gc();
		return result;
	}
	
	public static void setCameraOrientation(Activity activity, Camera camera,
			int cameraId, int orientation) {
		android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
		android.hardware.Camera.getCameraInfo(cameraId, info);
		orientation = (orientation + 45) / 90 * 90;
		int rotation = 0;
		if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
			rotation = (info.orientation - orientation + 360) % 360;
		} else {  // back-facing camera
			rotation = (info.orientation + orientation) % 360;
		}
		Parameters param = camera.getParameters();
		param.setRotation(rotation);
		camera.setParameters(param);
	}
	
	public static int getCameraDisplayOrientation(Activity activity,
			int cameraId, android.hardware.Camera camera) {
		android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
		android.hardware.Camera.getCameraInfo(cameraId, info);
		int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
		int degrees = 0;
		switch (rotation) {
		case Surface.ROTATION_0: degrees = 0; break;
		case Surface.ROTATION_90: degrees = 90; break;
		case Surface.ROTATION_180: degrees = 180; break;
		case Surface.ROTATION_270: degrees = 270; break;
		}
		
		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360;  // compensate the mirror
		} else {  // back-facing
			result = (info.orientation - degrees + 360) % 360;
		}
		return result;
		//camera.setDisplayOrientation(result);
	}
	
	public static void setGpsParameters(Parameters parameters, Location loc) {
        // Clear previous GPS location from the parameters.
        parameters.removeGpsData();

        // We always encode GpsTimeStamp
        parameters.setGpsTimestamp(System.currentTimeMillis() / 1000);

        // Set GPS location.
        if (loc != null) {
            double lat = loc.getLatitude();
            double lon = loc.getLongitude();
            boolean hasLatLon = (lat != 0.0d) || (lon != 0.0d);

            if (hasLatLon) {
//                Log.d(TAG, "Set gps location");
                parameters.setGpsLatitude(lat);
                parameters.setGpsLongitude(lon);
                parameters.setGpsProcessingMethod(loc.getProvider().toUpperCase());
                if (loc.hasAltitude()) {
                    parameters.setGpsAltitude(loc.getAltitude());
                } else {
                    // for NETWORK_PROVIDER location provider, we may have
                    // no altitude information, but the driver needs it, so
                    // we fake one.
                    parameters.setGpsAltitude(0);
                }
                if (loc.getTime() != 0) {
                    // Location.getTime() is UTC in milliseconds.
                    // gps-timestamp is UTC in seconds.
                    long utcTimeSeconds = loc.getTime() / 1000;
                    parameters.setGpsTimestamp(utcTimeSeconds);
                }
            } else {
                loc = null;
            }
        }
    }
	

	 /**
     * delete file from storage
     * 
     * @param path file path to delete
     * @return return true if file deleted successfully
     */
	public static boolean removeFile(String path) {
		final File target = new File(path);
		if (target == null || !target.exists())
			return false;
		
		File[] currList;
		Stack<File> stack = new Stack<File>();
		stack.push(target);
		while (!stack.isEmpty()) {
			final File lastFile = stack.lastElement();
		    if (lastFile.isDirectory()) {
		        currList = lastFile.listFiles();
		        if (currList.length > 0) {
		            for (File curr: currList)
		                stack.push(curr);
		        } else {
		            stack.pop().delete();
		        }
		    } else {
		    	stack.pop().delete();
		    }
		}
		return true;
	}
	
	public static int getCharacterDrawable(Characters character, String where) {
		if(where.equals("detail_full")) {
			if(character.getCharacterIndex() == 0) {
				return R.drawable.detail_monkey_01;
			}
			else if(character.getCharacterIndex() == 1) {
				return R.drawable.detail_monkey_02;
			}
			else if(character.getCharacterIndex() == 2) {
				return R.drawable.detail_monkey_03;
			}
			else if(character.getCharacterIndex() == 3) {
				return R.drawable.detail_monkey_04;
			}
			else if(character.getCharacterIndex() == 4) {
				return R.drawable.detail_monkey_05;
			}
		}
		else if(where.equals("detail_zoom")) {
			if(character.getCharacterIndex() == 0) {
				return R.drawable.detail_all_monkey_01;
			}
			else if(character.getCharacterIndex() == 1) {
				return R.drawable.detail_all_monkey_02;
			}
			else if(character.getCharacterIndex() == 2) {
				return R.drawable.detail_all_monkey_03;
			}
			else if(character.getCharacterIndex() == 3) {
				return R.drawable.detail_all_monkey_04;
			}
			else if(character.getCharacterIndex() == 4) {
				return R.drawable.detail_all_monkey_05;
			}
		}
		else if(where.equals("detail_zoom_indicator")) {
			if(character.getCharacterIndex() == 0) {
				return R.drawable.detail_all_indicator_monkey_01;
			}
			else if(character.getCharacterIndex() == 1) {
				return R.drawable.detail_all_indicator_monkey_02;
			}
			else if(character.getCharacterIndex() == 2) {
				return R.drawable.detail_all_indicator_monkey_03;
			}
			else if(character.getCharacterIndex() == 3) {
				return R.drawable.detail_all_indicator_monkey_04;
			}
			else if(character.getCharacterIndex() == 4) {
				return R.drawable.detail_all_indicator_monkey_05;
			}
		}
		return -1;
	}
	
	public static String getIp(Context context) {
		WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		return Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
	}
	
	public static String getUUID(Context context) {
		String uuid = null;
		final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		uuid = tm.getDeviceId();
		
		if(uuid == null) {
			uuid = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
		}
		
		if(uuid == null)
			return null;
		
		try {
			uuid = makeSHA1Hash(uuid);
		} catch (NoSuchAlgorithmException e) {
			android.util.Log.e(TAG, "Exception occurred while encrypting uuid", e);
			uuid = null;
		}
		
		return uuid;
	}
	
	private static String makeSHA1Hash(String input) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA1");
		md.reset();
		
		byte[] buffer = input.getBytes();
		md.update(buffer);
		
		byte[] digest = md.digest();
		
		String hexStr = "";
		for (int i = 0; i < digest.length; i++) {
			hexStr +=  Integer.toString( ( digest[i] & 0xff ) + 0x100, 16).substring( 1 );
		}
		return hexStr;
	}
	
	public static Bitmap decodeFile(String path, int maxDecodePixelSize) throws FileNotFoundException {
		try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);
            if (options.mCancel || options.outWidth == -1
                    || options.outHeight == -1) {
                return null;
            }
            options.inSampleSize = computeSampleSize(options, -1, maxDecodePixelSize);
            options.inJustDecodeBounds = false;

            options.inDither = false;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            return BitmapFactory.decodeFile(path, options);
        } catch (OutOfMemoryError ex) {
            Log.e(TAG, "Got oom exception ", ex);
            return null;
        }
	}
	
	/*
     * Compute the sample size as a function of minSideLength
     * and maxNumOfPixels.
     * minSideLength is used to specify that minimal width or height of a
     * bitmap.
     * maxNumOfPixels is used to specify the maximal size in pixels that is
     * tolerable in terms of memory usage.
     *
     * The function returns a sample size based on the constraints.
     * Both size and minSideLength can be passed in as -1
     * which indicates no care of the corresponding constraint.
     * The functions prefers returning a sample size that
     * generates a smaller bitmap, unless minSideLength = -1.
     *
     * Also, the function rounds up the sample size to a power of 2 or multiple
     * of 8 because BitmapFactory only honors sample size this way.
     * For example, BitmapFactory downsamples an image by 2 even though the
     * request is 3. So we round up the sample size to avoid OOM.
     */
    public static int computeSampleSize(BitmapFactory.Options options,
            int minSideLength, int maxNumOfPixels) {
        int initialSize = computeInitialSampleSize(options, minSideLength,
                maxNumOfPixels);

        int roundedSize;
        if (initialSize <= 8) {
            roundedSize = 1;
            while (roundedSize < initialSize) {
                roundedSize <<= 1;
            }
        } else {
            roundedSize = (initialSize + 7) / 8 * 8;
        }

        return roundedSize;
    }

    private static int computeInitialSampleSize(BitmapFactory.Options options,
            int minSideLength, int maxNumOfPixels) {
        double w = options.outWidth;
        double h = options.outHeight;

        int lowerBound = (maxNumOfPixels < 0) ? 1 :
                (int) Math.ceil(Math.sqrt(w * h / maxNumOfPixels));
        int upperBound = (minSideLength < 0) ? 128 :
                (int) Math.min(Math.floor(w / minSideLength),
                Math.floor(h / minSideLength));

        if (upperBound < lowerBound) {
            // return the larger one when there is no overlapping zone.
            return lowerBound;
        }

        if (maxNumOfPixels < 0 && minSideLength < 0) {
            return 1;
        } else if (minSideLength < 0) {
            return lowerBound;
        } else {
            return upperBound;
        }
    }
    
    public static boolean checkEmail(String src) {
    	String a = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
    	Pattern p = Pattern.compile(a);
    	Matcher m = p.matcher(src);
    	return m.find();
    }
    
    public static String switchErrorCode(int code, Resources res) {
    	switch(code) {
    	case ServerTask.RESULT_ERROR_NETWORK:
    		return res.getString(R.string.unstable_network);
    	case Constants.ERROR_CODE_NO_RECORD:
    		return res.getString(R.string.email_exist_or_incorrect);
    	case Constants.ERROR_CODE_PARAM:
    		return res.getString(R.string.error_code_param);
    	case Constants.ERROR_CODE_EXIST_ID:
    		return res.getString(R.string.already_used_email);
    	case Constants.ERROR_CODE_EXIST_EMAIL:
    		return res.getString(R.string.already_used_email);
    	case Constants.ERROR_CODE_NO_DATA:
    		return res.getString(R.string.email_exist_or_incorrect);
    	case Constants.ERROR_CODE_CHECK_PWD:
    		return res.getString(R.string.password_incorrect);
    	case Constants.ERROR_CODE_UNREGIST_SNS:
    		return res.getString(R.string.error_code_default);
    	case Constants.ERROR_CODE_SNS_REG:
    		return res.getString(R.string.cannot_register_check_facebook);
    	case Constants.ERROR_CODE_SNS_ID:
    		return res.getString(R.string.email_registerd_by_facebook);
    	case Constants.ERROR_CODE_FACEBOOK_ID:
    		return res.getString(R.string.email_registerd_by_facebook);
    	case Constants.ERROR_CODE_LOGIN_LIMIT:
    		return res.getString(R.string.error_code_default);
    	case Constants.ERROR_CODE_USELESS_AUTH_KEY:
    		return res.getString(R.string.error_code_default);
    	case Constants.ERROR_CODE_INVALID_AUTH_KEY:
    		return res.getString(R.string.error_code_default);
    	default:
    		return res.getString(R.string.error_code_default);
    	}
    }
    
    public static ProgressDialog openProgressDialog(Context context) {
    	ProgressDialog dialog = new ProgressDialog(context);
    	dialog.setCancelable(false);
    	dialog.setIndeterminate(true);
    	dialog.show();
    	dialog.setContentView(R.layout.progress_layout);
    	return dialog;
    }
    
    public static String getCountryIsoForFacebook(Context context, GraphUser user) {
    	String countryIso = null;
    	if( user.getLocation() != null) {
    		countryIso = user.getLocation().getCountry();
    	}
    	
    	if(countryIso == null) {
    		 countryIso = getCountryIsoFromTelephonyManager(context);
    	} else {
    		return countryIso;
    	}
    	
    	return countryIso;
    }
    
    public static String getCityForFacebook(GraphUser user) {
    	String city = null;
    	if(user.getLocation() != null) {
    		city = user.getLocation().getCity();

    		if(city == null) {
    			String cityAndCountry = (String) user.getLocation().getProperty("name");
    			if(cityAndCountry != null) {
    				city = cityAndCountry.substring(0, cityAndCountry.indexOf(","));
    			}
    		}
    	}
    	if(city == null)
    		return "";
    	return city;
    }
    
    public static String getCountryIsoFromTelephonyManager(Context context) {
    	try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                return simCountry.toLowerCase(Locale.US);
            }
            else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                } else {
                	return Locale.getDefault().getCountry().toLowerCase(Locale.US);
                }
            } else {
            	return Locale.getDefault().getCountry().toLowerCase(Locale.US);
            }
        }
        catch (Exception e) { 
        	e.printStackTrace();
        }
        return null;
    }
    
    public static void requestPublishPermissions(Activity activity, Session session, List<String> permissions,  
    		int requestCode) {
    	if (session != null) {
    		Session.NewPermissionsRequest reauthRequest = new Session.NewPermissionsRequest(activity, permissions)
    		.setRequestCode(requestCode);
    		session.requestNewPublishPermissions(reauthRequest);
    	}
    }

    public static void requestReadPermissions(Activity activity, Session session, List<String> permissions,  
    		int requestCode) {
    	if (session != null) {
    		Session.NewPermissionsRequest reauthRequest = new Session.NewPermissionsRequest(activity, permissions)
    		.setRequestCode(requestCode);
    		session.requestNewReadPermissions(reauthRequest);
    	}
    }
    
    public static String createOrderId() {
    	String userNo = String.valueOf(HeightChartPreference.getInt(Constants.TAG_USER_NO, 0));
    	String timeStamp = String.valueOf(System.currentTimeMillis());
    	return userNo + "_" + timeStamp;
    }

	/**
	 * Use qrcode
	 */
    public static void requestUseQrCode(Context context, IServerResponseCallback callback, String serial) {
//    	final String authKey = HeightChartPreference.getString(Constants.TAG_AUTHKEY, null);
    	final String authKey = HeightChartPreference.getString(Constants.PREF_AUTH_KEY, "");
    	final String userNo = String.valueOf(HeightChartPreference.getInt(Constants.PREF_USER_NO, 0));
    	DLog.e(TAG, "authKey:"+authKey);
    	DLog.e(TAG, "userNo:"+userNo);
    	if(authKey == null || userNo == null)
    		return;
    	
    	final Map<String, String> request = new HashMap<String, String>();
    	request.put(Constants.TAG_USER_NO, userNo);
    	request.put(Constants.TAG_AUTHKEY, authKey);
    	request.put(Constants.TAG_ORDER_ID, createOrderId());
    	request.put(Constants.TAG_SERIAL, serial);
    	request.put(Constants.TAG_CLIENT_IP, Util.getIp(context));

    	ServerTask task = new RequestUseQRCode(context, request);
    	task.setCallback(callback);
    	Model.runOnWorkerThread(task);
    }

	/**
	 * Charge Dole coin
	 */
    public static void requestChargeCoin(Context context, IServerResponseCallback callback, int chargeAmount) {
    	final String authKey = HeightChartPreference.getString(Constants.PREF_AUTH_KEY, null);
    	final String userNo = String.valueOf(HeightChartPreference.getInt(Constants.PREF_USER_NO, 0));
    	if(authKey == null || userNo == null)
    		return;
    	
    	final Map<String, String> request = new HashMap<String, String>();
    	request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
    	request.put(Constants.TAG_USER_NO, userNo);
    	request.put(Constants.TAG_AUTHKEY, authKey);
    	request.put(Constants.TAG_ORDER_ID, createOrderId());
    	
    	request.put(Constants.TAG_PAYMENT_METHOD_CODE, Constants.PAYMENT_METHOD_CODE);
    	request.put(Constants.TAG_PAYMENT_AMOUNT, "0");
    	request.put(Constants.TAG_CHARGE_AMOUNT, String.valueOf(chargeAmount));
    	request.put(Constants.TAG_VALIDITY_PERIOD, Constants.PAYMENT_VALIDITY_PERIOD);
    	request.put(Constants.TAG_DESCRIPTION, "");
    	request.put(Constants.TAG_UUID, Util.getUUID(context));    	
    	request.put(Constants.TAG_CLIENT_IP, Util.getIp(context));

    	ServerTask task = new RequestChargeCoin(context, request);
    	task.setCallback(callback);
    	Model.runOnWorkerThread(task);
    }

	/**
	 * refresh authkey
	 */
    public static void requestNewAuthKey(Context context, IServerResponseCallback callback) {
    	final String authKey = HeightChartPreference.getString(Constants.PREF_AUTH_KEY, null);
    	final String userNo = String.valueOf(HeightChartPreference.getInt(Constants.PREF_USER_NO, 0));
    	if(authKey == null || userNo == null)
    		return;
    	String registrationId = HeightChartPreference.getString(Constants.PROPERTY_REG_ID, "");

    	final Map<String, String> request = new HashMap<String, String>();
    	request.put(Constants.TAG_USER_NO, userNo);
    	request.put(Constants.TAG_AUTHKEY, authKey);
    	request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
    	request.put(Constants.TAG_MOBILE_OS, Constants.OS_STRING);
		request.put(Constants.TAG_DEVICE_TOKEN, registrationId);
    	request.put(Constants.TAG_UUID, Util.getUUID(context));    	
    	request.put(Constants.TAG_CLIENT_IP, Util.getIp(context));

    	ServerTask task = new RequestNewAuthKey(context, request);
    	task.setCallback(callback);
    	Model.runOnWorkerThread(task);
    }

	/**
	 * clear userinfo for logout
	 */
    public static void clearUserInfo() {
    	HeightChartPreference.remove(Constants.PREF_USER_ID);
		HeightChartPreference.remove(Constants.PREF_USER_NO);
		HeightChartPreference.remove(Constants.PREF_AUTH_KEY);
		HeightChartPreference.remove(Constants.PREF_EMAIL_VALIFICATION);
		HeightChartPreference.remove(Constants.PREF_EMAIL_ACTIVATE_DATE_TIME);
		HeightChartPreference.remove(Constants.PREF_CHANGE_PASSWROD_DATE_TIME);
		HeightChartPreference.remove(Constants.PREF_REGISTER_DATE_TIME);
		HeightChartPreference.remove(Constants.PREF_COUNTRY);
		HeightChartPreference.remove(Constants.PREF_LANGUAGE);
		HeightChartPreference.remove(Constants.PREF_GENDER);
		HeightChartPreference.remove(Constants.PREF_BIRTHDAY);
		HeightChartPreference.remove(Constants.PREF_ADDRESS_MAIN);
		HeightChartPreference.remove(Constants.PREF_LOGIN_TYPE);
    }
    
    public static void compressBitmapToJpeg(Bitmap bitmap, File file) {
    	FileOutputStream out = null;
    	try {
    		out = new FileOutputStream(file);
    		bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
    	} catch (Exception e) {
    		DLog.e(TAG, "Error creating bitmap occurred", e);
    	} finally {
    		if(out != null) {
    			try {
					out.flush();
					out.close();
				} catch (IOException e) {
					DLog.e(TAG, "Error closing bitmap file occurred", e);
				}
    		}
    	}
    }
    
    public static void compressBitmapToPng(Bitmap bitmap, File file) {
    	FileOutputStream out = null;
    	try {
    		out = new FileOutputStream(file);
    		bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
    	} catch (Exception e) {
    		DLog.e(TAG, "Error creating bitmap occurred", e);
    	} finally {
    		if(out != null) {
    			try {
					out.flush();
					out.close();
				} catch (IOException e) {
					DLog.e(TAG, "Error closing bitmap file occurred", e);
				}
    		}
    	}
    }
    
    public static int getAgeOfDate(String birthDay, String dateStr) {
		try {
			Calendar curCalendar = Calendar.getInstance();
			Calendar birthCalendar = Calendar.getInstance();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date curDate = new Date();
			
			if(dateStr != null) {
				curDate = format.parse(dateStr);  
				curCalendar.setTime(curDate);
			}
			
			Date birthDate = format.parse(birthDay);
			birthCalendar.setTime(birthDate);
			
			if(birthCalendar.after(curCalendar))
				return -1;
			int age = curCalendar.get(Calendar.YEAR) - birthCalendar.get(Calendar.YEAR);
			if(birthCalendar.get(Calendar.DAY_OF_YEAR) > curCalendar.get(Calendar.DAY_OF_YEAR))
				age-=1;
			
			return age;
		} catch (ParseException e) { 
			e.printStackTrace();
			return -1;
		}
    }
    
    public static float getAverageHeightOfAge(Context context, int age, String gender) {
    	float avgHeight = -1f;

    	String[] avgHeights = context.getResources().getStringArray(R.array.avg_height);
		
		if(avgHeights != null && age >= 1) {
			String avgHeightStr = null;
			if(age > avgHeights.length)
				avgHeightStr = avgHeights[avgHeights.length-1];
			else
				avgHeightStr = avgHeights[age-1];
			
			String[] avgGenderHeight = avgHeightStr.split(",");
			if(Constants.NICKNAME_GENDER_BOY.equals(gender))
				avgHeight = Float.valueOf(avgGenderHeight[0]);
			else
				avgHeight = Float.valueOf(avgGenderHeight[1]);
		}
		
		return avgHeight;
    }
    
    public static void hideSoftInput(Context context, View v) {
    	InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
	    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
    
    public static String getLanguage(String language) {
    	if(language.equals(Constants.LANGUAGE_KO))
    		return "Korean";
    	else if(language.equals(Constants.LANGUAGE_JA))
    		return "Japanese";
    	else if(language.equals(Constants.LANGUAGE_EN))
    		return "English";
    	else
    		return "English";
    }
    
    public static String getContentGroupClassNo(String countryCode) {
    	if(countryCode.equalsIgnoreCase(Constants.COUNTRY_PH))
    		return "153";
    	else if(countryCode.equalsIgnoreCase(Constants.COUNTRY_SG))
    		return "154";
    	else if(countryCode.equalsIgnoreCase(Constants.COUNTRY_NZ))
    		return "155";
    	else if(countryCode.equalsIgnoreCase(Constants.COUNTRY_KR))
    		return "156";
    	else if(countryCode.equalsIgnoreCase(Constants.COUNTRY_JP))
    		return "157";
    	else
    		return "-1";
    }
}