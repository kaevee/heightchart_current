package com.dole.heightchart;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class AgreePersonalInfoFragment extends Fragment {
	
	public static final String HIDE_CHECKBOX = "hide_checkbox";
	public static final String FROM_FACEBOOK_LOGIN = "from_facebook_login";
	private boolean mIsHideCheckButton = false; 
	private boolean mIsFromFacebook = false; 
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		final Bundle args = getArguments();
		if(args != null) {
			mIsHideCheckButton = args.getBoolean(AgreePersonalInfoFragment.HIDE_CHECKBOX, false);
			mIsFromFacebook = args.getBoolean(AgreePersonalInfoFragment.FROM_FACEBOOK_LOGIN, false);
		}
		
		int layoutId = R.layout.agree_personal_main;
		if(mIsHideCheckButton) {
			layoutId = R.layout.agree_personal_about;
		}
		return inflater.inflate(layoutId, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		final WebView termsConditionHtml = (WebView) view.findViewById(R.id.agree_personal_terms_condition_text);
		termsConditionHtml.loadUrl("file:///android_res/raw/terms_and_conditions.html");
		
		final WebView privatePolicyHtml = (WebView) view.findViewById(R.id.agree_personal_policy_privacy_text);
		privatePolicyHtml.loadUrl("file:///android_res/raw/privacy_policy.html");

		if(!mIsHideCheckButton) {
			final Button next = (Button)view.findViewById(R.id.agree_personal_btn_next);
			final CheckBox useTerm = (CheckBox)view.findViewById(R.id.agree_personal_use_term);
			final CheckBox infoTerm = (CheckBox)view.findViewById(R.id.sign_up_text_personal_info_term);
			final Button cancel = (Button)view.findViewById(R.id.agree_personal_btn_cancel);
			
			next.setEnabled(false);

			if(mIsFromFacebook) {
				next.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Bundle args = new Bundle(getArguments());
						args.putInt(UserDetailFragment.EXTRA_USER_DETAIL_MODE_NAME, UserDetailFragment.SIGN_UP_FACEBOOK);
						getFragmentManager().popBackStack();
						Util.replaceFragment(getFragmentManager(), new UserDetailFragment(), R.id.container,
	        	    			Constants.TAG_FRAGMENT_USER_DETAIL, args, true);
					}
				});

				cancel.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						getFragmentManager().popBackStack();
					}
				});
			}
			else {
				next.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Fragment target = getTargetFragment();
						if(target != null) {
							Intent i = new Intent();
							i.putExtra(Constants.CLICK_NEXT, true);
							target.onActivityResult(Constants.REQUEST_CODE_AGREE_PERSONAL, Activity.RESULT_OK, i);
							getFragmentManager().popBackStack();
						}
					}
				});

				cancel.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Fragment target = getTargetFragment();
						if(target != null) {
							Intent i = new Intent();
							i.putExtra(Constants.CLICK_NEXT, false);
							target.onActivityResult(Constants.REQUEST_CODE_AGREE_PERSONAL, Activity.RESULT_OK, i);
							getFragmentManager().popBackStack();
						}
					}
				});
			}

			useTerm.setOnCheckedChangeListener(mCheckedChangeListener);
			infoTerm.setOnCheckedChangeListener(mCheckedChangeListener);
		}
	}
	
	private OnCheckedChangeListener mCheckedChangeListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			Button next = (Button)getView().findViewById(R.id.agree_personal_btn_next);
			CheckBox useTerm = (CheckBox)getView().findViewById(R.id.agree_personal_use_term);
			CheckBox infoTerm = (CheckBox)getView().findViewById(R.id.sign_up_text_personal_info_term);
			
			if(useTerm.isChecked() && infoTerm.isChecked())
				next.setEnabled(true);
			else
				next.setEnabled(false);
		}
		
	};
}
