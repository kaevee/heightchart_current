package com.dole.heightchart;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dole.heightchart.Constants.GenderType;
import com.dole.heightchart.server.GetMobileProfile;
import com.dole.heightchart.server.ModifyMobileProfile;
import com.dole.heightchart.server.RequestAuthKey;
import com.dole.heightchart.server.RequestModifySNSUserProfile;
import com.dole.heightchart.server.RequestSNSLogin;
import com.dole.heightchart.server.RequestSignUp;
import com.dole.heightchart.server.ServerTask;
import com.dole.heightchart.server.ServerTask.IServerResponseCallback;
import com.dole.heightchart.ui.CustomDialog;
import com.dole.heightchart.ui.DoleCheckBox;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

public class UserDetailFragment extends Fragment implements IServerResponseCallback {
	
	private static final String TAG = UserDetailFragment.class.getName();
	
	public static final String EXTRA_USER_DETAIL_MODE_NAME = "user_detail_mode";
	public static final int SIGN_UP_EMAIL = -1;
	public static final int SIGN_UP_FACEBOOK = -2;
	
	private RequestSignUp mRequestSingUp;
	private RequestAuthKey mRequestAuthKey;
	private ModifyMobileProfile mModifyMobile;
	private int mMode;
	private String mTempUserId;
	private String mTempPassword;
	private String mTempUserNo;
	private String mTempAuthKey;
	private String mTempCountry;
	private String mTempLanguage;
	private int mTempGender;
	private String mTempBirth;
	private String mTempAddress;
	private boolean mIsCheckedNext;
	private boolean mIsEmailFill = false;
	private boolean mIsPasswordFill = false;
	private boolean mIsBirthFill = false;
	private boolean mIsCityFill = false;
	private ProgressDialog mProgressDialog;
	private boolean mIsFacebookLogin = false;

	MixpanelAPI mMixpanel;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
	    //longzhe cui added
		mMixpanel = MixpanelAPI.getInstance(getActivity(), Util.MIXPANEL_TOKEN);
		mMixpanel.timeEvent("Signup Success");
		
		return inflater.inflate(R.layout.user_detail, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		Bundle args = getArguments();
		if(args != null)
			mMode = args.getInt(UserDetailFragment.EXTRA_USER_DETAIL_MODE_NAME, SIGN_UP_EMAIL);
		else
			mMode = SIGN_UP_EMAIL;
		
		//Change visibility and Change text
		final TextView emailText = (TextView) view.findViewById(R.id.user_detail_edit_email);
		final TextView oldPassView = (TextView)view.findViewById(R.id.user_detail_old_password);
		final TextView passView = (TextView)view.findViewById(R.id.user_detail_edit_password);
		final TextView passConfirmView = (TextView)view.findViewById(R.id.user_detail_edit_confirm_password);
		final DoleCheckBox checkTerms = (DoleCheckBox)view.findViewById(R.id.user_detail_text_personal_info_term);
		final Button confirmChangeButton = (Button) view.findViewById(R.id.user_detail_btn_confirm_edit);
		final TextView editBirthDay = (TextView)view.findViewById(R.id.user_detail_edit_birthday);
		final TextView cityView = (TextView) view.findViewById(R.id.user_detail_edit_city);
		final RadioGroup genderGroup = (RadioGroup) view.findViewById(R.id.user_detail_select_gender);
		View back = view.findViewById(R.id.user_detail_btn_cancel);
		
		final String[] cities = getResources().getStringArray(R.array.city);		
		if(cities != null && cities.length > 0) {
			cityView.setFocusable(false);
			cityView.setClickable(true);
			cityView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					Util.showDialogFragment(getFragmentManager(), new CityDialog(), Constants.CITY, null, UserDetailFragment.this, Constants.REQUEST_CITY);
				}
			});
		}
		
		switch (mMode) {
		case Constants.LOGIN_TYPE_FACEBOOK:
			checkTerms.setNext(true);
			emailText.setTextColor(0xFF606060);
			emailText.setBackgroundColor(Color.TRANSPARENT);
			emailText.setGravity(Gravity.CENTER);
			emailText.setText(HeightChartPreference.getString(Constants.PREF_USER_ID, ""));
			emailText.setCompoundDrawables(null, null, null, null);
			emailText.setEnabled(false);
			
			oldPassView.setVisibility(View.GONE);
			passView.setVisibility(View.GONE);
			passConfirmView.setVisibility(View.GONE);
			
			editBirthDay.setText(HeightChartPreference.getString(Constants.PREF_BIRTHDAY, "").substring(0, 4));
			cityView.setText(HeightChartPreference.getString(Constants.PREF_ADDRESS_MAIN, ""));
			
			if(HeightChartPreference.getInt(Constants.PREF_GENDER, 0) == Constants.GenderType.MALE.type)
				genderGroup.check(R.id.user_detail_gender_male);
			else if(HeightChartPreference.getInt(Constants.PREF_GENDER, 0) == Constants.GenderType.FEMALE.type)
				genderGroup.check(R.id.user_detail_gender_female);
			checkTerms.setVisibility(View.GONE);
			confirmChangeButton.setText(getString(R.string.save));
			confirmChangeButton.setEnabled(true);
			mIsEmailFill = true;
			mIsPasswordFill = true;
			mIsBirthFill = true;
			mIsCityFill = true;
			break;
		case SIGN_UP_FACEBOOK:
			checkTerms.setNext(true);
			emailText.setTextColor(0xFF606060);
			emailText.setBackgroundColor(Color.TRANSPARENT);
			emailText.setGravity(Gravity.CENTER);
			emailText.setText(args.getString(Constants.TAG_USER_ID, ""));
			emailText.setCompoundDrawables(null, null, null, null);
			emailText.setEnabled(false);
			
			oldPassView.setVisibility(View.GONE);
			passView.setVisibility(View.GONE);
			passConfirmView.setVisibility(View.GONE);
			
			editBirthDay.setText(args.getString(Constants.TAG_BIRTHDAY, "").substring(6));
			cityView.setText(args.getString(Constants.TAG_ADDRESS_MAIN, ""));
			
			if(Integer.valueOf(args.getString(Constants.TAG_GENDER, "0")) == Constants.GenderType.MALE.type)
				genderGroup.check(R.id.user_detail_gender_male);
			else if(Integer.valueOf(args.getString(Constants.TAG_GENDER, "0")) == Constants.GenderType.FEMALE.type)
				genderGroup.check(R.id.user_detail_gender_female);
			checkTerms.setVisibility(View.GONE);
			confirmChangeButton.setText(getString(R.string.save));
			confirmChangeButton.setEnabled(true);
			back.setVisibility(View.GONE);
			mIsEmailFill = true;
			mIsPasswordFill = true;
			mIsBirthFill = true;
			mIsCityFill = true;
			break;
		case Constants.LOGIN_TYPE_EMAIL:
			checkTerms.setNext(true);
			passView.setHint(R.string.change_password);
			emailText.setTextColor(0xFF606060);
			emailText.setBackgroundColor(Color.TRANSPARENT);
			emailText.setGravity(Gravity.CENTER);
			emailText.setText(HeightChartPreference.getString(Constants.PREF_USER_ID, ""));
			emailText.setCompoundDrawables(null, null, null, null);
			emailText.setEnabled(false);
			editBirthDay.setText(HeightChartPreference.getString(Constants.PREF_BIRTHDAY, "").substring(0, 4));
			cityView.setText(HeightChartPreference.getString(Constants.PREF_ADDRESS_MAIN, ""));
			if(HeightChartPreference.getInt(Constants.PREF_GENDER, 0) == Constants.GenderType.MALE.type)
				genderGroup.check(R.id.user_detail_gender_male);
			else if(HeightChartPreference.getInt(Constants.PREF_GENDER, 0) == Constants.GenderType.FEMALE.type)
				genderGroup.check(R.id.user_detail_gender_female);
			checkTerms.setVisibility(View.GONE);
			confirmChangeButton.setText(getString(R.string.save));
			confirmChangeButton.setEnabled(true);
			mIsEmailFill = true;
			mIsPasswordFill = true;
			mIsBirthFill = true;
			mIsCityFill = true;
			break;
			
		//Show all
		case SIGN_UP_EMAIL:
			oldPassView.setVisibility(View.GONE);
			genderGroup.check(R.id.user_detail_gender_male);
			confirmChangeButton.setEnabled(false);
			checkTerms.setNext(mIsCheckedNext);
		default:
			break;
		}
		
		editBirthDay.setOnTouchListener(mTransactFragmentOnclickListener);
		confirmChangeButton.setOnClickListener(mDoneEditOnClickListener);
		checkTerms.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				confirmChangeButton.setEnabled(isChecked);
			}
		});
		checkTerms.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP) {
					Util.replaceFragment(getFragmentManager(), new AgreePersonalInfoFragment(), R.id.container, 
							Constants.TAG_FRAGMENT_AGREE_PERSONAL_INFO, null, UserDetailFragment.this,
							Constants.REQUEST_CODE_AGREE_PERSONAL, true);
					return true;
				}
				return false;
			}
		});
		back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
				switch (mMode) {
				case Constants.LOGIN_TYPE_FACEBOOK:
				case Constants.LOGIN_TYPE_EMAIL:
					((MainActivity)getActivity()).onBackPressed();
					break;
					
				//Show all
				case SIGN_UP_EMAIL:
					Bundle args = new Bundle();
					args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
					args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.cancel_sign_up);
					args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, R.drawable.popup_cancel_btn);
					args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
					args.putBoolean(Constants.CUSTOM_DIALOG_CHANGE_TEXT_YES_NO, true);
					Util.showDialogFragment(getFragmentManager(), new CustomDialog(), Constants.TAG_DIALOG_CUSTOM, 
							args, UserDetailFragment.this, Constants.REQUEST_USER_CONFIRM);
					break;
				default:
					break;
				}
			}
		});
		
		emailText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				if(mMode != SIGN_UP_EMAIL)
					return;
				String st = s.toString();
				if(st.trim().equals(""))
					mIsEmailFill = false;
				else
					mIsEmailFill = true;

				if(mIsEmailFill && mIsPasswordFill && mIsBirthFill && mIsCityFill && checkTerms.isChecked())
					confirmChangeButton.setEnabled(true);
				else
					confirmChangeButton.setEnabled(false);
			}
		});
		
		passView.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				if(mMode != SIGN_UP_EMAIL)
					return;
				String st = s.toString();
				if(st.trim().equals(""))
					mIsPasswordFill = false;
				else
					mIsPasswordFill = true;

				if(mIsEmailFill && mIsPasswordFill && mIsBirthFill && mIsCityFill && checkTerms.isChecked())
					confirmChangeButton.setEnabled(true);
				else
					confirmChangeButton.setEnabled(false);
			}
		});
		
		editBirthDay.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				String st = s.toString();
				if(st.trim().equals(""))
					mIsBirthFill = false;
				else
					mIsBirthFill = true;

				if(mIsEmailFill && mIsPasswordFill && mIsBirthFill && mIsCityFill && checkTerms.isChecked())
					confirmChangeButton.setEnabled(true);
				else
					confirmChangeButton.setEnabled(false);
			}
		});
		
		cityView.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				String st = s.toString();
				if(st.trim().equals(""))
					mIsCityFill = false;
				else
					mIsCityFill = true;

				if(mIsEmailFill && mIsPasswordFill && mIsBirthFill && mIsCityFill && checkTerms.isChecked())
					confirmChangeButton.setEnabled(true);
				else
					confirmChangeButton.setEnabled(false);
			}
		});
		
		getView().setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent e) {
				Util.hideSoftInput(getActivity(), v);
				emailText.clearFocus();
				oldPassView.clearFocus();
				passView.clearFocus();
				passConfirmView.clearFocus();
				passView.clearFocus();
				cityView.clearFocus();
				return false;
			}
		});
	}
	
	private OnClickListener mDoneEditOnClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (mMode) {
			case SIGN_UP_EMAIL:
				requestSignUp();
				break;

			case SIGN_UP_FACEBOOK:
				requestSignupSns();
				break;
				
			case Constants.LOGIN_TYPE_EMAIL:
				modifyEmail();
				break;
				
			case Constants.LOGIN_TYPE_FACEBOOK:
				modifySnsInfo();
				break;

			default:
				break;
			}
		}
	};
	
	private OnTouchListener mTransactFragmentOnclickListener = new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if(event.getAction() != MotionEvent.ACTION_UP) 
				return false;
			
			final int id = v.getId();
			String tag = null;
			BirthDayPickerDialog fragment = null;
			switch (id) {
			case R.id.user_detail_edit_birthday:
				tag = Constants.TAG_DIALOG_BIRTHDAY_PICKER;
				fragment = new BirthDayPickerDialog();
				
				final Bundle args = new Bundle();
				args.putBoolean(BirthDayPickerDialog.EXTRA_BIRTH_DATE_ONLY_YEAR, true);
				try {
					args.putInt(BirthDayPickerDialog.EXTRA_YEAR, Integer.valueOf(((TextView)v).getText().toString()));
				}
				catch (java.lang.NumberFormatException e) {
					
				}
				fragment.setArguments(args);
				fragment.setTargetFragment(UserDetailFragment.this, Constants.REQUEST_CODE_DATE_DIALOG);
				break;
			default:
				break;
			}
			
			if(tag == null || fragment == null)
				return false;

			FragmentTransaction ft = getFragmentManager().beginTransaction();
			Fragment prevFrag = getFragmentManager().findFragmentByTag(tag);
			if(prevFrag != null) {
				ft.remove(prevFrag);
			}
			ft.addToBackStack(null);
			fragment.show(ft, tag);
			return true;
		}
	};
	
	private void requestSignUp() {
		
		final TextView emailView = (TextView) getView().findViewById(R.id.user_detail_edit_email);
		final TextView passView = (TextView) getView().findViewById(R.id.user_detail_edit_password);
		final TextView passConfirmView = (TextView) getView().findViewById(R.id.user_detail_edit_confirm_password);
		final TextView birthView = (TextView) getView().findViewById(R.id.user_detail_edit_birthday);
		final TextView cityView = (TextView) getView().findViewById(R.id.user_detail_edit_city);
		final RadioGroup genderGroup = (RadioGroup) getView().findViewById(R.id.user_detail_select_gender);
		final CheckBox termsAgreeCheck = (CheckBox) getView().findViewById(R.id.user_detail_text_personal_info_term);
		
		final int genderId = genderGroup.getCheckedRadioButtonId();
		final RadioButton genderButton = (RadioButton) genderGroup.findViewById(genderId);
		final GenderType gender;
		if(genderId == R.id.user_detail_gender_male) {
			gender = GenderType.MALE;
		} else if(genderId == R.id.user_detail_gender_female){
			gender = GenderType.FEMALE;
		} else {
			gender = null;
		}
		
		final Locale current = getResources().getConfiguration().locale;
		final String lang = Util.getLanguage(current.getLanguage());
		
		String emailAdd = emailView.getText().toString();
		String newPwd = passView.getText().toString();
		String newConfirmPwd = passConfirmView.getText().toString();

		if(!Util.checkEmail(emailAdd)) {
			Toast.makeText(getActivity(), getResources().getString(R.string.enter_full_email), Toast.LENGTH_SHORT).show();
			return;
		}
		if(newPwd.length() < Constants.PASSWORD_MIN_LENGTH) {
			Toast.makeText(getActivity(), R.string.password_at_least_six_2, Toast.LENGTH_SHORT).show();
			return;
		}
		if(!newPwd.equals(newConfirmPwd)) {
			Toast.makeText(getActivity(), R.string.password_incorrect2, Toast.LENGTH_SHORT).show();
			return;
		}
		
		mTempUserId = emailView.getText().toString();
		if(!Util.checkEmail(mTempUserId)) {
			Toast.makeText(getActivity(), R.string.enter_full_email, Toast.LENGTH_SHORT).show();
			return;
		}
		mTempPassword = passView.getText().toString();
		mTempCountry = Util.getCountryIsoFromTelephonyManager(getActivity());
		mTempAddress = cityView.getText().toString();
		mTempBirth = birthView.getText().toString() + "-01-01";
		mTempGender = gender.type;
		mTempLanguage = lang;
		final HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_ID, mTempUserId);
		request.put(Constants.TAG_PASSWORD, mTempPassword);
		request.put(Constants.TAG_EMAIL, mTempUserId);
		request.put(Constants.TAG_STANDARD_COUNTRY_CODE, mTempCountry);
		request.put(Constants.TAG_UUID, Util.getUUID(getActivity()));
		request.put(Constants.TAG_ADDRESS_MAIN, mTempAddress);
		request.put(Constants.TAG_BIRTHDAY, mTempBirth);
		request.put(Constants.TAG_GENDER, Integer.toString(mTempGender));
		request.put(Constants.TAG_LANGUAGE, mTempLanguage);
		request.put(Constants.TAG_SNS_CODE, ""); //TODO For facebook
		request.put(Constants.TAG_SNS_ID, ""); //TODO For facebook
		request.put(Constants.TAG_SNS_USER_NAME, ""); //TODO For facebook
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
		
		mRequestSingUp = new RequestSignUp(getActivity(), request);
		mRequestSingUp.setCallback(this);
		Model.runOnWorkerThread(mRequestSingUp);
		mProgressDialog = Util.openProgressDialog(getActivity());
	}
	
	private void modifyEmail() {
		final TextView oldPassView = (TextView) getView().findViewById(R.id.user_detail_old_password);
		final TextView passView = (TextView) getView().findViewById(R.id.user_detail_edit_password);
		final TextView passConfirmView = (TextView) getView().findViewById(R.id.user_detail_edit_confirm_password);
		final TextView birthView = (TextView) getView().findViewById(R.id.user_detail_edit_birthday);
		final TextView cityView = (TextView) getView().findViewById(R.id.user_detail_edit_city);
		final RadioGroup genderGroup = (RadioGroup) getView().findViewById(R.id.user_detail_select_gender);
		
		final int genderId = genderGroup.getCheckedRadioButtonId();
		final RadioButton genderButton = (RadioButton) genderGroup.findViewById(genderId);
		final GenderType gender;
		if(genderId == R.id.user_detail_gender_male) {
			gender = GenderType.MALE;
		} else if(genderId == R.id.user_detail_gender_female){
			gender = GenderType.FEMALE;
		} else {
			gender = null;
		}
		
		String oldPwd = oldPassView.getText().toString();
		String newPwd = passView.getText().toString();
		String newConfirmPwd = passConfirmView.getText().toString();
		if(!newPwd.equals(newConfirmPwd)) {
			Toast.makeText(getActivity(), R.string.password_incorrect, Toast.LENGTH_SHORT).show();
			return;
		}
		if("".equals(oldPwd)) {
			Toast.makeText(getActivity(), R.string.input_current_password, Toast.LENGTH_SHORT).show();
			return;
		}
		if(!"".equals(newPwd) && newPwd.length() < Constants.PASSWORD_MIN_LENGTH) {
			Toast.makeText(getActivity(), R.string.password_at_least_six_2, Toast.LENGTH_SHORT).show();
			return;
		}
		
		final Locale current = getResources().getConfiguration().locale;
		final String lang = Util.getLanguage(current.getLanguage());

		mTempCountry = Util.getCountryIsoFromTelephonyManager(getActivity());
		mTempAddress = cityView.getText().toString();
		mTempBirth = birthView.getText().toString() + "-01-01";
		mTempGender = gender.type;
		mTempLanguage = lang;
		final HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_NO, String.valueOf(HeightChartPreference.getInt(Constants.PREF_USER_NO, 0)));
		request.put(Constants.TAG_USER_ID, HeightChartPreference.getString(Constants.PREF_USER_ID, ""));
		request.put(Constants.TAG_COUNTRY, mTempCountry);
		request.put(Constants.TAG_LANGUAGE, mTempLanguage);
		request.put(Constants.TAG_GENDER, Integer.toString(mTempGender));
		request.put(Constants.TAG_BIRTHDAY, mTempBirth);
		request.put(Constants.TAG_OLD_PASSWORD, oldPwd);
		request.put(Constants.TAG_NEW_PASSWORD, newPwd);
		request.put(Constants.TAG_ADDRESS_MAIN, mTempAddress);
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
		
		mModifyMobile = new ModifyMobileProfile(getActivity(), request);
		mModifyMobile.setCallback(this);
		Model.runOnWorkerThread(mModifyMobile);
		mProgressDialog = Util.openProgressDialog(getActivity());
	}
	
	private void modifySnsInfo() {
		final TextView birthView = (TextView) getView().findViewById(R.id.user_detail_edit_birthday);
		final TextView cityView = (TextView) getView().findViewById(R.id.user_detail_edit_city);
		final RadioGroup genderGroup = (RadioGroup) getView().findViewById(R.id.user_detail_select_gender);
		
		final int genderId = genderGroup.getCheckedRadioButtonId();
		final GenderType gender;
		if(genderId == R.id.user_detail_gender_male) {
			gender = GenderType.MALE;
		} else if(genderId == R.id.user_detail_gender_female){
			gender = GenderType.FEMALE;
		} else {
			gender = null;
		}
		
		final Locale current = getResources().getConfiguration().locale;
		final String lang = Util.getLanguage(current.getLanguage());

		mTempCountry = Util.getCountryIsoFromTelephonyManager(getActivity());
		mTempAddress = cityView.getText().toString(); //TODO
		mTempBirth = birthView.getText().toString() + "-01-01";
		mTempGender = gender.type;
		mTempLanguage = lang;
		final HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_NO, String.valueOf(HeightChartPreference.getInt(Constants.PREF_USER_NO, 0)));
		request.put(Constants.TAG_USER_ID, HeightChartPreference.getString(Constants.PREF_USER_ID, ""));
		request.put(Constants.TAG_COUNTRY, mTempCountry);
		request.put(Constants.TAG_LANGUAGE, mTempLanguage);
		request.put(Constants.TAG_GENDER, Integer.toString(mTempGender));
		request.put(Constants.TAG_BIRTHDAY, mTempBirth);
		request.put(Constants.TAG_ADDRESS_MAIN, mTempAddress);
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
		
		RequestModifySNSUserProfile requestTask = new RequestModifySNSUserProfile(getActivity(), request);
		requestTask.setCallback(this);
		Model.runOnWorkerThread(requestTask);
		mProgressDialog = Util.openProgressDialog(getActivity());
	}
	
	private void requestSignupSns() {
		final TextView birthView = (TextView) getView().findViewById(R.id.user_detail_edit_birthday);
		final TextView cityView = (TextView) getView().findViewById(R.id.user_detail_edit_city);
		final RadioGroup genderGroup = (RadioGroup) getView().findViewById(R.id.user_detail_select_gender);
		
		final int genderId = genderGroup.getCheckedRadioButtonId();
		final GenderType gender;
		if(genderId == R.id.user_detail_gender_male) {
			gender = GenderType.MALE;
		} else if(genderId == R.id.user_detail_gender_female){
			gender = GenderType.FEMALE;
		} else {
			gender = null;
		}

		Bundle args = getArguments();
		mTempUserId = args.getString(Constants.TAG_USER_ID, "");
		mTempCountry = args.getString(Constants.TAG_STANDARD_COUNTRY_CODE, "");
		mTempAddress = cityView.getText().toString(); //TODO
		mTempBirth = birthView.getText().toString() + "-01-01";
		mTempGender = gender.type;
		
		HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_ID, mTempUserId);
		request.put(Constants.TAG_PASSWORD, "");
		request.put(Constants.TAG_EMAIL, args.getString(Constants.TAG_EMAIL, ""));
		request.put(Constants.TAG_STANDARD_COUNTRY_CODE, mTempCountry);
		request.put(Constants.TAG_UUID, Util.getUUID(getActivity()));
		request.put(Constants.TAG_ADDRESS_MAIN, mTempAddress);
		request.put(Constants.TAG_BIRTHDAY, mTempBirth);
		request.put(Constants.TAG_GENDER, String.valueOf(mTempGender));
		request.put(Constants.TAG_LANGUAGE, args.getString(Constants.TAG_LANGUAGE, ""));
		request.put(Constants.TAG_SNS_CODE, Constants.SNS_CODE);
		request.put(Constants.TAG_SNS_ID, args.getString(Constants.TAG_SNS_ID, ""));
		request.put(Constants.TAG_SNS_USER_NAME, args.getString(Constants.TAG_SNS_USER_NAME, ""));
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
		
		RequestSignUp requestSignUp = new RequestSignUp(getActivity(), request);
		requestSignUp.setCallback(this);
		Model.runOnWorkerThread(requestSignUp);
		mProgressDialog = Util.openProgressDialog(getActivity());
	}
	
	private void requestSNSLogin() {
		mIsFacebookLogin = true;
		final HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_UUID, Util.getUUID(getActivity()));
		request.put(Constants.TAG_SNS_TYPE, Constants.SNS_CODE);
		request.put(Constants.TAG_SNSID, mTempUserId);

		String registrationId = HeightChartPreference.getString(Constants.PROPERTY_REG_ID, "");
		request.put(Constants.TAG_DEVICE_TOKEN, registrationId);
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
		
		RequestSNSLogin snsLogin = new RequestSNSLogin(getActivity(), request);
		snsLogin.setCallback(this);
		Model.runOnWorkerThread(snsLogin);
	}

	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		DLog.d(TAG, " callback success = " + result);
		//TODO Do it here?
		if(parser instanceof RequestSignUp) {
			if(mMode == SIGN_UP_EMAIL) {
				mTempUserNo = result.get(Constants.TAG_USER_NO);
				
				final HashMap<String, String> request = new HashMap<String, String>();
				request.put(Constants.TAG_USER_ID, mTempUserId);
				request.put(Constants.TAG_PASSWORD, mTempPassword);
				request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);

		   		String registrationId = HeightChartPreference.getString(Constants.PROPERTY_REG_ID, "");

				request.put(Constants.TAG_DEVICE_TOKEN, registrationId);
				request.put(Constants.TAG_UUID, Util.getUUID(getActivity()));
				request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
				
				mRequestAuthKey = new RequestAuthKey(getActivity(), request);
				mRequestAuthKey.setCallback(this);
				Model.runOnWorkerThread(mRequestAuthKey);
			}
			else if(mMode == SIGN_UP_FACEBOOK) {
				requestSNSLogin();
			}
		}
		else if(parser instanceof RequestAuthKey) {
			HeightChartPreference.putString(Constants.PREF_USER_ID, mTempUserId);
			HeightChartPreference.putInt(Constants.PREF_USER_NO, Integer.valueOf(mTempUserNo));
			HeightChartPreference.putString(Constants.PREF_AUTH_KEY, result.get(Constants.TAG_AUTHKEY));
			HeightChartPreference.putString(Constants.PREF_COUNTRY, mTempCountry);
			HeightChartPreference.putString(Constants.PREF_LANGUAGE, mTempLanguage);
			HeightChartPreference.putInt(Constants.PREF_GENDER, mTempGender);
			HeightChartPreference.putString(Constants.PREF_BIRTHDAY, mTempBirth);
			HeightChartPreference.putString(Constants.PREF_ADDRESS_MAIN, mTempAddress);
			HeightChartPreference.putInt(Constants.PREF_LOGIN_TYPE, Constants.LOGIN_TYPE_EMAIL);
			getFragmentManager().popBackStack();
			mProgressDialog.dismiss();
			
            //longzhe cui added
			JSONObject jsonObj = new JSONObject();
			try {
				jsonObj.put("Authentication", "Email");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mMixpanel.track("Signup Success", jsonObj);

			mMixpanel.identify(mTempUserId);
			mMixpanel.getPeople().identify(mTempUserId);
			mMixpanel.getPeople().set("$email", mTempUserId);
			mMixpanel.getPeople().set("city", mTempCountry);
			mMixpanel.getPeople().set("birthYear", mTempBirth);
			mMixpanel.getPeople().set("genType", mTempGender);
			mMixpanel.getPeople().set("Deleted", "false");
			mMixpanel.getPeople().initPushHandling(Util.GCM_SENDERID);
            //longzhe cui added end
		}
		else if(parser instanceof ModifyMobileProfile) {
			HeightChartPreference.putString(Constants.PREF_COUNTRY, mTempCountry);
			HeightChartPreference.putString(Constants.PREF_LANGUAGE, mTempLanguage);
			HeightChartPreference.putInt(Constants.PREF_GENDER, mTempGender);
			HeightChartPreference.putString(Constants.PREF_BIRTHDAY, mTempBirth);
			HeightChartPreference.putString(Constants.PREF_ADDRESS_MAIN, mTempAddress);
			getFragmentManager().popBackStack(Constants.TAG_FRAGMENT_LOGIN_BEFORE_OPTION, 0);
			Toast.makeText(getActivity(), getResources().getString(R.string.modified), Toast.LENGTH_SHORT).show();
			mProgressDialog.dismiss();
		} else if(parser instanceof RequestModifySNSUserProfile) {
			HeightChartPreference.putString(Constants.PREF_COUNTRY, mTempCountry);
			HeightChartPreference.putString(Constants.PREF_LANGUAGE, mTempLanguage);
			HeightChartPreference.putInt(Constants.PREF_GENDER, mTempGender);
			HeightChartPreference.putString(Constants.PREF_BIRTHDAY, mTempBirth);
			HeightChartPreference.putString(Constants.PREF_ADDRESS_MAIN, mTempAddress);
			getFragmentManager().popBackStack(Constants.TAG_FRAGMENT_LOGIN_BEFORE_OPTION, 0);
			Toast.makeText(getActivity(), getResources().getString(R.string.modified), Toast.LENGTH_SHORT).show();
			mProgressDialog.dismiss();
		}
		else if(parser instanceof RequestSNSLogin) {
			DLog.e(TAG, "RequestAuthKey");
			mTempUserNo = result.get(Constants.TAG_USER_NO);
			mTempAuthKey = result.get(Constants.TAG_AUTHKEY);
			
			final HashMap<String, String> request = new HashMap<String, String>();
			request.put(Constants.TAG_USER_NO, mTempUserNo);
			request.put(Constants.TAG_AUTHKEY, mTempAuthKey);
			request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
			
			GetMobileProfile getMobileProfile = new GetMobileProfile(getActivity(), request);
			getMobileProfile.setCallback(this);
			Model.runOnWorkerThread(getMobileProfile);
			
		}
		else if(parser instanceof GetMobileProfile) {
			HeightChartPreference.putString(Constants.PREF_USER_ID, mTempUserId);
			HeightChartPreference.putInt(Constants.PREF_USER_NO, Integer.valueOf(mTempUserNo));
			HeightChartPreference.putString(Constants.PREF_AUTH_KEY, mTempAuthKey);
			HeightChartPreference.putBoolean(Constants.PREF_EMAIL_VALIFICATION, Boolean.valueOf(result.get(Constants.TAG_EMAIL_VALIFICATION)));
			HeightChartPreference.putString(Constants.PREF_EMAIL_ACTIVATE_DATE_TIME, result.get(Constants.TAG_EMAIL_ACTIVATE_DATE_TIME));
			HeightChartPreference.putString(Constants.PREF_CHANGE_PASSWROD_DATE_TIME, result.get(Constants.TAG_CHANGE_PASSWROD_DATE_TIME));
			HeightChartPreference.putString(Constants.PREF_REGISTER_DATE_TIME, result.get(Constants.TAG_REGISTER_DATE_TIME));
			HeightChartPreference.putString(Constants.PREF_COUNTRY, result.get(Constants.TAG_COUNTRY));
			HeightChartPreference.putString(Constants.PREF_LANGUAGE, result.get(Constants.TAG_LANGUAGE));
			HeightChartPreference.putInt(Constants.PREF_GENDER, Integer.valueOf(result.get(Constants.TAG_GENDER)));
			HeightChartPreference.putString(Constants.PREF_BIRTHDAY, result.get(Constants.TAG_BIRTHDAY));
			HeightChartPreference.putString(Constants.PREF_ADDRESS_MAIN, result.get(Constants.TAG_ADDRESS_MAIN));
			HeightChartPreference.putInt(Constants.PREF_LOGIN_TYPE, mIsFacebookLogin?Constants.LOGIN_TYPE_FACEBOOK:Constants.LOGIN_TYPE_EMAIL);
			
			getFragmentManager().popBackStackImmediate();
			mProgressDialog.dismiss();
		}
	}

	@Override
	public void onFailed(ServerTask parser, Map<String, String> result, int returnCode) {
		DLog.d(TAG, " callback fail = " + result);
		Toast.makeText(getActivity(), Util.switchErrorCode(returnCode, getResources()), Toast.LENGTH_SHORT).show();
		mProgressDialog.dismiss();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(resultCode != Activity.RESULT_OK) 
			return;
		
		switch (requestCode) {
		case Constants.REQUEST_CODE_DATE_DIALOG:
			TextView birthText = (TextView) getView().findViewById(R.id.user_detail_edit_birthday);
			final int year = data.getIntExtra(BirthDayPickerDialog.EXTRA_YEAR, 0);
			birthText.setText(Integer.toString(year));
			break;
			
		case Constants.REQUEST_CODE_AGREE_PERSONAL:
			boolean isNext = data.getBooleanExtra(Constants.CLICK_NEXT, false);
			mIsCheckedNext = isNext;
			break;
		case Constants.REQUEST_USER_CONFIRM:
			if(resultCode == Activity.RESULT_OK)
				getFragmentManager().popBackStackImmediate();
			break;
		case Constants.REQUEST_CITY:
			if(resultCode == Activity.RESULT_OK) {
				String city = data.getStringExtra(Constants.CITY);
				TextView cityText = (TextView) getView().findViewById(R.id.user_detail_edit_city);
				cityText.setText(city);
			}
		default:
			break;
		}
	}
}
