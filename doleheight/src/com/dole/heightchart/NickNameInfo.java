package com.dole.heightchart;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.dole.heightchart.Constants.Characters;
import com.dole.heightchart.TableColumns.Nickname;

import java.util.ArrayList;
import java.util.List;

public class NickNameInfo {
	
	public static final int ADDNICK_OWL_ID = -1;
	
	long mNickNameId;
	String mNickName;

	Characters mCharacter;
	String mBirthday;
	String mGender;
	ArrayList<HeightInfo> mHeightList;
	float mLastHeight;
	private static Object sLock = new Object();
	
	public static NickNameInfo loadFromCursor(Context context, Cursor c) {
		NickNameInfo info = new NickNameInfo();
		info.mNickNameId = c.getLong(c.getColumnIndex(TableColumns.Nickname._ID));
		info.mNickName = c.getString(c.getColumnIndex(TableColumns.Nickname.NICKNAME));
		int characterIdx = c.getInt(c.getColumnIndex(TableColumns.Nickname.CHARACTER));
		info.mCharacter = Characters.getCharacterAt(characterIdx);
		info.mBirthday = c.getString(c.getColumnIndex(TableColumns.Nickname.BIRTHDAY));
		info.mGender = c.getString(c.getColumnIndex(TableColumns.Nickname.GENDER));
		info.mHeightList = new ArrayList<HeightInfo>();
		
		Cursor cursor = context.getContentResolver().query(TableColumns.Height.URI, null, TableColumns.Height.NICK_ID + "=?", new String[]{String.valueOf(info.mNickNameId)}, null);
		synchronized (sLock) {
			while(cursor.moveToNext()) {
				HeightInfo height = HeightInfo.loadFromCursor(info, cursor);
				info.mHeightList.add(height);
			}
		}
		info.computeLastHeight();
		
		return info;
	}

	public static NickNameInfo insert(Context context, String name, Characters character, String birth, String genderText) {
		int characterId = character.getCharacterIndex(); 
		if(characterId < 0)
			return null;
		ContentValues values = new ContentValues();
		values.put(Nickname.NICKNAME, name);
		values.put(Nickname.CHARACTER, characterId);
		values.put(Nickname.BIRTHDAY, birth);
		values.put(Nickname.GENDER, genderText);
		
		Uri uri = context.getContentResolver().insert(TableColumns.Nickname.URI, values);
		
		if(uri == null)
			return null;
		
		NickNameInfo info = new NickNameInfo();
		info.mNickNameId = ContentUris.parseId(uri);
		info.mNickName = name;
		info.mCharacter = Characters.getCharacterAt(characterId);
		info.mBirthday = values.getAsString(Nickname.BIRTHDAY);
		info.mGender = values.getAsString(Nickname.GENDER);
		info.mHeightList = new ArrayList<HeightInfo>();
		info.mLastHeight = 0f;
		return info;
	}

	public static int update(Context context, String name, Characters character, String birth, String genderText, long nickNameId) {
		int characterId = character.getCharacterIndex(); 
		if(characterId < 0)
			return 0;
		ContentValues values = new ContentValues();
		values.put(Nickname.NICKNAME, name);
		values.put(Nickname.CHARACTER, characterId);
		values.put(Nickname.BIRTHDAY, birth);
		values.put(Nickname.GENDER, genderText);
		
		int update = context.getContentResolver().update(TableColumns.Nickname.URI, values,
				TableColumns.Nickname._ID + "=?", new String[]{String.valueOf(nickNameId)});
		
		return update;
	}
	
	public void removeHeightInfos(List<HeightInfo> heightInfos) {
		synchronized (sLock) {
			if(mHeightList != null)
				mHeightList.removeAll(heightInfos);
		}
		computeLastHeight();
	}
	
	private void computeLastHeight() {
		mLastHeight = 0f;
		synchronized (sLock) {
			for(HeightInfo info : mHeightList) {
				if(info.mHeight > mLastHeight)
					mLastHeight = info.mHeight;
			}
		}
	}
	
	public void addHeight(HeightInfo heightInfo) {
		synchronized (sLock) {
			if(mHeightList == null)
				mHeightList = new ArrayList<HeightInfo>();
			mHeightList.add(heightInfo);	
		}
		computeLastHeight();
	}

	public long getNickNameId() {
		return mNickNameId;
	}
	
	public void setNickNameId(int id) {
		mNickNameId = id;
	}

	public String getNickName() {
		return mNickName;
	}

	public void setNickName(String nickName) {
		this.mNickName = nickName;
	}

	public ArrayList<HeightInfo> getHeightList() {
		return mHeightList;
	}

	public void setHeightList(ArrayList<HeightInfo> heightList) {
		this.mHeightList = heightList;
	}

	public float getLastHeight() {
		return mLastHeight;
	}

	public void setLastHeight(float lastHeight) {
		this.mLastHeight = lastHeight;
	}
	
	public Characters getCharacter() {
		return mCharacter;
	}
	
	public void setCharacter(Characters character) {
		mCharacter = character;
	}
	
	public String getGender() {
		return mGender;
	}
	
	public String getBirthday() {
		return mBirthday;
	}
	
}
