package com.dole.heightchart.server;

import android.content.Context;

import com.dole.heightchart.DLog;
import com.dole.heightchart.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class RequestUseQRCode extends ServerTask {

	public RequestUseQRCode(Context context, Map<String, String> request) {
		super(context, request);
	}
	
	@Override
	protected String applyApiDetail() {
		return getContext().getResources().getString(R.string.api_usecoupon);
	}

	@Override
	public Map<String, String> parseResponse(String jsonResult, IServerResponseCallback callback) {
		DLog.d(TAG, " json_res = " + jsonResult);
		final HashMap<String, String> result = new HashMap<String, String>();
		JSONObject jsonObj;
		try {
			jsonObj = new JSONObject(jsonResult);
//			jsonObj = (JSONObject) jsonObj.get("CheckMobileCouponResult");
			
			Iterator<String> iter = jsonObj.keys();
			while(iter.hasNext()){
				String key = (String)iter.next();
				String value = jsonObj.getString(key);
				result.put(key,value);
			}
		} catch (JSONException e1) {
			DLog.d(TAG, " json parse error! ", e1);
		}

		return result;
	}
}
