package com.dole.heightchart.camera;

import android.app.Activity;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.Area;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLayoutChangeListener;
import android.widget.ImageView;

import com.dole.heightchart.DLog;
import com.dole.heightchart.R;
import com.dole.heightchart.Util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class CameraManager implements TextureView.SurfaceTextureListener, LocationManager.Listener, SensorEventListener {
    
    private static final float MAX_EXPOSURE_COMPENSATION = 1.5f;
    private static final float MIN_EXPOSURE_COMPENSATION = 0.0f;
    private static final int MIN_FPS = 10;
    private static final int MAX_FPS = 20;
    private static final int RESUME_QRCODE_FOCUS_CALLBACK = 3000;
	
	private static final String TAG = "CameraManager";
	private static final float SHAKE_THRESHOLD = 3f;
	
	private Camera mCameraDevice;
	private Activity mActivity;
	private View mRootView;

	private int mDisplayOrientation;

	private boolean mIsMeteringAreasSupported = false;
	private LocationManager mLocationManager;
	private int mCameraId = 0;
	private CameraMode mMode;
	
	private PictureCallback mPictureCallback;
	private PreviewCallback mPreviewCallback;
	private AutoFocusCallback mAutoFocusCallback;
	
	private final SensorManager mSensorManager;
    private final Sensor mAccelerometer;
    
    private boolean mIsInitFinished = false;
    private boolean mIsPreviewing = false;
    private boolean mIsFocused = false;
    
    private boolean mReopenCameraOnResume = false;
    
    private TextureView mTextureView;
    private int mPreviewWidth = 0;
    private int mPreviewHeight = 0;
    //RectF previewRect = new RectF();
	
	private Matrix mMatrix = null;
	private Matrix mConvertMatrix = null;
    private float mAspectRatio = 16f / 9f;
    private float mSurfaceTextureUncroppedWidth;
    private float mSurfaceTextureUncroppedHeight;
    
    private View mFocusView;
    private RectF mFocusRect;
    
    private SurfaceTexture mSurfaceTexture;
    
    private boolean mIsPausePreviewCallback;
    
    /**
     * Accelerometers
     */
	private long mLastUpdateTime = 0;
	private float mLastZ = 0f;
	private float mAccumulatedZDiff = 0f;
	
	public enum CameraMode {
		QRCODE,
		RECORD_HEIGHT,
	}
	
	public CameraManager(Activity activity, View rootView, CameraMode mode, View focusView) {
		mActivity = activity;
		mMode = mode;
		
		mSensorManager = (SensorManager)activity.getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        
        initCamera(rootView, focusView);
	}
	
	public void initCamera(View rootView, View focusView) {
		mRootView = rootView;
		mFocusView = focusView;
		final CameraHolder cameraHolder = CameraHolder.instance();
		mCameraId = cameraHolder.getBackCameraId();
		mCameraDevice = cameraHolder.tryOpen(mCameraId);
		
		mLocationManager = new LocationManager(mActivity, this);

		mTextureView = (TextureView) mRootView.findViewById(R.id.camera_preview);
		SurfaceTexture surfaceTexture = mTextureView.getSurfaceTexture();
		if(surfaceTexture == null) {
			mTextureView.setSurfaceTextureListener(this);
			mTextureView.addOnLayoutChangeListener(new OnLayoutChangeListener() {
				@Override
				public void onLayoutChange(View v, int left, int top, int right,
						int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
					int width = right - left;
					int height = bottom - top;
					if (mPreviewWidth != width || mPreviewHeight != height) {
						mPreviewWidth = width;
						mPreviewHeight = height;
						DLog.d(TAG, " mPrev w = " + mPreviewWidth + " h = " + mPreviewHeight);
						setTransformMatrix(width, height);
					}
				}
			});
		} else {
			mSurfaceTexture = mTextureView.getSurfaceTexture();
			try {
				mCameraDevice.setPreviewTexture(mSurfaceTexture);
			} catch (IOException e) {
				DLog.e(TAG, " setPreviewTexture", e);
			}
			
			if(mFocusView != null)
				mFocusView.setSelected(false);

			initPreviewSize();
			startPreview();
		}
		
		Parameters param = mCameraDevice.getParameters();
		param.setExposureCompensation(0);
		mDisplayOrientation = Util.getCameraDisplayOrientation(mActivity, mCameraId, mCameraDevice);
		mCameraDevice.setDisplayOrientation(mDisplayOrientation);
		param.setAntibanding(Parameters.ANTIBANDING_OFF);

		//Set param to camera
		setCameraParameter(param);
		
		mReopenCameraOnResume = false;
	}
	
	public boolean isFocused() {
		return mIsFocused;
	}
	
	public void setOnTakeButtonClickListener() {
		if(mMode == CameraMode.RECORD_HEIGHT) {
        	final ImageView shutterBtn = (ImageView) mRootView.findViewById(R.id.record_height_shutter_btn);
        	shutterBtn.setOnClickListener(mShutterBtnClickListener);
        }
	}
	
	public int getCameraId() {
		return mCameraId;
	}
	
	public void releaseCamera() {
		if(mCameraDevice != null) {
			stopPreview();
			closeCamera();
		}
	}
	
	public boolean isCameraReleased() {
		return mCameraDevice == null;
	}
	
	public boolean isPreviewing() {
		return mIsPreviewing;
	}
	
	public void setCallback(PreviewCallback previewCallback, PictureCallback pictureCallback) {
		mPreviewCallback = previewCallback;
		mPictureCallback = pictureCallback;
	}
	
	public void pausePreview(boolean pausePreviewCallback) {
		mIsPausePreviewCallback = pausePreviewCallback;
		mIsPreviewing = !pausePreviewCallback;
		
		if(!mIsPausePreviewCallback) 
			resumeQrcodeAutoFocus();
	}
	
	public void resumeQrcodeAutoFocus() {
		final View view = mTextureView;
		if(view == null)
			return;
		
		view.getHandler().removeCallbacks(focusRunnable);
		if(view != null) {
			view.postDelayed(focusRunnable, RESUME_QRCODE_FOCUS_CALLBACK);
		}
	}
	
	private Runnable focusRunnable = new Runnable() {
		@Override
		public void run() {
			if(!isCameraReleased()) {
				mIsFocused = false;
				mCameraDevice.autoFocus(mAutoFocusCallback);
				if(mFocusView != null)
					mFocusView.setSelected(false);
			}
		}
	};
	
	public void startOneShotPreviewCallbackLoop() {
		if(mCameraDevice != null)
			mCameraDevice.setOneShotPreviewCallback(mPreviewCallback);
	}

	private AutoFocusCallback mQRCodeAutoFocusCallback = new AutoFocusCallback() {
		@Override
		public void onAutoFocus(boolean success, Camera camera) {
			if(mFocusView != null)
				mFocusView.setSelected(success);
			
			mIsFocused = success;
			
			if(mPreviewCallback != null) {
				camera.setOneShotPreviewCallback(mPreviewCallback);
			}
			
			resumeQrcodeAutoFocus();
		}
	};
	
	private AutoFocusCallback mRecordHeightAutoFocusCallback = new AutoFocusCallback() {
		@Override
		public void onAutoFocus(boolean success, Camera camera) {
			mIsFocused = success;
			if(mIsFocused)
				mSensorManager.registerListener(CameraManager.this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
			if(mFocusView != null)
				mFocusView.setSelected(success);
			
			if(!success) {
				mRootView.postDelayed(new Runnable() {

					@Override
					public void run() {
						if(mCameraDevice != null) {
							mIsFocused = false;
							mCameraDevice.autoFocus(mRecordHeightAutoFocusCallback);
							
						}
					}
				}, 300);
			}
		}
	};
	
	private void closeCamera() {
		mSensorManager.unregisterListener(this);
        if (mCameraDevice != null) {
        	mCameraDevice.cancelAutoFocus();
            mCameraDevice.setZoomChangeListener(null);
            mCameraDevice.setFaceDetectionListener(null);
            mCameraDevice.setErrorCallback(null);
            CameraHolder.instance().release();
            mCameraDevice = null;
        }
    }
	
	private void initFocus() {

		if(null != mCameraDevice) {
			mCameraDevice.cancelAutoFocus();

			String focusMode = null;
			final Parameters parameters = mCameraDevice.getParameters();
			final List<String> focusModes = parameters.getSupportedFocusModes();

			if(focusModes.contains(Parameters.FOCUS_MODE_MACRO))
				focusMode = Parameters.FOCUS_MODE_MACRO;
			else if(focusModes.contains(Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
				focusMode = Parameters.FOCUS_MODE_CONTINUOUS_PICTURE;
			} else {
				focusMode = Parameters.FOCUS_MODE_AUTO;
			}
			
			mAutoFocusCallback = null;

			switch (mMode) {
			case QRCODE:
				mAutoFocusCallback = mQRCodeAutoFocusCallback;
				break;
			case RECORD_HEIGHT:
				mAutoFocusCallback = mRecordHeightAutoFocusCallback;
				break;
			default:
				break;
			}
			
			int[] location = new int[2]; 
			mFocusView.getLocationInWindow(location);

			Rect previewRect = new Rect(0, 0, mPreviewWidth, mPreviewHeight);
			Matrix matrix = new Matrix();
			Util.prepareMatrix(matrix, mDisplayOrientation, previewRect);
			RectF rect = new RectF(
					(int)location[0], (int)location[1], 
					(int)(location[0] + mFocusView.getWidth()), (int)(location[1] + mFocusView.getHeight()));
			
			mConvertMatrix = new Matrix();
			mConvertMatrix.preRotate(-90, 0, 0);
			mConvertMatrix.postConcat(mMatrix);
			//mConvertMatrix.postTranslate(mPreviewHeight, 0);
			mConvertMatrix.postTranslate(0, mPreviewWidth);
			
			mFocusRect = new RectF(rect); //(int) rect.left, (int) rect.top, (int) rect.right, (int) rect.bottom); // Copy
			mConvertMatrix.mapRect(mFocusRect);
					
			matrix.mapRect(rect);
			
			final List<Area> areas = new ArrayList<Area>();
			areas.add(new Camera.Area(new Rect((int)rect.left, (int)rect.top, (int)rect.right, (int)rect.bottom), 100));
			parameters.setFocusAreas(areas);
			parameters.setFocusMode(focusMode);

			if (mIsMeteringAreasSupported) {
				parameters.setMeteringAreas(areas);
			}

			setCameraParameter(parameters);
			mCameraDevice.autoFocus(mAutoFocusCallback);
			mIsFocused = false;
		}
		mIsInitFinished = true;
	}

	private void initPreviewSize() {
		final Size size = initPictureSize();
        final Parameters parameters = mCameraDevice.getParameters();
        List<Size> sizes = parameters.getSupportedPreviewSizes();
        
        Size optimalSize = Util.getOptimalPreviewSize(mActivity,
                sizes, mAspectRatio); //(double) size.width / size.height);
        Size original =  parameters.getPreviewSize();
        if (!original.equals(optimalSize)) {
        	parameters.setPreviewSize(optimalSize.width, optimalSize.height);
        } else {
        	parameters.setPreviewSize(sizes.get(0).width, sizes.get(0).height);
        }
        DLog.d(TAG, " size x = " + original.width + " y = " + original.height);
        setCameraParameter(parameters);
	}
	
	private Size initPictureSize() {
		final Parameters parameters = mCameraDevice.getParameters();
		final List<Size> supported = parameters.getSupportedPictureSizes();
		if (supported == null) 
			return null;
		Size maxSize = supported.get(0);
		for(Size size : supported) {
			float aspectRatio = (float)size.width / (float)size.height;
			
			if(Math.round(aspectRatio * 100) == Math.round(mAspectRatio * 100) && size.width > maxSize.width || size.height > maxSize.height)
				maxSize = size;
		}
		parameters.setPictureSize(maxSize.width, maxSize.height);
		setCameraParameter(parameters);

		return maxSize;
	}
	
	public void onResume() {
		if(mReopenCameraOnResume)
			initCamera(mRootView, mFocusView);
	}
	
	public void onPause() {
		mReopenCameraOnResume = !isCameraReleased();
		if(mCameraDevice != null) {
			stopPreview();
			releaseCamera();
		}
		mSensorManager.unregisterListener(this);
	}
	
	public Point getPreviewResolution() {
		Point resolution = new Point(0, 0);
		Camera.Size previewSize = mCameraDevice.getParameters().getPreviewSize();
		resolution.set(previewSize.height, previewSize.width);
		return resolution;
	}
	
	public synchronized RectF getFramingRectInPreview() {
		return mFocusRect;
	}
	
	public Matrix getPreviewConvertMatrix() {
		return mConvertMatrix;
	}
	
	public void convertRectInPreview(RectF original) {
		mConvertMatrix.mapRect(original);
	}
	
	private void startPreview() {
		try {
			mCameraDevice.startPreview();
		} catch (Throwable ex) {
			closeCamera();
			throw new RuntimeException("startPreview failed", ex);
		}
		mIsPreviewing = true;
		mIsPausePreviewCallback = false;
		
		mTextureView.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				initFocus();
				
			}
		}, 500);
		
    }
	
	private void stopPreview() {
		mIsPreviewing = false;
        if (mCameraDevice != null) { // && mCameraState != PREVIEW_STOPPED) {
            mCameraDevice.cancelAutoFocus(); // Reset the focus.
            mCameraDevice.stopPreview();
        }
    }
	
	private void setTransformMatrix(int width, int height) {
        mMatrix = mTextureView.getTransform(mMatrix);
        float scaleX = 1f, scaleY = 1f;
        float scaledTextureWidth, scaledTextureHeight;
        
        float aspectRatio = (float) mPreviewHeight / (float) mPreviewWidth; //(float) height / (float) width;
        
        if (width > height) {
            scaledTextureWidth = Math.max(width, (int) (height * aspectRatio));
            scaledTextureHeight = Math.max(height, (int)(width / aspectRatio));
            DLog.d(TAG, " width > height");
        } else {
            scaledTextureWidth = Math.max(width, (int) (height / aspectRatio));
            scaledTextureHeight = Math.max(height, (int) (width * aspectRatio));
            DLog.d(TAG, " width <= height");
        }
        DLog.d(TAG, " scaled tw = " + scaledTextureWidth + " th = " + scaledTextureHeight);

        if (mSurfaceTextureUncroppedWidth != scaledTextureWidth ||
                mSurfaceTextureUncroppedHeight != scaledTextureHeight) {
            mSurfaceTextureUncroppedWidth = scaledTextureWidth;
            mSurfaceTextureUncroppedHeight = scaledTextureHeight;
        }
        scaleX = scaledTextureWidth / width;
        scaleY = scaledTextureHeight / height;
        DLog.d(TAG, " scaleX = " + scaleX + " scaleY = " + scaleY);
        mMatrix.setScale(scaleX , scaleY, (float) width / 2, (float) height / 2);
        mTextureView.setTransform(mMatrix);
    }
	
	private OnClickListener mShutterBtnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
	        final Parameters parameters = mCameraDevice.getParameters();
            Location loc = mLocationManager.getCurrentLocation();
            Util.setGpsParameters(parameters, loc);
            parameters.setRotation(0);
            setCameraParameter(parameters);
            if(mIsPreviewing && mIsFocused && mPictureCallback != null) {
            	mIsPreviewing = false;
            	v.setOnClickListener(null);
            	mCameraDevice.takePicture(null, null, null, mPictureCallback);
            }
		}
	};
	
	private void setCameraParameter(Parameters param) {
        if(mCameraDevice == null) {
        	return;
        }
        try {
    		mCameraDevice.setParameters(param);
        }
        catch(Exception e) {
        	DLog.d(TAG, "set parameter fail");
        }
	}

	@Override
	public void showGpsOnScreenIndicator(boolean hasSignal) {
	}

	@Override
	public void hideGpsOnScreenIndicator() {
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	@Override
	public void onSensorChanged(SensorEvent event)  {
		
		if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			
			long curTime = System.currentTimeMillis();

			if ((curTime - mLastUpdateTime) > 100) {
				long diffTime = (curTime - mLastUpdateTime);
				mLastUpdateTime = curTime;
				
				float[] values = event.values;
				float z = values[2];

				float speed =  (z - mLastZ) / diffTime * 1000;
				mAccumulatedZDiff += speed;
				mLastZ = z;
				
				if(Math.abs(mAccumulatedZDiff) > SHAKE_THRESHOLD) {
					mAccumulatedZDiff = 0;
					if(!isCameraReleased() && mIsPreviewing) {
						mIsFocused = false;
						if(mMode == CameraMode.QRCODE)
							resumeQrcodeAutoFocus();
						else
							mCameraDevice.autoFocus(mAutoFocusCallback);	
						if(mFocusView != null)
							mFocusView.setSelected(false);
					}
					mSensorManager.unregisterListener(this);
				}
			}
		}
	}

	@Override
	public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
		if (mCameraDevice == null) 
			return;
		
		mSurfaceTexture = surface;
		
        Parameters p = mCameraDevice.getParameters();
        if (p.getMaxNumMeteringAreas() > 0) {
           mIsMeteringAreasSupported = true;
        }
		
        initPreviewSize();
        setTransformMatrix(width, height);
        try {
			mCameraDevice.setPreviewTexture(surface);
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        startPreview();
	}

	@Override
	public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
		releaseCamera();
		return true;
	}

	@Override
	public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
		setTransformMatrix(width, height);
	}

	@Override
	public void onSurfaceTextureUpdated(SurfaceTexture surface) {

	}
	
	public void setBestExposure(boolean lightOn) {
		final Camera.Parameters parameters = mCameraDevice.getParameters();
		int minExposure = parameters.getMinExposureCompensation();
		int maxExposure = parameters.getMaxExposureCompensation();
		float step = parameters.getExposureCompensationStep();
		if ((minExposure != 0 || maxExposure != 0) && step > 0.0f) {
			// Set low when light is on
			float targetCompensation = lightOn ? MIN_EXPOSURE_COMPENSATION : MAX_EXPOSURE_COMPENSATION;
			int compensationSteps = Math.round(targetCompensation / step);
			float actualCompensation = step * compensationSteps;
			// Clamp value:
			compensationSteps = Math.max(Math.min(compensationSteps, maxExposure), minExposure);
			if (parameters.getExposureCompensation() == compensationSteps) {
				DLog.i(TAG, "Exposure compensation already set to " + compensationSteps + " / " + actualCompensation);
			} else {
				DLog.i(TAG, "Setting exposure compensation to " + compensationSteps + " / " + actualCompensation);
				parameters.setExposureCompensation(compensationSteps);
			}
		} else {
			DLog.i(TAG, "Camera does not support exposure compensation");
		}
		mCameraDevice.setParameters(parameters);
	}

	public void setBestPreviewFPS() {
		setBestPreviewFPS(MIN_FPS, MAX_FPS);
	}

	public void setBestPreviewFPS(int minFPS, int maxFPS) {
		final Camera.Parameters parameters = mCameraDevice.getParameters();
		List<int[]> supportedPreviewFpsRanges = parameters.getSupportedPreviewFpsRange();
		DLog.i(TAG, "Supported FPS ranges: " + toString(supportedPreviewFpsRanges));
		if (supportedPreviewFpsRanges != null && !supportedPreviewFpsRanges.isEmpty()) {
			int[] suitableFPSRange = null;
			for (int[] fpsRange : supportedPreviewFpsRanges) {
				int thisMin = fpsRange[Camera.Parameters.PREVIEW_FPS_MIN_INDEX];
				int thisMax = fpsRange[Camera.Parameters.PREVIEW_FPS_MAX_INDEX];
				if (thisMin >= minFPS * 1000 && thisMax <= maxFPS * 1000) {
					suitableFPSRange = fpsRange;
					break;
				}
			}
			if (suitableFPSRange == null) {
				DLog.i(TAG, "No suitable FPS range?");
			} else {
				int[] currentFpsRange = new int[2];
				parameters.getPreviewFpsRange(currentFpsRange);
				if (Arrays.equals(currentFpsRange, suitableFPSRange)) {
					DLog.i(TAG, "FPS range already set to " + Arrays.toString(suitableFPSRange));
				} else {
					DLog.i(TAG, "Setting FPS range to " + Arrays.toString(suitableFPSRange));
					parameters.setPreviewFpsRange(suitableFPSRange[Camera.Parameters.PREVIEW_FPS_MIN_INDEX],
							suitableFPSRange[Camera.Parameters.PREVIEW_FPS_MAX_INDEX]);
				}
			}
		}
		mCameraDevice.setParameters(parameters);
	}

	public void setBarcodeSceneMode() {
		final Camera.Parameters parameters = mCameraDevice.getParameters();
		if (Camera.Parameters.SCENE_MODE_BARCODE.equals(parameters.getSceneMode())) {
			DLog.i(TAG, "Barcode scene mode already set");
			return;
		}
		String sceneMode = findSettableValue("scene mode",
				parameters.getSupportedSceneModes(),
				Camera.Parameters.SCENE_MODE_BARCODE);
		if (sceneMode != null) {
			parameters.setSceneMode(sceneMode);
		}
		mCameraDevice.setParameters(parameters);
	}
	
	private static String toString(Collection<int[]> arrays) {
        if (arrays == null || arrays.isEmpty()) {
            return "[]";
        }
        StringBuilder buffer = new StringBuilder();
        buffer.append('[');
        Iterator<int[]> it = arrays.iterator();
        while (it.hasNext()) {
            buffer.append(Arrays.toString(it.next()));
            if (it.hasNext()) {
                buffer.append(", ");
            }
        }
        buffer.append(']');
        return buffer.toString();
    }

	private static String findSettableValue(String name,
			Collection<String> supportedValues,
			String... desiredValues) {
		DLog.i(TAG, "Requesting " + name + " value from among: " + Arrays.toString(desiredValues));
		DLog.i(TAG, "Supported " + name + " values: " + supportedValues);
		if (supportedValues != null) {
			for (String desiredValue : desiredValues) {
				if (supportedValues.contains(desiredValue)) {
					DLog.i(TAG, "Can set " + name + " to: " + desiredValue);
					return desiredValue;
				}
			}
		}
		DLog.i(TAG, "No supported values match");
		return null;
	}
}
