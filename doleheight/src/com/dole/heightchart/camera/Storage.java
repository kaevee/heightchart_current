/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dole.heightchart.camera;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Images.ImageColumns;

import com.dole.heightchart.DLog;
import com.dole.heightchart.HeightInfo;
import com.dole.heightchart.HeightInfo.FaceImageMode;
import com.dole.heightchart.NickNameInfo;
import com.dole.heightchart.TableColumns;
import com.dole.heightchart.Util;

import java.io.File;
import java.util.Date;

public class Storage {
    private static final String TAG = "CameraStorage";

    public static final String PICTURE = 
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();

    public static final String IMAGE_DIRECTORY = PICTURE + "/HeightChart";

    public static final String BUCKET_ID =
            String.valueOf(IMAGE_DIRECTORY.toLowerCase().hashCode());

    public static final long UNAVAILABLE = -1L;
    public static final long PREPARING = -2L;
    public static final long UNKNOWN_SIZE = -3L;
    public static final long LOW_STORAGE_THRESHOLD= 50000000;
    public static final long PICTURE_SIZE = 1500000;
    
    public static final String POSTFIX_LEFT = "_left";
    public static final String POSTFIX_LEFT_NO_ARROW = "_left_no_arrow";
    public static final String POSTFIX_RIGHT = "_right";
    public static final String POSTFIX_RIGHT_NO_ARROW = "_right_no_arrow";
    public static final String POSTFIX_CHART = "_chart";
    
    public static final String IMAGE_EXTENSION  = ".jpg";
    public static final String IMAGE_FACE_EXTENSION  = ".png";

    public static Uri addNewHeight(Activity activity, NickNameInfo nickInfo, HeightInfo heightInfo, Bitmap src, int frameId) {
    	final ContentResolver resolver = activity.getContentResolver();
    	final Resources res = activity.getResources();
    			
    	// Current time
        final Date date = new Date();
        final long currentTimeStamp = date.getTime();
        
        // Create directory if not exist
        final File directory = new File(IMAGE_DIRECTORY);
        if(directory == null || !directory.isDirectory()) {
        	directory.mkdir();
        }
        
        final String imageFilename = generateFilename(String.valueOf(nickInfo.getNickNameId()), currentTimeStamp);

        Bitmap faceImage = null;
        // Create Left image
        faceImage = Util.createFaceImageWithCircleMask(
        		activity, 
        		src, 
        		FaceImageMode.LEFT);
        Util.compressBitmapToPng(faceImage, new File(activity.getFilesDir(), imageFilename + POSTFIX_LEFT + IMAGE_FACE_EXTENSION));
        faceImage.recycle();
        
        // Create Left no arrow image
        faceImage = Util.createFaceImageWithCircleMask(
        		activity, 
        		src, 
        		FaceImageMode.LEFT_NO_ARROW);
        Util.compressBitmapToPng(faceImage, new File(activity.getFilesDir(), imageFilename + POSTFIX_LEFT_NO_ARROW + IMAGE_FACE_EXTENSION));
        faceImage.recycle();

        // Create Right image
        faceImage = Util.createFaceImageWithCircleMask(
        		activity, 
        		src, 
        		FaceImageMode.RIGHT);
        Util.compressBitmapToPng(faceImage, new File(activity.getFilesDir(), imageFilename + POSTFIX_RIGHT + IMAGE_FACE_EXTENSION));
        faceImage.recycle();
        
        // Create Right no arrow image
        faceImage = Util.createFaceImageWithCircleMask(
        		activity, 
        		src, 
        		FaceImageMode.RIGHT_NO_ARROW);
        Util.compressBitmapToPng(faceImage, new File(activity.getFilesDir(), imageFilename + POSTFIX_RIGHT_NO_ARROW + IMAGE_FACE_EXTENSION));
        faceImage.recycle();
        
        // Create Summary image
        faceImage = Util.createFaceImageWithCircleMask(
        		activity, 
        		src, 
        		FaceImageMode.CHART);
        Util.compressBitmapToPng(faceImage, new File(activity.getFilesDir(), imageFilename + POSTFIX_CHART + IMAGE_FACE_EXTENSION));
        faceImage.recycle();
        
        // Overlap frame image on source image
        Bitmap newImage = Bitmap.createBitmap(src.getWidth(), src.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newImage);
        canvas.drawBitmap(src, 0, 0, null);

        Drawable overlayImage = res.getDrawable(frameId);
        overlayImage.setBounds(0, 0, src.getWidth(), src.getHeight());
        overlayImage.draw(canvas);
        
        // Create original src image
        Util.compressBitmapToJpeg(newImage, new File(IMAGE_DIRECTORY, imageFilename + IMAGE_EXTENSION));
        newImage.recycle();
    	
    	heightInfo.setNickId(nickInfo.getNickNameId());
    	heightInfo.setImagePath(imageFilename);
    	heightInfo.setInputDate(date);
        
        final ContentValues heightValue = new ContentValues();
        heightInfo.onAddToContentValue(heightValue);
        return resolver.insert(TableColumns.Height.URI, heightValue);
    }
    
    private static Uri inserToMediaStore(
    		Activity activity, NickNameInfo nickInfo, Location location, String path, long currentTimeStamp, long fileSize) {
    	final ContentResolver resolver = activity.getContentResolver();
    	final String title = nickInfo.getNickName() + "_" + currentTimeStamp;
    	int orientation = Util.getDisplayRotation(activity); 
    	// Insert into MediaStore.
        ContentValues values = new ContentValues();
        values.put(ImageColumns.TITLE, title);
        values.put(ImageColumns.DISPLAY_NAME, title + IMAGE_EXTENSION);
        values.put(ImageColumns.DATE_TAKEN, currentTimeStamp);
        values.put(ImageColumns.MIME_TYPE, "image/jpeg");
        values.put(ImageColumns.ORIENTATION, orientation);
        values.put(ImageColumns.DATA, path);
        values.put(ImageColumns.SIZE, fileSize);

        if (location != null) {
            values.put(ImageColumns.LATITUDE, location.getLatitude());
            values.put(ImageColumns.LONGITUDE, location.getLongitude());
        }

        Uri uri = null;
        try {
            uri = resolver.insert(Images.Media.EXTERNAL_CONTENT_URI, values);
        } catch (Throwable th)  {
            // This can happen when the external volume is already mounted, but
            // MediaScanner has not notify MediaProvider to add that volume.
            // The picture is still safe and MediaScanner will find it and
            // insert it into MediaProvider. The only problem is that the user
            // cannot click the thumbnail to review the picture.
            DLog.e(TAG, "Failed to write MediaStore" + th);
        }
        
        return uri;
    }

    public static String generateFilename(String id, long timeStamp) {
        return id + "_" + timeStamp;
    }

    public static long getAvailableSpace() {
        String state = Environment.getExternalStorageState();
        DLog.d(TAG, "External storage state=" + state);
        if (Environment.MEDIA_CHECKING.equals(state)) {
            return PREPARING;
        }
        if (!Environment.MEDIA_MOUNTED.equals(state)) {
            return UNAVAILABLE;
        }

        File dir = new File(IMAGE_DIRECTORY);
        dir.mkdirs();
        if (!dir.isDirectory() || !dir.canWrite()) {
            return UNAVAILABLE;
        }

        try {
            StatFs stat = new StatFs(IMAGE_DIRECTORY);
            return stat.getAvailableBlocks() * (long) stat.getBlockSize();
        } catch (Exception e) {
            DLog.d(TAG, "Fail to access external storage", e);
        }
        return UNKNOWN_SIZE;
    }
}
