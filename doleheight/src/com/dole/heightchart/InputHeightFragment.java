package com.dole.heightchart;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.dole.heightchart.CameraFragment.ViewMode;
import com.dole.heightchart.GuideDialog.GuideMode;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

public class InputHeightFragment extends Fragment {
	
	private static final String TAG = InputHeightFragment.class.getName();

	private NickNameInfo mNickNameInfo;
	private int mFragmentHeight = 0;
	private int mListViewHeight = 0;
	private float mInterval = 0;
	private boolean mIsInit = false;
	
	private String mQrStringValueCp = null;
	private String mQrPromotionCode = null;
	private boolean mIsUsedCodeAndWithin24Hour = false;
	private long mDeleteHeightIdForCodeReuse;
	
	MixpanelAPI mMixpanel;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view =  inflater.inflate(R.layout.input_height_main, null);
		
		mMixpanel = MixpanelAPI.getInstance(getActivity(), Util.MIXPANEL_TOKEN);
		
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		Bundle args = getArguments();
		if(mNickNameInfo == null && args != null) {
			long nickId = args.getLong(Constants.NICKNAME_ID);
			mNickNameInfo = Util.getNickNameInfoById(getActivity(), nickId);
			mQrStringValueCp = args.getString(Constants.QRCODE);
			mQrPromotionCode = args.getString(Constants.PROMOTION_TYPE);
			mIsUsedCodeAndWithin24Hour = args.getBoolean(Constants.USED_CODE_WITHIN_24H, false);
			
			mDeleteHeightIdForCodeReuse = args.getLong(Constants.REUSE_HEIGHT_ID, Constants.UNAVAILABLE_ID);
		}

		final ListView ruler = (ListView)view.findViewById(R.id.input_height_ruler);
		final EditText height = (EditText)view.findViewById(R.id.input_height_input_text);
		final TextView hint = (TextView)view.findViewById(R.id.input_height_input_text_hint);
		final TextView cm = (TextView)view.findViewById(R.id.input_height_input_text_cm);
		final View saveBtn = view.findViewById(R.id.input_height_save_btn);
		
		HeightTreeAdapter adapter = new HeightTreeAdapter(1, 200);
		ruler.setAdapter(adapter);
		
		saveBtn.setOnClickListener(mAddHeightClickListener);
		saveBtn.setEnabled(false);
		
		final int oneItemHeight = getResources().getDimensionPixelSize(R.dimen.input_height_item_height);

		ruler.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				height.clearFocus();
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			    imm.hideSoftInputFromWindow(arg0.getWindowToken(), 0);
				return false;
			}
		});
		hint.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				height.setVisibility(View.VISIBLE);
				cm.setVisibility(View.VISIBLE);
				hint.setVisibility(View.INVISIBLE);
				height.requestFocus();
			}
		});
		height.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(mFragmentHeight == 0) {
					mFragmentHeight = InputHeightFragment.this.getView().getMeasuredHeight();
				}
				if(mFragmentHeight != 0 && mListViewHeight == 0) {
					mListViewHeight = mFragmentHeight + (20*getResources().getDimensionPixelSize(R.dimen.input_height_item_height));
				}
				if(mFragmentHeight != 0 && mListViewHeight != 0 && mInterval == 0) {
					mInterval = (float)(mListViewHeight - mFragmentHeight)/2000f;
				}
				if(mInterval == 0)
					return;
				float f = 0f;
				try {
					f = Float.valueOf(s.toString());
				}
				catch(Exception e) {
					return;
				}
				if(f < 0 || f > 200)
					return;
				int position = 21-(int)f/10;
				int offset = (int)(((f*10)%100)*mInterval)+mFragmentHeight/2;
				ruler.setSelectionFromTop(position, offset);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				saveBtn.setEnabled(true);
				hint.setVisibility(View.INVISIBLE);
				height.setVisibility(View.VISIBLE);
				cm.setVisibility(View.VISIBLE);
			}
		});
		ruler.setOnScrollListener(new OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				if(height.hasFocus())
					return;
				if(mFragmentHeight == 0) {
					mFragmentHeight = InputHeightFragment.this.getView().getMeasuredHeight();
				}
				if(mFragmentHeight != 0 && mListViewHeight == 0) {
					mListViewHeight = mFragmentHeight + (20*oneItemHeight);
				}
				if(mFragmentHeight != 0 && mListViewHeight != 0 && mInterval == 0) {
					mInterval = (float)(mListViewHeight - mFragmentHeight)/2000f;
				}
				if(mInterval == 0)
					return;
				if(visibleItemCount == 0)
					return;
				
				int move;
				if(firstVisibleItem == 0)
					move = -view.getChildAt(0).getTop();
				else
					move = (mFragmentHeight/2)+((firstVisibleItem-1)*oneItemHeight)-view.getChildAt(0).getTop();
				
				//mPosition += mTracker.calculateIncrementalOffset(firstVisibleItem, visibleItemCount);
				
				int curmm = (int)(2000 - (float)move/mInterval);
				float curCm = (float)curmm/10;

				height.setText(String.valueOf(curCm));
			}
		});
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				if(mNickNameInfo.mLastHeight > 0) {
					height.setText(String.valueOf(mNickNameInfo.getLastHeight()));
				}
				else {
					height.setText(String.valueOf(15));
				}
				mIsInit = true;
			}
		}, 500);

		// Guide dialog
		final GuideMode mode = GuideMode.GUIDE_INPUT_HEIGHT;
		boolean isGuideShown = HeightChartPreference.getBoolean(mode.name(), false);
		if(!isGuideShown) {
			final View v = getView();
			v.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {

				@Override
				public boolean onPreDraw() {
					if(getView() != null) { 
						final Bundle args = new Bundle();
						args.putString(GuideDialog.MODE_GUIDE, mode.name());
						Util.showDialogFragment(getFragmentManager(), new GuideDialog(), Constants.TAG_DIALOG_GUIDE, args);
					}
					v.getViewTreeObserver().removeOnPreDrawListener(this);
					return true;
				}
			});
		}
	}
	
	public void backPress() {
		getFragmentManager().popBackStack();
	}
	
//	public class ListViewScrollTracker {
//		private AbsListView mListView;
//		private SparseArray<Integer> mPositions;
//		
//		public ListViewScrollTracker(final AbsListView listView) {
//			mListView = listView;
//		}
//		
//		public int calculateIncrementalOffset(final int firstVisiblePosition, final int visibleItemCount) {
//			SparseArray<Integer> previousPositions = mPositions;
//			mPositions = new SparseArray<Integer>();
//			for(int i = 0; i < visibleItemCount; i++) {
//				mPositions.put(firstVisiblePosition + i, mListView.getChildAt(i).getTop());
//			}
//			if(previousPositions != null) {
//				for(int i = 0; i < previousPositions.size(); i++) {
//					int position = previousPositions.keyAt(i);
//					int previousTop = previousPositions.get(position);
//					Integer newTop = mPositions.get(position);
//					if(newTop != null) {
//						return newTop - previousTop;
//					}
//				}
//			}
//			return 0;
//		}
//		
//		public void clear() {
//			mPositions = null;
//		}
//	}
	
	private class HeightTreeAdapter extends BaseAdapter {
		private int[] mData;
		
		public HeightTreeAdapter(int min, int max) {
			if(min >= max)
				return;
			int len = (max - min + 1)/10;
			mData = new int[len];
			for(int i = 0; i < len; i++) {
				mData[i] = min + 9 + (i*10);
			}
		}
		
		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public int getCount() {
			return mData.length+2;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		@Override
		public int getItemViewType(int position) {
			if(position == 0 || position == getCount()-1)
				return 1;
			else
				return 2;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			int viewType = getItemViewType(position);
			if(convertView == null || (Integer)convertView.getTag() != viewType) {
				if(viewType == 1) {
					convertView = getActivity().getLayoutInflater().inflate(R.layout.input_height_empty_item, null);
					convertView.setTag(viewType);
				}
				else {
					convertView = getActivity().getLayoutInflater().inflate(R.layout.input_height_item, null);
					convertView.setTag(viewType);
				}
			}
			View v = convertView;
			if(viewType == 1) {
				if(mFragmentHeight == 0) {
					mFragmentHeight = InputHeightFragment.this.getView().getMeasuredHeight();
				}
				View view = v.findViewById(R.id.input_height_item_container);
				LayoutParams params = view.getLayoutParams();
				params.height = mFragmentHeight/2;
				view.setLayoutParams(params);
				convertView.requestLayout();
			}
			else {
				TextView tv = (TextView)v.findViewById(R.id.input_height_item_cover);
				tv.setText(mData[mData.length-position]+"");
			}
			
			return v;
		}
		
	}
	
	private OnClickListener mAddHeightClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
		    
			Bundle args = new Bundle();
			final TextView height = (TextView)getView().findViewById(R.id.input_height_input_text);
			final String heightStr = height.getText().toString();
			args.putFloat(Constants.HEIGHT, Float.valueOf(heightStr));
			args.putString(Constants.MODE, ViewMode.TAKE_PICTURE.name());
			args.putLong(Constants.NICKNAME_ID, mNickNameInfo.mNickNameId);
			args.putString(Constants.QRCODE, mQrStringValueCp);
			args.putBoolean(Constants.USED_CODE_WITHIN_24H, mIsUsedCodeAndWithin24Hour);
			args.putString(Constants.PROMOTION_TYPE, mQrPromotionCode);
			DLog.e(TAG, "INPUT HEIGHT:"+mQrPromotionCode);
			
			if(mDeleteHeightIdForCodeReuse > 0)
				args.putLong(Constants.REUSE_HEIGHT_ID, mDeleteHeightIdForCodeReuse);
			
			Util.replaceFragment(getFragmentManager(), new CameraFragment(), R.id.container, Constants.TAG_FRAGMENT_CAMERA, args, false);
			
		    //longzhe cui added
			mMixpanel.track("Recorded Height", null);
		}
	};
}
