package com.dole.heightchart;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

public class BackupDialog extends DialogFragment {
	
	public static final String KEY_MODE = "key_mode";
	public static final String KEY_SELECT = "key_select";
	public static final String KEY_FILE = "key_file";
	public static final int MODE_BACKUP_RESTORE = 1;
	public static final int MODE_CHOOSE_RESTORE = 2;
	public static final int SELECT_BACKUP = 1;
	public static final int SELECT_RESTORE = 2;
	
	private int mMode;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		final Dialog d = getDialog();
		d.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		
		int divierId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = getDialog().findViewById(divierId);
		divider.setVisibility(View.GONE);
		
		int titleId = d.getContext().getResources().getIdentifier("android:id/title", null, null);
		View title = getDialog().findViewById(titleId);
		title.setVisibility(View.GONE);
		
		Bundle args = getArguments();
		mMode = args.getInt(KEY_MODE);
		int titleHeight = getResources().getDimensionPixelSize(R.dimen.dialog_title_height);
		int buttonHeight = getResources().getDimensionPixelSize(R.dimen.dialog_button_height);
		int itemHeight = (mMode == MODE_BACKUP_RESTORE)?
				getResources().getDimensionPixelSize(R.dimen.dialog_two_item_height) :
				getResources().getDimensionPixelSize(R.dimen.dialog_item_height);
		int totalHeight = titleHeight + buttonHeight + itemHeight;
		
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
	    lp.copyFrom(d.getWindow().getAttributes());
	    lp.height = totalHeight;
	    d.getWindow().setAttributes(lp);
		
		return inflater.inflate(R.layout.backup_main, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		View content = view.findViewById(R.id.backup_content_frame);
		View backup = view.findViewById(R.id.backup_backup);
		View restore = view.findViewById(R.id.backup_import);
		TextView restorePick = (TextView)view.findViewById(R.id.backup_import_date);
		View button = view.findViewById(R.id.backup_cancel_button);
		TextView title = (TextView) view.findViewById(R.id.backup_dialog_title);
		
		if(mMode == MODE_BACKUP_RESTORE) {
			title.setText(R.string.backup_import);
			restorePick.setVisibility(View.GONE);
		}
		else if(mMode == MODE_CHOOSE_RESTORE) {
			title.setText(R.string.restore);
			if(getArguments() == null)
				return;
			
			content.setVisibility(View.GONE);
			long lastBackupDate = getArguments().getLong(BackupDialog.KEY_FILE);
			String lastDate = DateUtils.formatDateTime(getActivity(), lastBackupDate,
					DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_SHOW_DATE | 
					DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_TIME);
			restorePick.setText(lastDate);
		}
		
		backup.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				Intent data = new Intent();
				data.putExtra(KEY_SELECT, SELECT_BACKUP);
				getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
			}
		});
		
		restore.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				Intent data = new Intent();
				data.putExtra(KEY_SELECT, SELECT_RESTORE);
				getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
			}
		});
		
		restorePick.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
			}
		});
		
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}
}
