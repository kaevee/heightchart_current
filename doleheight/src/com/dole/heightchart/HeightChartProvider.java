package com.dole.heightchart;


import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

public class HeightChartProvider extends ContentProvider {
    private static final String TAG = "AppProvider";
    private static final boolean DEBUG = false;

    public static final String DATABASE_NAME = "heightchart.db";
    private static final int DATABASE_VERSION = 1;
    
    public static final String AUTHORITY = "com.dole.heightchart";
    public static final String TABLE_NICKNAME = "nickname";
    public static final String TABLE_HEIGHT = "height";
    
    protected static final String PARAMETER_NOTIFY = "notify";
    
    private DatabaseHelper mOpenHelper;
    
    @Override
    public boolean onCreate() {
        try {
            mOpenHelper = new DatabaseHelper(getContext());
        } catch(Exception e) {
        	//TODO
        	DLog.e(TAG, " db error occurred! ", e);
        }
        return true;
    }

    @Override
    public String getType(Uri uri) {
        SqlArguments args = new SqlArguments(uri, null, null);
        if (TextUtils.isEmpty(args.where)) {
            return "vnd.android.cursor.dir/" + args.table;
        } else {
            return "vnd.android.cursor.item/" + args.table;
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        SqlArguments args = new SqlArguments(uri, selection, selectionArgs);
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(args.table);

        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        Cursor result = qb.query(db, projection, args.where, args.args, null, null, sortOrder);
        result.setNotificationUri(getContext().getContentResolver(), uri);

        return result;
    }

    private static long dbInsertAndCheck(DatabaseHelper helper,
            SQLiteDatabase db, String table, String nullColumnHack, ContentValues values) {
        return db.insert(table, nullColumnHack, values);
    }

    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        SqlArguments args = new SqlArguments(uri);

        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final long rowId = dbInsertAndCheck(mOpenHelper, db, args.table, null, initialValues);
        if (rowId <= 0) return null;

        uri = ContentUris.withAppendedId(uri, rowId);
        sendNotify(uri);

        return uri;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        SqlArguments args = new SqlArguments(uri);

        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            int numValues = values.length;
            for (int i = 0; i < numValues; i++) {
                if (dbInsertAndCheck(mOpenHelper, db, args.table, null, values[i]) < 0) {
                    return 0;
                }
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        sendNotify(uri);
        return values.length;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SqlArguments args = new SqlArguments(uri, selection, selectionArgs);

        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count = db.delete(args.table, args.where, args.args);
        if (count > 0) sendNotify(uri);

        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SqlArguments args = new SqlArguments(uri, selection, selectionArgs);

        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count = db.update(args.table, values, args.where, args.args);
        if (count > 0) sendNotify(uri);

        return count;
    }

    private void sendNotify(Uri uri) {
        String notify = uri.getQueryParameter(PARAMETER_NOTIFY);
        if (notify == null || "true".equals(notify)) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
    }
    
    public void resetDatabase() {
    	mOpenHelper.close();
    	mOpenHelper = new DatabaseHelper(getContext());
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {

        //private final Context mContext;
        
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);    

            //mContext = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            createNewDb(db);
        }
        
        private void createNewDb(SQLiteDatabase db) {

            db.execSQL("CREATE TABLE " + TABLE_NICKNAME + " (" +
                    BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    TableColumns.Nickname.NICKNAME + " TEXT," +
                    TableColumns.Nickname.CHARACTER + " INTEGER," +
                    TableColumns.Nickname.BIRTHDAY + " TEXT," +
                    TableColumns.Nickname.GENDER + " TEXT" +
                    ");");
            
            db.execSQL("CREATE TABLE " + TABLE_HEIGHT + " (" +
                    BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    TableColumns.Height.NICK_ID + " INTEGER," +
                    TableColumns.Height.HEIGHT + " REAL," +
                    TableColumns.Height.INPUT_DATE + " INTEGER," +
                    TableColumns.Height.IMAGE_PATH + " TEXT," +
                    TableColumns.Height.QRCODE + " TEXT," +
                    TableColumns.Height.COUPON_TYPE + " TEXT" +
                    ");");
        }
        
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        	DLog.d(TAG, "onUpgrade triggered");

            int version = oldVersion;
            if (version != DATABASE_VERSION) {
            	DLog.d(TAG, "Destroying all old data.");
                db.execSQL("DROP TABLE IF EXISTS " + TABLE_NICKNAME);
                db.execSQL("DROP TABLE IF EXISTS " + TABLE_HEIGHT);
                onCreate(db);
            }
        }
    }

    static class SqlArguments {
        public final String table;
        public final String where;
        public final String[] args;

        SqlArguments(Uri url, String where, String[] args) {
            if (url.getPathSegments().size() == 1) {
                this.table = url.getPathSegments().get(0);
                this.where = where;
                this.args = args;
            } else if (url.getPathSegments().size() != 2) {
                throw new IllegalArgumentException("Invalid URI: " + url);
            } else if (!TextUtils.isEmpty(where)) {
            	DLog.d(TAG, "url=" + url);
            	DLog.d(TAG, "where=" + where);
            	DLog.d(TAG, "args=" + args[0]);
            	
            	DLog.d(TAG, "url.getPathSegments().size()=" + url.getPathSegments().size());
            	
            	
                throw new UnsupportedOperationException("WHERE clause not supported: " + url);
            } else {
                this.table = url.getPathSegments().get(0);
                this.where = "_id=" + ContentUris.parseId(url);
                this.args = null;
            }
        }

        SqlArguments(Uri url) {
            if (url.getPathSegments().size() == 1) {
                table = url.getPathSegments().get(0);
                where = null;
                args = null;
            } else {
                throw new IllegalArgumentException("Invalid URI: " + url);
            }
        }
    }
}
