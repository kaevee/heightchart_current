package com.dole.heightchart;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dole.heightchart.server.RegisterCS;
import com.dole.heightchart.server.ServerTask;
import com.dole.heightchart.server.ServerTask.IServerResponseCallback;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class HelpFragment extends Fragment implements IServerResponseCallback {
	
	private RegisterCS mRegisterCS;
	private boolean mIsEmailFill = false;
	private boolean mIsQuestionFill = false;
	private ProgressDialog mProgressDialog;

	MixpanelAPI mMixpanel;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view =  inflater.inflate(R.layout.help_main, null);
		
		mMixpanel = MixpanelAPI.getInstance(getActivity(), Util.MIXPANEL_TOKEN);
		
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		TextView device = (TextView)view.findViewById(R.id.help_device);
		TextView topic = (TextView)view.findViewById(R.id.help_topic);
		final Button send = (Button)view.findViewById(R.id.help_send);
		final TextView email = (TextView)view.findViewById(R.id.help_email);
		final TextView question = (TextView)view.findViewById(R.id.help_question);
		
		String deviceText = device.getText().toString() + " : Android OS / " + Build.MODEL;
		device.setText(deviceText);
		send.setEnabled(false);
		send.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				sendCS();
			}
		});
		
		email.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				String st = s.toString();
				if(st.trim().equals(""))
					mIsEmailFill = false;
				else
					mIsEmailFill = true;
				
				if(mIsEmailFill && mIsQuestionFill)
					send.setEnabled(true);
				else
					send.setEnabled(false);
			}
		});
		
		question.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				String st = s.toString();
				if(st.trim().equals(""))
					mIsQuestionFill = false;
				else
					mIsQuestionFill = true;
				
				if(mIsEmailFill && mIsQuestionFill)
					send.setEnabled(true);
				else
					send.setEnabled(false);
			}
		});
		
		topic.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent faq = new Intent();
				faq.setAction(Intent.ACTION_VIEW);
				faq.setData(Uri.parse(Constants.FAQ_URL));
				getActivity().startActivity(faq);
			}
		});

		String userId = HeightChartPreference.getString(Constants.PREF_USER_ID, "");
		if(!userId.equals(""))
			email.setText(userId);
		
		getView().setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent e) {
				Util.hideSoftInput(getActivity(), v);
				email.clearFocus();
				question.clearFocus();
				return false;
			}
		});
	}
	
	private void sendCS() {
		View root = getView();
		TextView email = (TextView)root.findViewById(R.id.help_email);
		TextView question = (TextView)root.findViewById(R.id.help_question);
		TextView device = (TextView)root.findViewById(R.id.help_device);
		Date date = new Date();
		
		String emailAdd = email.getText().toString();
		if(!Util.checkEmail(emailAdd)) {
			Toast.makeText(getActivity(), getResources().getString(R.string.enter_full_email), Toast.LENGTH_SHORT).show();
			return;
		}
		
		HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_NO, String.valueOf(HeightChartPreference.getInt(Constants.PREF_USER_NO, 0)));
		request.put(Constants.TAG_USER_ID, HeightChartPreference.getString(Constants.PREF_USER_ID, emailAdd));
		request.put(Constants.TAG_EMAIL, emailAdd);
		request.put(Constants.TAG_IS_RECEIVE_EMAIL, String.valueOf(true));
		request.put(Constants.TAG_CATEGORY_NO, String.valueOf(1));
		request.put(Constants.TAG_SUB_CATEGORY_NO, String.valueOf(1));
		request.put(Constants.TAG_INQUIRY_TITLE, "Height Chart - android");
		request.put(Constants.TAG_INQUIRY_CONTENT, question.getText().toString());
		request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
		request.put(Constants.TAG_OCCURRENCE_DATE_TIME, date.toString());
		request.put(Constants.TAG_DESCRIPTION, device.getText().toString());
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
		
		mRegisterCS = new RegisterCS(getActivity(), request);
		mRegisterCS.setCallback(this);
		Model.runOnWorkerThread(mRegisterCS);
		
		mProgressDialog = Util.openProgressDialog(getActivity());
	}

	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		Toast.makeText(getActivity(), getResources().getString(R.string.inquiry_received), Toast.LENGTH_SHORT).show();
		getFragmentManager().popBackStackImmediate();
		mProgressDialog.dismiss();
		
		//longzhe cui added
		mMixpanel.track("feedback", null);
	}

	@Override
	public void onFailed(ServerTask parser, Map<String, String> result, int returnCode) {
		Toast.makeText(getActivity(), Util.switchErrorCode(returnCode, getResources()), Toast.LENGTH_SHORT).show();
		mProgressDialog.dismiss();
	}
}
