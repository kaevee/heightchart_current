package com.dole.heightchart;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dole.heightchart.Constants.CouponType;
import com.dole.heightchart.camera.Storage;
import com.dole.heightchart.server.RequestChargeCoin;
import com.dole.heightchart.server.RequestNewAuthKey;
import com.dole.heightchart.server.ServerTask;
import com.dole.heightchart.server.ServerTask.IServerResponseCallback;
import com.dole.heightchart.ui.AnimationView;
import com.dole.heightchart.ui.CustomDialog;
import com.facebook.FacebookRequestError;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.NewPermissionsRequest;
import com.facebook.SessionState;
import com.facebook.internal.Utility;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class HeightViewerFragment extends Fragment implements IServerResponseCallback, Session.StatusCallback {
	private static final String TAG = "HeightViewerFragment";
	private AnimationDrawableLoader mLoader;

	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	
	private boolean mIsConfirm;
	private long mCurTime;
	private ViewPager mViewPager;
	private ProgressDialog mProgressDialog;
	private List<HeightInfo> mHeights;
	private int mInitPosition = 0;
	
	private boolean mIsUIHidden = false;
	private int[] mToggleIds = {
			R.id.height_viewer_btn_confirm,
			R.id.height_viewer_btn_delete,
			R.id.height_viewer_btn_graph,
			R.id.height_viewer_btn_share,
			R.id.common_btn_cancel
	};
	
	private Comparator<HeightInfo> mHeightInfoDateComparator = new Comparator<HeightInfo>() {
		
		@Override
		public int compare(HeightInfo object1, HeightInfo object2) {
			return object2.mInputDate.compareTo(object1.mInputDate);
		}
	};
	
	class ViewerImageFrameAdapter extends PagerAdapter {
		
		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			final LayoutInflater inflater = LayoutInflater.from(getActivity());
			final ImageView page = (ImageView) inflater.inflate(R.layout.height_viewer_item, null);
			page.setOnClickListener(mToggleUiClickListener);

			HeightInfo info = 	mHeights.get(position);
			initPageImage(page, info);
			initLayout(info);
			
			container.addView(page);
			return page;
		}
		
		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			if(mHeights == null)
				return 0;
			return mHeights.size();
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}
	}
	
	private OnClickListener mToggleUiClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			for(int id : mToggleIds) {
				if(mIsConfirm && id == R.id.common_btn_cancel) {
					continue;
				} else if(!mIsConfirm && id == R.id.height_viewer_btn_confirm) {
					continue;
				}
				
				View view = getView().findViewById(id);
				view.setVisibility((mIsUIHidden)?View.INVISIBLE:View.VISIBLE);
			}
			mIsUIHidden ^= true;
		}
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view =  inflater.inflate(R.layout.height_viewer, null);
		
		return view;
	}

	@Override
	public void onViewCreated(View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		final Model model = ((ApplicationImpl)getActivity().getApplication()).getModel();
		
		Bundle args = getArguments();
		if(args != null) {
			long heightId = args.getLong(Constants.HEIGHT_ID);
			final HeightInfo heightInfo = model.getHeightInfoById(heightId);
			mIsConfirm = args.getBoolean(Constants.IS_VIEWER_CONFIRM);
			
			if(mIsConfirm) {
				mHeights = new ArrayList<HeightInfo>();
				mHeights.add(heightInfo);
			} else {
				// Split confirm and normal viewer
				NickNameInfo nickInfo = model.getNickNameInfoById(heightInfo.mNickId);
				if(nickInfo != null) {
					mHeights = nickInfo.getHeightList();
				}
			}
			Collections.sort(mHeights, mHeightInfoDateComparator);
			mInitPosition = mHeights.indexOf(heightInfo);
		}
		
		if(mIsConfirm) {
			final ImageButton cancel = (ImageButton) view.findViewById(R.id.common_btn_cancel);
			final ImageButton confirm = (ImageButton) view.findViewById(R.id.height_viewer_btn_confirm);
			cancel.setVisibility(View.INVISIBLE);
			confirm.setVisibility(View.VISIBLE);
			
			confirm.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					getFragmentManager().popBackStack();
				}
			});
		}
		
		initViewPager();
		
		View delete = view.findViewById(R.id.height_viewer_btn_delete);
		View graph = view.findViewById(R.id.height_viewer_btn_graph);
		View share = view.findViewById(R.id.height_viewer_btn_share);
		
		delete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Bundle args = new Bundle();
				args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
				args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.selected_record_deletion);
				args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, R.drawable.popup_cancel_btn);
				args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
				
				Util.showDialogFragment(
						getFragmentManager(), 
						new CustomDialog(), 
						Constants.TAG_DIALOG_CUSTOM, 
						args, 
						HeightViewerFragment.this, 
						Constants.REQUEST_DELETE_USER_INFO);
				
			}
		});
		
		final int userNo = HeightChartPreference.getInt(Constants.PREF_USER_NO, 0);
		graph.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(userNo > 0) {
					final HeightInfo heightInfo = mHeights.get(mViewPager.getCurrentItem());
					
					final Bundle args = new Bundle();
					args.putLong(Constants.NICKNAME_ID, heightInfo.mNickId);
					//Util.replaceFragment(getFragmentManager(), new ChartFragment(), R.id.container, Constants.TAG_FRAGMENT_CHART, args, true);
					Util.startChartActivity(getActivity(), args);
				} else {
					Bundle args = new Bundle();
					args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
					args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.request_log_in);
					args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_TEXT_RES, R.string.login);
					args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, R.drawable.popup_cancel_btn);
					args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
					Util.showDialogFragment(
							getFragmentManager(), 
							new CustomDialog(), 
							Constants.TAG_DIALOG_CUSTOM, 
							args, 
							HeightViewerFragment.this, 
							Constants.REQUEST_USER_CONFIRM);
				}
			}
		});
		
		share.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(userNo > 0) {
					Util.showDialogFragment(
							getFragmentManager(), 
							new ShareDialog(), 
							Constants.TAG_DIALOG_SHARE, 
							null, 
							HeightViewerFragment.this, 
							Constants.REQUEST_CONFIRM_SHARE);
				} else {
					Bundle args = new Bundle();
					args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
					args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.request_log_in);
					args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_TEXT_RES, R.string.login);
					args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, R.drawable.popup_cancel_btn);
					args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
					Util.showDialogFragment(
							getFragmentManager(), 
							new CustomDialog(), 
							Constants.TAG_DIALOG_CUSTOM, 
							args, 
							HeightViewerFragment.this, 
							Constants.REQUEST_USER_CONFIRM_SHARE);
				}
			}
		});
		
		disableViews();
	}
	
	private void initViewPager() {
		mViewPager = (ViewPager) getView().findViewById(R.id.height_viewer_pager);
		mViewPager.setAdapter(new ViewerImageFrameAdapter());
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageScrollStateChanged(int state) {
				
			}
			
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
				
			}
			
			@Override
			public void onPageSelected(int position){
				initLayout(mHeights.get(position));
				disableViews();
			}
		});
		mViewPager.setCurrentItem(mInitPosition);
	}
	
	private void initPageImage(ImageView imageView, HeightInfo heightInfo) {
		if(heightInfo != null) {
			Display display = getActivity().getWindowManager().getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			final int maxDecodePixelSize = size.x * size.y;
			
			Bitmap bitmap = null;
			try {
				bitmap = Util.decodeFile(heightInfo.getImageFullPath() + Storage.IMAGE_EXTENSION, maxDecodePixelSize);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
			imageView.setImageBitmap(bitmap);
		}
	}
	
	private void initLayout(HeightInfo heightInfo) {
		final TextView textView = (TextView) getView().findViewById(R.id.height_viewer_date);
		textView.setText(new SimpleDateFormat("yyyy.MM.dd").format(heightInfo.mInputDate));
		
		CouponType type = heightInfo.mCouponType;
		DLog.e(TAG, "mCouponType:"+type);
		//TODO:
		if(type == null)
			type = CouponType.LION;
		
		int arrayRes = 0;
		int imageRes = 0;
		switch(type) {
		case RACCOON:
			arrayRes = R.array.ani_raccoon_position;
			imageRes = R.drawable.ani_raccoon;
			break;
		case BIRD:
			arrayRes = R.array.ani_toucan_position;
			imageRes = R.drawable.ani_toucan;
			break;
		case LION:
			arrayRes = R.array.ani_lion_position;
			imageRes = R.drawable.ani_lion;
			break;
		case ALLIGATOR:
			arrayRes = R.array.ani_alligator_position;
			imageRes = R.drawable.ani_alligator;
			break;
		case FOX:
			arrayRes = R.array.ani_fox_position;
			imageRes = R.drawable.ani_fox;
			break;
		default:
			break;
		}
		if(arrayRes != 0 && imageRes != 0) {
			mLoader = new AnimationDrawableLoader();
			Integer[] paramsArray = new Integer[AnimationDrawableLoader.PARAMS_COUNT];
			paramsArray[AnimationDrawableLoader.VIEW_IDX] = R.id.height_viewer_animated_height;
			paramsArray[AnimationDrawableLoader.IMG_RES_IDX] = imageRes;
			paramsArray[AnimationDrawableLoader.TEXT_POS_IDX] = arrayRes;
			mLoader.execute(paramsArray);
		}
	}
	
	private void disableViews() {
		final View view = getView();
		final View delete = view.findViewById(R.id.height_viewer_btn_delete);
		final View graph = view.findViewById(R.id.height_viewer_btn_graph);
		final View share = view.findViewById(R.id.height_viewer_btn_share);

//		delete.setVisibility(View.GONE);
//		graph.setVisibility(View.GONE);
//		share.setVisibility(View.GONE);
		delete.setEnabled(false);
		graph.setEnabled(false);
		share.setEnabled(false);

		delete.setAlpha(0.5f);
		graph.setAlpha(0.5f);
		share.setAlpha(0.5f);
	}
	
	private void enableViews() {
		final View view = getView();
		if(view == null)
			return;
		
		long curTime = Calendar.getInstance().getTimeInMillis();
		final View delete = view.findViewById(R.id.height_viewer_btn_delete);
		final View graph = view.findViewById(R.id.height_viewer_btn_graph);
		final View share = view.findViewById(R.id.height_viewer_btn_share);
		
		DLog.e(TAG, "cur:"+curTime+",mcur:"+mCurTime);
		if(curTime >= mCurTime+2500) {
//			delete.setVisibility(View.VISIBLE);
//			graph.setVisibility(View.VISIBLE);
//			share.setVisibility(View.VISIBLE);
			delete.setEnabled(true);
			graph.setEnabled(true);
			share.setEnabled(true);
			delete.setAlpha(1f);
			graph.setAlpha(1f);
			share.setAlpha(1f);
		}
		else {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
//					delete.setVisibility(View.VISIBLE);
//					graph.setVisibility(View.VISIBLE);
//					share.setVisibility(View.VISIBLE);
					delete.setEnabled(true);
					graph.setEnabled(true);
					share.setEnabled(true);

					delete.setAlpha(1f);
					graph.setAlpha(1f);
					share.setAlpha(1f);
				}
			}, mCurTime+2500-curTime);
		}
	}
	
	
	class AnimationDrawableLoader extends AsyncTask<Integer, Integer, Integer> {
		
		private static final int RESULT_BYPASS = -1;
		
		static final int VIEW_IDX = 0;
		static final int IMG_RES_IDX = VIEW_IDX + 1;
		static final int TEXT_POS_IDX = IMG_RES_IDX + 1;
		static final int PARAMS_COUNT = TEXT_POS_IDX + 1;
		
		static final int ANIMATION_READY = 0x1000;
		
		private AnimationView mView;
		private AnimationDrawable mAnimationDrawable;

		@Override
		protected Integer doInBackground(Integer... params) {
			final int len = params.length;
			
			if(getView() == null)
				return RESULT_BYPASS;
			
			if(len == 3) {
				int viewId = params[VIEW_IDX];
				int animationId = params[IMG_RES_IDX];
				int textPositionId = params[TEXT_POS_IDX];
				
				Resources res = getResources();
				if(res == null)
					return -1;
				
				mAnimationDrawable = (AnimationDrawable) res.getDrawable(animationId);
				if(getView() != null) {
					final HeightInfo heightInfo = mHeights.get(mViewPager.getCurrentItem());
					
					mView = (AnimationView) getView().findViewById(viewId);
					mView.setTextPositionRes(textPositionId, heightInfo.mHeight);

					return ANIMATION_READY;
				}
			}
			
			return 0;
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			if(result == RESULT_BYPASS) 
				return;
			
			if(result == ANIMATION_READY) {
				mView.setImageDrawable(mAnimationDrawable);
				mView.initAnimation();
				mAnimationDrawable.start();
				mCurTime = Calendar.getInstance().getTimeInMillis();
				DLog.e(TAG, "onPostExecute mCurTime:"+mCurTime);
				enableViews();
			}
		}
		
		public void restartAnimation() {
			if(mAnimationDrawable != null && mView != null) {
				mAnimationDrawable.setVisible(false, true);
				if(mAnimationDrawable.isRunning())
					mAnimationDrawable.stop();
				mAnimationDrawable.start();
				mCurTime = Calendar.getInstance().getTimeInMillis();
				DLog.e(TAG, "restartAnimation mCurTime:"+mCurTime);
				enableViews();
			}
		}
	}
	
	public void loadAnimationDrawable(View view, int animationDrawableId) {
		getActivity().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				
			}
		});
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		DLog.e(TAG, "onActivityResult requestCode:"+requestCode);
		DLog.e(TAG, "onActivityResult resultCode:"+resultCode);
		if(resultCode != Activity.RESULT_OK)
			return;

		final HeightInfo heightInfo = mHeights.get(mViewPager.getCurrentItem());
		switch (requestCode) {
		case Constants.REQUEST_USER_CONFIRM:
			Util.replaceFragment(
					getFragmentManager(), 
					new LoginFragment(), 
					R.id.container, 
					Constants.TAG_FRAGMENT_LOGIN, 
					null, 
					this,
					Constants.REQUEST_VIEW_GRAPH,
					true);

			break;

		case Constants.REQUEST_VIEW_GRAPH: {
			
			final Bundle args = new Bundle();
			args.putLong(Constants.NICKNAME_ID, heightInfo.mNickId);
			//Util.replaceFragment(getFragmentManager(), new ChartFragment(), R.id.container, Constants.TAG_FRAGMENT_CHART, args, true);
			Util.startChartActivity(getActivity(), args);
			break;
		}

		case Constants.REQUEST_USER_CONFIRM_SHARE:
			Util.replaceFragment(
					getFragmentManager(), 
					new LoginFragment(), 
					R.id.container, 
					Constants.TAG_FRAGMENT_LOGIN, 
					null, 
					this,
					Constants.REQUEST_SHARE_VIEWER,
					true);

			break;

		case Constants.REQUEST_SHARE_VIEWER: {
			Util.showDialogFragment(
					getFragmentManager(), 
					new ShareDialog(), 
					Constants.TAG_DIALOG_SHARE, 
					null, 
					HeightViewerFragment.this, 
					Constants.REQUEST_CONFIRM_SHARE);
			break;
		}
			
		case Constants.REQUEST_DELETE_USER_INFO:
			final Model model = ((ApplicationImpl)getActivity().getApplication()).getModel();
			final NickNameInfo nickInfo = model.getNickNameInfoById(heightInfo.mNickId);
			final ArrayList<HeightInfo> heightList = nickInfo.getHeightList();
			final int deleteIdx = heightList.indexOf(heightInfo);
			
			Model.runOnWorkerThread(new Runnable() {
				public void run() {
					List<HeightInfo> delList = new ArrayList<HeightInfo>();
					delList.add(heightInfo);
					Model.deleteHeights(getActivity(), nickInfo, delList);
					
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if(mIsConfirm) {
								getFragmentManager().popBackStack();
							} else {
								if(heightList.size() > 0) {
									final HeightInfo newHeight;
									if(deleteIdx >= heightList.size() - 1)
										newHeight = heightList.get(heightList.size() - 1);
									else
										newHeight = heightList.get(deleteIdx);
									initViewPager();
									initLayout(newHeight);
								}
								else {
									getFragmentManager().popBackStack();
								}
							}
						}
					});
				}
			});
			break;
			
		case Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE:
			DLog.d(TAG, " Return from facebook login activity");
            Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);
            break;

		case Constants.REQUEST_DOLE_COIN:
			Toast.makeText(getActivity(), getResources().getString(R.string.shared), Toast.LENGTH_SHORT).show();
			break;

		case Constants.REQUEST_CONFIRM_SHARE:
			share();
			break;
		}
	}
	
	private void share() {
		Session session = Session.getActiveSession();
		DLog.e(TAG, "session:"+session);
        if (session == null) {
//            if (savedInstanceState != null) {
//                session = Session.restoreSession(getActivity(), null, HeightViewerFragment.this, savedInstanceState);
//            }
            if (session == null) {
                session = new Session(getActivity());
            }
            Session.setActiveSession(session);
        }
		DLog.e(TAG, "session:"+session);
		if (session == null || !session.isOpened()) {
			DLog.e(TAG, "session is not Opened:");
            Session.openActiveSession(
                    getActivity(), HeightViewerFragment.this, 
                    true, 
                    HeightViewerFragment.this);
        }
		else {
			DLog.e(TAG, "session isOpened:");
			publishPicture();
		}
	}
	
	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
	    for (String string : subset) {
	        if (!superset.contains(string)) {
	            return false;
	        }
	    }
	    return true;
	}
	
	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
	    if (state.isOpened()) {
	    	publishPicture();
	    }
	}

	@Override
	public void call(Session session, SessionState state, Exception exception) {
		DLog.e(TAG, "state:"+state.isOpened());
		onSessionStateChange(session, state, exception);
	}
	
	private void publishPicture() {
		DLog.e(TAG, "publishPicture");
		Session session = Session.getActiveSession();
    	List<String> permissions = session.getPermissions();
    	if (!isSubsetOf(PERMISSIONS, permissions)) {
    		DLog.e(TAG, "session:"+session);
    		session.requestNewPublishPermissions(new NewPermissionsRequest(HeightViewerFragment.this, PERMISSIONS));
    		return;
    	}
    	
    	Request.Callback callback= new Request.Callback() {
            public void onCompleted(Response response) {
            	DLog.e(TAG, response.toString());
                FacebookRequestError error = response.getError();
                if (error != null) {
                    Toast.makeText(getActivity(),
                         error.getErrorMessage(),
                         Toast.LENGTH_SHORT).show();
                } else {
                	Util.requestChargeCoin(getActivity(), HeightViewerFragment.this, 100);
                }
            }
        };
        
        final HeightInfo heightInfo = mHeights.get(mViewPager.getCurrentItem());
        ImageView animView = (ImageView)getView().findViewById(R.id.height_viewer_animated_height);
        View dateView = getView().findViewById(R.id.height_viewer_date);
        View logoView = getView().findViewById(R.id.height_viewer_logo);
        logoView.setVisibility(View.VISIBLE);
		Bitmap bitmap = BitmapFactory.decodeFile(heightInfo.getImageFullPath() + ".jpg");
		Bitmap copyBitmap = bitmap.copy(Bitmap.Config.ARGB_8888,true);
		Canvas c = new Canvas(copyBitmap);
		
		c.save();
		c.translate(animView.getLeft(), animView.getTop());
		animView.draw(c);
		c.restore();

		c.save();
		c.translate(dateView.getLeft(), dateView.getTop());
		dateView.draw(c);
		c.restore();

		c.save();
		c.translate(logoView.getLeft(), logoView.getTop());
		logoView.draw(c);
		c.restore();

        Model model = ((ApplicationImpl)getActivity().getApplication()).getModel();
    	NickNameInfo nickInfo = model.getNickNameInfoById(heightInfo.mNickId);
        String applicationId = Utility.getMetadataApplicationId(getActivity());
		
        Bundle parameters = new Bundle(1);
        parameters.putParcelable("picture", copyBitmap);
        parameters.putString("message", getResources().getString(R.string.facebook_height_viewer, nickInfo.mNickName, heightInfo.mHeight));
        parameters.putString("link", "http://www.doleapps.com");
        parameters.putString("app_id", applicationId);
        
		Request request = Request.newUploadPhotoRequest(session, bitmap, callback);
		request.setParameters(parameters);
		RequestAsyncTask task = new RequestAsyncTask(request);
		task.execute();
        logoView.setVisibility(View.GONE);
    	mProgressDialog = Util.openProgressDialog(getActivity());
	}

	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		if(parser instanceof RequestNewAuthKey) {
	    	if(Constants.DEBUG)
	    		Toast.makeText(getActivity(), result.get(Constants.TAG_AUTHKEY), Toast.LENGTH_LONG).show();
			HeightChartPreference.putString(Constants.PREF_AUTH_KEY, result.get(Constants.TAG_AUTHKEY));
			Util.requestChargeCoin(getActivity(), HeightViewerFragment.this, 100);
		}
		else if(parser instanceof RequestChargeCoin) {
			int coin = HeightChartPreference.getInt(Constants.PREF_LAST_DOLE_COIN, 0);
			coin+=100;
			HeightChartPreference.putInt(Constants.PREF_LAST_DOLE_COIN, coin);
			if(mProgressDialog != null)
				mProgressDialog.dismiss();
			
			Bundle args = new Bundle();
			args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_coin);
			args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.congratulation_for_coin);
			args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES_PARAM1, 100);
			args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
			
			Util.showDialogFragment(
					getFragmentManager(), 
					new CustomDialog(), 
					Constants.TAG_DIALOG_CUSTOM, 
					args, 
					HeightViewerFragment.this, 
					Constants.REQUEST_DOLE_COIN);
		}
	}

	@Override
	public void onFailed(ServerTask parser, Map<String, String> result, int returnCode) {
		if(parser instanceof RequestChargeCoin && returnCode == Constants.ERROR_CODE_AUTHKEY_EXPIRED) {
			Util.requestNewAuthKey(getActivity(), this);
		}
		else if(parser instanceof RequestNewAuthKey) {
			Toast.makeText(getActivity(), R.string.error_code_default, Toast.LENGTH_SHORT).show();
			Util.clearUserInfo();
			getActivity().finish();
		}
		else {
			int failAmount = HeightChartPreference.getInt(Constants.PREF_COIN_TO_ADDED, 0);
			failAmount+=100;
			HeightChartPreference.putInt(Constants.PREF_COIN_TO_ADDED, failAmount);
			if(mProgressDialog != null)
				mProgressDialog.dismiss();
		}
	}
}
