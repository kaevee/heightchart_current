package com.dole.heightchart.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Interpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.dole.heightchart.R;

public class CloudBg extends RelativeLayout {
	private static final int CLOUD_FLOW_DURATION = 15000;
	private static final int ANIMAL_DURATION = 10000;
	private static final int ANIMAL_DELAY = 20000;
	private static final int BALOON_DELAY = 10000;
	private final int mAnimationStep;

	public CloudBg(Context context, AttributeSet attrs) {
		super(context, attrs);
		TypedArray styleArray = context.obtainStyledAttributes(attrs, R.styleable.cloudAnimation);
		mAnimationStep = styleArray.getInt(R.styleable.cloudAnimation_animationStepCount, 0);
	}

	public CloudBg(Context context) {
		this(context, null);
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		if(oldw == 0 && oldh == 0 && w > 0 && h > 0) {
			animationStart();
		}
	}
	
	public void animationStart() {
		View cloudUpper1 = findViewById(R.id.upper_cloud1);
		View cloudUpper2 = findViewById(R.id.upper_cloud2);
		View cloudCenter1 = findViewById(R.id.center_cloud1);
		View cloudCenter2 = findViewById(R.id.center_cloud2);
		View cloudBelow1 = findViewById(R.id.lower_cloud1);
		View cloudBelow2 = findViewById(R.id.lower_cloud2);
		int width = getMeasuredWidth();
		
		TranslateAnimation firstAnimation = new TranslateAnimation(0,width,0,0);
		TranslateAnimation secondAnimation = new TranslateAnimation(-width,0,0,0);

		firstAnimation = setupCloudAnimation(firstAnimation);
		secondAnimation = setupCloudAnimation(secondAnimation);
		
		if(cloudUpper1 != null && cloudUpper2 != null) {
			cloudUpper1.startAnimation(firstAnimation);
			cloudUpper2.startAnimation(secondAnimation);
		}
		if(cloudCenter1 != null && cloudCenter2 != null) {
			cloudCenter1.startAnimation(firstAnimation);
			cloudCenter2.startAnimation(secondAnimation);
		}
		if(cloudBelow1 != null && cloudBelow2 != null) {
			cloudBelow1.startAnimation(firstAnimation);
			cloudBelow2.startAnimation(secondAnimation);
		}
		
		if(mAnimationStep == 3) {
			start3StepAnimation();
		}
		else if(mAnimationStep == 2) {
			start2StepAnimation();
		}
	}
	
	private void start2StepAnimation() {
		int height = getMeasuredHeight();
		int animationMargin = getResources().getDimensionPixelSize(R.dimen.input_height_animation_view_margin);
		int animationViewWidth = getResources().getDimensionPixelSize(R.dimen.input_height_animation_view_width);
		int animationWidth = getMeasuredWidth()-(animationMargin*2)-animationViewWidth;

		final ImageView leftAnimation = (ImageView)findViewById(R.id.left_animation_view);
		final ImageView rightAnimation = (ImageView)findViewById(R.id.right_animation_view);
		
		final TranslateAnimation leftAnimation1 = new TranslateAnimation(0,animationWidth,0,-height/2);
		final TranslateAnimation leftAnimation2 = new TranslateAnimation(animationWidth,0,-height/2,-height);
		
		final TranslateAnimation rightAnimation1 = new TranslateAnimation(0,-animationWidth,0,-height/2);
		final TranslateAnimation rightAnimation2 = new TranslateAnimation(-animationWidth,0,-height/2,-height);

		setupAnimalAnimation(leftAnimation1);
		setupAnimalAnimation(leftAnimation2);
		leftAnimation1.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				if(leftAnimation != null) {
					leftAnimation.clearAnimation();
					leftAnimation.startAnimation(leftAnimation2);
				}
			}
		});
		leftAnimation2.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				if(leftAnimation != null) {
					leftAnimation.clearAnimation();
					new Handler().postDelayed(new Runnable() {
						public void run() {
							leftAnimation.startAnimation(leftAnimation1);
						}
					}, BALOON_DELAY);
				}
			}
		});
		
		setupAnimalAnimation(rightAnimation1);
		setupAnimalAnimation(rightAnimation2);
		rightAnimation1.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				if(rightAnimation != null) {
					rightAnimation.clearAnimation();
					rightAnimation.startAnimation(rightAnimation2);
				}
			}
		});
		rightAnimation2.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				if(rightAnimation != null) {
					rightAnimation.clearAnimation();
					new Handler().postDelayed(new Runnable() {
						public void run() {
							rightAnimation.startAnimation(rightAnimation1);
						}
					}, BALOON_DELAY);
				}
			}
		});
		
		if(leftAnimation != null) {
			leftAnimation.startAnimation(leftAnimation1);
		}
		if(rightAnimation != null) {
			new Handler().postDelayed(new Runnable() {
				public void run() {
					rightAnimation.startAnimation(rightAnimation1);
				}
			}, BALOON_DELAY);
		}
	}
	
	private void start3StepAnimation() {
		int height = getMeasuredHeight();
		int animationMargin = getResources().getDimensionPixelSize(R.dimen.detail_animation_view_margin);
		int animationViewWidth = getResources().getDimensionPixelSize(R.dimen.detail_animation_view_width);
		int animationWidth = getMeasuredWidth()-(animationMargin*2)-animationViewWidth;

		final ImageView leftAnimation = (ImageView)findViewById(R.id.left_animation_view);
		final ImageView rightAnimation = (ImageView)findViewById(R.id.right_animation_view);
		
		final TranslateAnimation leftAnimation1 = new TranslateAnimation(0,animationWidth,0,-height/3);
		final TranslateAnimation leftAnimation2 = new TranslateAnimation(animationWidth,0,-height/3,(-height/3)*2);
		final TranslateAnimation leftAnimation3 = new TranslateAnimation(0,animationWidth,(-height/3)*2,-height);
		
		final TranslateAnimation rightAnimation1 = new TranslateAnimation(0,-animationWidth,0,-height/3);
		final TranslateAnimation rightAnimation2 = new TranslateAnimation(-animationWidth,0,-height/3,(-height/3)*2);
		final TranslateAnimation rightAnimation3 = new TranslateAnimation(0,-animationWidth,(-height/3)*2,-height);

		setupAnimalAnimation(leftAnimation1);
		setupAnimalAnimation(leftAnimation2);
		setupAnimalAnimation(leftAnimation3);
		leftAnimation1.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				if(leftAnimation != null) {
					leftAnimation.clearAnimation();
					leftAnimation.startAnimation(leftAnimation2);
				}
			}
		});
		leftAnimation2.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				if(leftAnimation != null) {
					leftAnimation.clearAnimation();
					leftAnimation.startAnimation(leftAnimation3);
				}
			}
		});
		leftAnimation3.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				if(leftAnimation != null) {
					leftAnimation.clearAnimation();
					new Handler().postDelayed(new Runnable() {
						public void run() {
							leftAnimation.startAnimation(leftAnimation1);
						}
					}, ANIMAL_DELAY);
				}
			}
		});
		
		setupAnimalAnimation(rightAnimation1);
		setupAnimalAnimation(rightAnimation2);
		setupAnimalAnimation(rightAnimation3);
		rightAnimation1.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				if(rightAnimation != null) {
					rightAnimation.clearAnimation();
					rightAnimation.startAnimation(rightAnimation2);
				}
			}
		});
		rightAnimation2.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				if(rightAnimation != null) {
					rightAnimation.clearAnimation();
					rightAnimation.startAnimation(rightAnimation3);
				}
			}
		});
		rightAnimation3.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				if(rightAnimation != null) {
					rightAnimation.clearAnimation();
					new Handler().postDelayed(new Runnable() {
						public void run() {
							rightAnimation.startAnimation(rightAnimation1);
						}
					}, ANIMAL_DELAY);
				}
			}
		});
		
		if(leftAnimation != null) {
			((AnimationDrawable)leftAnimation.getDrawable()).start();
			leftAnimation.startAnimation(leftAnimation1);
		}
		if(rightAnimation != null) {
			((AnimationDrawable)rightAnimation.getDrawable()).start();
			new Handler().postDelayed(new Runnable() {
				public void run() {
					rightAnimation.startAnimation(rightAnimation1);
				}
			}, ANIMAL_DELAY);
		}
	}
	
	private TranslateAnimation setupCloudAnimation(TranslateAnimation anim) {
		anim.setInterpolator(new Interpolator() {
			@Override
			public float getInterpolation(float input) {
				return input;
			}
		});
		anim.setDuration(CLOUD_FLOW_DURATION);
		anim.setRepeatCount(Animation.INFINITE);
		anim.setRepeatMode(Animation.RESTART);
		return anim;
	}
	
	private TranslateAnimation setupAnimalAnimation(TranslateAnimation anim) {
		anim.setInterpolator(new Interpolator() {
			@Override
			public float getInterpolation(float input) {
				return input;
			}
		});
		anim.setDuration(ANIMAL_DURATION);
		anim.setRepeatCount(0);
		anim.setRepeatMode(Animation.RESTART);
		return anim;
	}
	
	public void setDeleteMode() {
		View delete = findViewById(R.id.cloud_delete_background);
		View normal = findViewById(R.id.cloud_normal_container);
		if(delete != null)
			delete.setVisibility(View.VISIBLE);
		if(normal != null)
			normal.setVisibility(View.GONE);
	}
	
	public void setNormalMode() {
		View delete = findViewById(R.id.cloud_delete_background);
		View normal = findViewById(R.id.cloud_normal_container);
		if(delete != null)
			delete.setVisibility(View.GONE);
		if(normal != null)
			normal.setVisibility(View.VISIBLE);
	}
}
