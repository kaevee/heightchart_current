package com.dole.heightchart.ui;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dole.heightchart.DetailFragment;
import com.dole.heightchart.DetailFragment.HeightTreeItem;
import com.dole.heightchart.DetailFragment.ModeChangeListener;
import com.dole.heightchart.DLog;
import com.dole.heightchart.HeightInfo;
import com.dole.heightchart.NickNameInfo;
import com.dole.heightchart.R;
import com.dole.heightchart.Util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ZoomHeightTree extends RelativeLayout {
	private static final String TAG = ZoomHeightTree.class.getName();
	
	private Bitmap mTreeCoverBitmap;
	private Bitmap mIndicatorBitmap;
	private boolean mStartAnimation = false;
	private List<DetailFragment.HeightTreeItem> mHeightInfos;
	
	private int mNodeWidth;
	private int mItemHeight;
	private int mPaddingTop;
	private int mHeightTextSize;
	private int mHeightTextMargin;
	private int mDateTextSize;
	private int mDateTextMargin;
	private int mIndicatorWidth;
	private int mIndicatorHeight;
	private int mIndicatorMargin;
	private Paint mHeightPaint;
	private Paint mDatePaint;
	private SimpleDateFormat mSimpleDateFormat;
	private float mAveHeight = 0f;
	private int mAge = 0;
	private ArrayList<ZoomHeightItem> mTextList;
	private ModeChangeListener mListener;
	
	private class ZoomHeightItem {
		Rect rect;
		int height;

		ZoomHeightItem(Rect r, int h) {
			rect = r;
			height = h;
		}
	}

	public ZoomHeightTree(Context context, AttributeSet attrs) {
		super(context, attrs);
		mTreeCoverBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.detail_all_tree_cover);
		
		Resources res = getResources();
		mNodeWidth = res.getDimensionPixelSize(R.dimen.detail_tree_zoom_node_width);
		mItemHeight = res.getDimensionPixelSize(R.dimen.detail_tree_zoom_item_height);
		mPaddingTop = res.getDimensionPixelSize(R.dimen.detail_tree_zoom_node_padding_top);
		mHeightTextSize = res.getDimensionPixelSize(R.dimen.detail_tree_zoom_item_height_text_size);
		mHeightTextMargin = res.getDimensionPixelSize(R.dimen.detail_tree_zoom_item_height_text_margin);
		mDateTextSize = res.getDimensionPixelSize(R.dimen.detail_tree_zoom_item_date_text_size);
		mDateTextMargin = res.getDimensionPixelSize(R.dimen.detail_tree_zoom_item_date_text_margin);
		mIndicatorWidth = res.getDimensionPixelSize(R.dimen.detail_tree_zoom_item_indicator_width);
		mIndicatorHeight = res.getDimensionPixelSize(R.dimen.detail_tree_zoom_item_indicator_height);
		mIndicatorMargin = res.getDimensionPixelSize(R.dimen.detail_tree_zoom_item_indicator_margin);
		mHeightPaint = new Paint();
		mHeightPaint.setTextSize(mHeightTextSize);
		mHeightPaint.setStyle(Paint.Style.FILL);
		mHeightPaint.setTypeface(Typeface.DEFAULT_BOLD);
		FontTextHelper.setFont(getContext(), mHeightPaint);
		mDatePaint = new Paint();
		mDatePaint.setTextSize(mDateTextSize);
		mDatePaint.setStyle(Paint.Style.FILL);
		mDatePaint.setTypeface(Typeface.DEFAULT_BOLD);
		FontTextHelper.setFont(getContext(), mDatePaint);
		mSimpleDateFormat = new SimpleDateFormat("yyyy.MM.dd");
		mTextList = new ArrayList<ZoomHeightItem>();
	}

	public ZoomHeightTree(Context context) {
		this(context, null);
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		if(oldw == 0 && oldh == 0 && w > 0 && h > 0) {
			setAvgHeightView();
		}
	}
	
	@Override
	protected void dispatchDraw(Canvas canvas) {
		super.dispatchDraw(canvas);
		if(mStartAnimation || getVisibility() == View.INVISIBLE)
			return;
		int left = (getMeasuredWidth()/2) - (mNodeWidth/2);
		int right = left + mNodeWidth;
		int lastOffset = mItemHeight/2;
		
		int height = getMeasuredHeight() - mPaddingTop - lastOffset;
		float interval = (float)height / (float)20;
		for(int i = 0; i < 21; i++) {
			canvas.drawBitmap(mTreeCoverBitmap, left, (float)(mPaddingTop+(interval*i)), null);
		}
		
		mTextList.clear();
		interval = (float)height / (float)200;
		int len = mHeightInfos.size();
		for(int i = 0; i < len; i++) {
			DetailFragment.HeightTreeItem cur = mHeightInfos.get(i);
			HeightInfo info = cur.heightInfo;
			String textHeight = info.getHeight()+"cm";
			String dateText = mSimpleDateFormat.format(info.getInputDate());
			float yPos = mPaddingTop + interval*(200f-info.getHeight());
			float heightWidth = mHeightPaint.measureText(textHeight);
			float dateWidth = mDatePaint.measureText(dateText);
			if(cur.isLeft) {
				canvas.drawBitmap(mIndicatorBitmap, left-mIndicatorWidth-mIndicatorMargin, yPos - mIndicatorHeight/2, null);
				canvas.drawText(textHeight, left-heightWidth-mHeightTextMargin, yPos - mDatePaint.ascent()/2, mHeightPaint);
				canvas.drawText(dateText, left-heightWidth-dateWidth-mHeightTextMargin-mDateTextMargin, yPos - mDatePaint.ascent()/2, mDatePaint);
				Rect rect = new Rect((int)(left-heightWidth-dateWidth-mHeightTextMargin-mDateTextMargin), 
						(int)(yPos - mDatePaint.ascent()/2), 
						(int)(left-mIndicatorWidth-mIndicatorMargin), 
						(int)(yPos - mDatePaint.ascent()/2 + mHeightTextSize));
				mTextList.add(new ZoomHeightItem(rect, (int)info.getHeight()));
			}
			else {
				canvas.drawBitmap(mIndicatorBitmap, right+mIndicatorMargin, yPos - mIndicatorHeight/2, null);
				canvas.drawText(textHeight, right+mHeightTextMargin, yPos - mDatePaint.ascent()/2, mHeightPaint);
				canvas.drawText(dateText, right+mHeightTextMargin+mDateTextMargin+heightWidth, yPos - mDatePaint.ascent()/2, mDatePaint);
				Rect rect = new Rect((int)(right+mHeightTextMargin), 
						(int)(yPos - mDatePaint.ascent()/2), 
						(int)(right+mHeightTextMargin+mDateTextMargin+heightWidth+dateWidth), 
						(int)(yPos - mDatePaint.ascent()/2 + mHeightTextSize));
				mTextList.add(new ZoomHeightItem(rect, (int)info.getHeight()));
			}
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int i, len = mTextList.size();
		for(i = 0; i < len; i++) {
			Rect rect = mTextList.get(i).rect;
			if(rect.contains((int)event.getX(), (int)event.getY())) {
				if(mListener != null) {
					mListener.changeMode(mTextList.get(i).height);
					return true;
				}
			}
		}
		return super.onTouchEvent(event);
	}
	
	public void changeZoomMode() {
		View addButton = findViewById(R.id.detail_tree_zoom_add);
		View avgHeight = findViewById(R.id.detail_height_tree_avg_height_container);
		addButton.setVisibility(View.GONE);
		avgHeight.setVisibility(View.GONE);
		final FullHeightTree trgView = (FullHeightTree)((Activity)getContext()).findViewById(R.id.detail_height_tree_list_view);
		Resources res = getResources();
		int srcWidth = res.getDimensionPixelSize(R.dimen.detail_tree_zoom_node_width);
		int trgWidth = res.getDimensionPixelSize(R.dimen.detail_tree_full_item_width);
		int srcHeight = getMeasuredHeight();
		int trgHeight = trgView.getMeasuredHeight();
		float widthRatio = (float)trgWidth / (float)srcWidth;
		float heightRatio = (float)trgHeight / (float)srcHeight;
		ScaleAnimation scaleAnimation = new ScaleAnimation(1, widthRatio, 1, heightRatio, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 1f);
		scaleAnimation.setDuration(DetailFragment.ZOOM_MODE_CHANGE_INTERVAL);
		scaleAnimation.setRepeatCount(0);
		scaleAnimation.setInterpolator(new Interpolator() {
			@Override
			public float getInterpolation(float arg0) {
				return arg0;
			}
		});
		scaleAnimation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				setVisibility(View.INVISIBLE);
				trgView.restoreVisibility();
				mStartAnimation = false;
				if(mListener != null) {
					mListener.onAnimationEnd();
				}
			}
		});
		mStartAnimation = true;
		startAnimation(scaleAnimation);
	}
	
	public void restoreVisibility() {
		View addButton = findViewById(R.id.detail_tree_zoom_add);
		View avgHeight = findViewById(R.id.detail_height_tree_avg_height_container);
		addButton.setVisibility(View.VISIBLE);
		if(mAveHeight > 0)
			avgHeight.setVisibility(View.VISIBLE);
		setVisibility(View.VISIBLE);
		requestLayout();
	}
	
	public void init(NickNameInfo info, ModeChangeListener listener) {
		if(info == null)
			return;
		mListener = listener;
		List<HeightInfo> heights = info.getHeightList();
		Collections.sort(heights, DetailFragment.HeightComparator);
		Collections.reverse(heights);
		mHeightInfos = new ArrayList<HeightTreeItem>();
		int len = heights.size();
		
		boolean pos = false;
		int lastHeight = -1;
		for(int i = 0; i < len; i++) {
			HeightInfo cur = heights.get(i);
			HeightTreeItem item = new HeightTreeItem();
			item.heightInfo = cur;
			
			if(lastHeight != (int)cur.getHeight()) {
				pos = !pos;
				lastHeight = (int)cur.getHeight();
				item.isLeft = pos;
				mHeightInfos.add(item);
			}
		}
		mIndicatorBitmap = BitmapFactory.decodeResource(getResources(), Util.getCharacterDrawable(info.getCharacter(), "detail_zoom_indicator"));
		mHeightPaint.setColor(getResources().getColor(info.getCharacter().getTagNameColor()));
		mDatePaint.setColor(getResources().getColor(info.getCharacter().getTagNameColor()));
//		invalidate();
	}
	
	public void setAvgHeightNAge(float height, int age) {
		mAveHeight = height;
		mAge = age;
		setAvgHeightView();
	}
	
	private void setAvgHeightView() {
		View avgHeight = findViewById(R.id.detail_height_tree_avg_height_container);
		DLog.e(TAG, "setAvgHeightView:"+mAveHeight);
		DLog.e(TAG, "setAvgHeightView:"+getMeasuredHeight());
		if(getMeasuredHeight() == 0 || mAveHeight <= 0f) {
			avgHeight.setVisibility(View.GONE);
			return;
		}
		
		int lastOffset = mItemHeight/2;
		int height = getMeasuredHeight() - mPaddingTop - lastOffset;
		float interval = (200f - mAveHeight)/200f;
		DLog.e(TAG, "setAvgHeightView interval:"+(interval));
		int margin = (int)(height * interval);
		DLog.e(TAG, "setAvgHeightView margin:"+(margin));
		
		TextView avgHeightText = (TextView)findViewById(R.id.detail_height_tree_avg_height_text);
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)avgHeight.getLayoutParams();
		avgHeightText.setText(getResources().getString(R.string.average_height_of_n, mAge));
		params.topMargin = mPaddingTop+margin;
		DLog.e(TAG, "setAvgHeightView margin:"+params.topMargin);
		avgHeight.setLayoutParams(params);
		avgHeight.setVisibility(View.VISIBLE);
	}
}
