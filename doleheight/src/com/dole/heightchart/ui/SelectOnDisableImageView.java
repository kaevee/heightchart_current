package com.dole.heightchart.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.dole.heightchart.R;

public class SelectOnDisableImageView extends ImageView {
	
	private Drawable mSelectionBoundDrawable;

	public SelectOnDisableImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public SelectOnDisableImageView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public SelectOnDisableImageView(Context context) {
		this(context, null);

	}
	
	private void init() {
		setScaleType(ScaleType.FIT_XY);
	}

	@Override
	public void setSelected(boolean selected) {
		super.setSelected(selected);
		if(selected) {
			mSelectionBoundDrawable = getResources().getDrawable(R.drawable.camera_frame_select);
			mSelectionBoundDrawable.setBounds(0, 0, getWidth(), getHeight());
		} else {
			mSelectionBoundDrawable = null;
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		if(mSelectionBoundDrawable != null) {
			mSelectionBoundDrawable.draw(canvas);
		}
	}
}
