package com.dole.heightchart.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.ImageView;

import com.dole.heightchart.DLog;
import com.dole.heightchart.R;

import java.util.HashMap;

public class AnimationView extends ImageView {	
	private static final String TAG = AnimationView.class.getName();
	
	private float[] mPositionX;
	private float[] mPositionY;
	private Paint mPaint;
	private String mHeight;
	private HashMap<Drawable, Integer> mDrawableMap;

	public AnimationView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public AnimationView(Context context) {
		this(context, null);
	}
	
	public void setTextPositionRes(int arrayRes, float height) {
		mHeight = String.valueOf(height);
		
		mPaint = new Paint();
		mPaint.setTextSize(getResources().getDimensionPixelSize(R.dimen.height_viewer_height_text_size));
		mPaint.setColor(getResources().getColor(android.R.color.white));
		FontTextHelper.setFont(getContext(), mPaint);

		DisplayMetrics metrics = getResources().getDisplayMetrics();
		int textWidth = (int)mPaint.measureText(mHeight)+10;

		String[] position = getResources().getStringArray(arrayRes);
		int i, len = position.length;
		mPositionX = new float[len];
		mPositionY = new float[len];
		for(i = 0; i < len; i++) {
			String ary[] = position[i].split(",");
			mPositionX[i] = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Float.valueOf(ary[0]), metrics)-textWidth;
			mPositionY[i] = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Float.valueOf(ary[1]), metrics);
		}
	}
	
	public void initAnimation() {
		mDrawableMap = new HashMap<Drawable, Integer>();
		AnimationDrawable drawable = (AnimationDrawable)getDrawable();
		int i, len = drawable.getNumberOfFrames();
		for(i = 0; i < len; i++) {
			mDrawableMap.put(drawable.getFrame(i), i);
		}
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(mDrawableMap != null && mPositionX != null && mPositionY != null) {
			AnimationDrawable drawable = (AnimationDrawable)getDrawable();
			Drawable current = drawable.getCurrent();
			int idx = mDrawableMap.get(current);
			if(idx >= 0 && idx < mPositionX.length) {
				if(mPositionX[idx] > 0 && mPositionY[idx] > 0)
					canvas.drawText(mHeight, mPositionX[idx], mPositionY[idx], mPaint);
			}
			else if(idx == mPositionX.length) {
				canvas.drawText(mHeight, mPositionX[idx-1], mPositionY[idx-1], mPaint);
			}
		}
	}
}
