
package com.dole.heightchart.ui;

import android.content.Context;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Transformation;
import android.widget.Gallery;

import com.dole.heightchart.R;


public class SoftCoverflow extends Gallery {

	private Camera mCamera = new Camera();
	private static float sMaxZoonIn = 1.58f;
	private static float sMaxVelocity = 2800;
	
	private float mTopOffset;
	
	private boolean mIsTouchedDown;
	private int mLastPosition;
	
	public SoftCoverflow(Context context) {
		this(context, null);
	}

	public SoftCoverflow(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public SoftCoverflow(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setStaticTransformationsEnabled(true);  
		setChildrenDrawingOrderEnabled(true);
	}
	
	@Override
	protected int getChildDrawingOrder(int childCount, int i) {
	    if (i == 0)
	    	mLastPosition = 0;

	    int centerPosition = getSelectedItemPosition() - getFirstVisiblePosition();

	    if (i == childCount - 1) {
	        return centerPosition;
	    } else if (i >= centerPosition) {
	    	mLastPosition++;
	        return childCount - mLastPosition;
	    } else {
	        return i;
	    }
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		final float widgetHeight = getResources().getDimensionPixelSize(R.dimen.addnick_coverflow_height);
		final float itemTopOffset = getResources().getDimensionPixelSize(R.dimen.addnick_coverflow_top_padding);
		mTopOffset =  1.0f * getHeight() / widgetHeight * itemTopOffset;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			mIsTouchedDown = true;
			break;
		case MotionEvent.ACTION_UP:
			mIsTouchedDown = false;
		default:
			break;
		}
		return super.onTouchEvent(event);
	}

	public boolean isTouchedDown() {
		return mIsTouchedDown;
	}

	@Override
	protected boolean getChildStaticTransformation(View child, Transformation t) {
		t.clear();
		t.setTransformationType(Transformation.TYPE_MATRIX);
		
		final View titleView = child.findViewById(R.id.add_nickname_avatar_name);
		final View spotLight = child.findViewById(R.id.add_nickname_avatar_spot_light);
		
		final Matrix matrix = t.getMatrix();;
		final int childCenterX = child.getWidth() / 2;
		final int childCenterY = child.getHeight() / 2;
		final int childCenter = getCenterOfView(child);
		
		mCamera.save();
		final float childOffset = getCenterOfCoverflow() - childCenter * 1.0f;
		final float startDistance = child.getWidth() / 4.0f; 
		final float stopDistance = getWidth() / 2.0f - startDistance; 
		final float absChildOffset = Math.abs(childOffset);
		float scale = sMaxZoonIn; //1.0f;
		boolean changeToInvisible = true;
		
		if(absChildOffset <= startDistance) {
			changeToInvisible = false;
			scale = sMaxZoonIn;
		} else if(absChildOffset > startDistance && absChildOffset <= stopDistance) {
			final float progress = (absChildOffset - startDistance) / (stopDistance - startDistance);
			scale = sMaxZoonIn - (sMaxZoonIn - 1.0f) * progress;
		} else {
			scale = 1.0f;
		} 
		mCamera.translate(0, -mTopOffset, 0);

		mCamera.getMatrix(matrix);
		matrix.preScale(scale, scale, childCenterX, childCenterY);
		
		mCamera.restore();
		
		if(changeToInvisible && getSelectedItem() != child) {
			titleView.setVisibility(View.INVISIBLE);
			spotLight.setVisibility(View.INVISIBLE);
		}
		
		child.invalidate();
		
		return true;
	}
	
	private int getCenterOfCoverflow() {
		return (getWidth() - getPaddingLeft() - getPaddingRight()) / 2 + getPaddingLeft();
	}
	
	private static int getCenterOfView(View view) {
		return view.getLeft() + view.getWidth() / 2;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {

		//Limit velocity		
		float newVelocityX = Math.min(Math.abs(velocityX), sMaxVelocity); 

		if(velocityX < 0)
			newVelocityX *= -1;
		return super.onFling(e1, e2, newVelocityX, velocityY);
	} 

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return true; //super.onSingleTapUp(e);
	}
}
