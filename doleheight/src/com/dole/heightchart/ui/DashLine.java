package com.dole.heightchart.ui;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class DashLine {
	
	private PointF mStart;
	private PointF mEnd;
	private float mGap;
	private int mDashWidth;
	private int mDashHeight;
	private Rect mBound;
	private Drawable mDashImage;
	
	private Point[] mPoints;
	
	public DashLine(Drawable dashImage, int dashWidth, int dashHeight, int gap, PointF start, PointF end) {
		mDashImage = dashImage;
		mDashWidth = dashWidth;
		mDashHeight = dashHeight;
		mGap = gap;
		mBound = new Rect(0, 0, dashWidth, dashHeight);
		mStart = start;
		mEnd = end;
		
		final float rad = angle(mStart, mEnd);
		final long dashDistX = Math.round(mGap * Math.cos(rad));
		final long dashDistY =  Math.round(mGap * Math.sin(rad));
		
		int loopX = (int)Math.abs((mStart.x - mEnd.x) / dashDistX);
		int loopY = (int)Math.abs((mStart.y - mEnd.y) / dashDistY);
		int loop = Math.max(loopX, loopY);
		
		mPoints = new Point[loop];
		
		for(int i = 0; i < loop; i++) {
			mPoints[i] = new Point((int)(mStart.x + i * dashDistX),
					(int)(mStart.y + i * dashDistY));
		}
 	}
	
	public void drawLine(Canvas canvas) {
		if(mDashImage == null || mBound == null || mStart == null || mEnd == null)
			return;

		for(Point point : mPoints) {
			mBound.offsetTo((int)(point.x - mDashWidth / 2), (int)(point.y - mDashHeight - 2));
			mDashImage.setBounds(mBound);
			mDashImage.draw(canvas);
		}
	}
	
	/** Get rotation from touch */
    private float angle(PointF start, PointF end) {
    	double deltaX = (end.x - start.x);
    	double deltaY = (end.y - start.y);

    	return (float) Math.atan2(deltaY, deltaX);
    }
}
