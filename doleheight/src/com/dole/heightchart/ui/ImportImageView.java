package com.dole.heightchart.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewParent;
import android.view.ViewTreeObserver.OnPreDrawListener;

public class ImportImageView extends View {

	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			if(mDrawMatrix != null) {
				float scale = detector.getScaleFactor();
				mDrawMatrix.postScale(scale, scale, mPivotPoint.x, mPivotPoint.y);
			}
			invalidate();
			return true;
		}
	}
	
	private boolean isPinching = false;
    private boolean isPanning = false;

    private PointF mPivotPoint;
    private PointF mTmpTouchCurrent = new PointF();
    private PointF mTmpTouchPrev = new PointF();
    
    private float mPrevRotation = 0f;
    
    private float mImageInitScale;
	private Matrix mDrawMatrix;
	private Drawable mImage;
	private boolean mIsFinishMeasure = false;
	
	private float[] mMatrixNineValue = new float[9];
	
	private ScaleGestureDetector mScaleGestureDetector;

	public ImportImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public ImportImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ImportImageView(Context context) {
		super(context);
	}
	
	private void update() {
		if(!mIsFinishMeasure) {
			getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
				@Override
				public boolean onPreDraw() {
					mIsFinishMeasure = true;
					update();
					getViewTreeObserver().removeOnPreDrawListener(this);
					return true;
				}
			});
		}
		mScaleGestureDetector = new ScaleGestureDetector(getContext(), new ScaleListener());
		mImage.setBounds(0, 0, mImage.getIntrinsicWidth(), mImage.getIntrinsicHeight());

		mDrawMatrix = new Matrix();
		Drawable image = mImage;

		int dwidth = image.getIntrinsicWidth();
		int dheight = image.getIntrinsicHeight();

		int vwidth = getWidth() - getPaddingLeft() - getPaddingRight();
		int vheight = getHeight() - getPaddingTop() - getPaddingBottom();
		mImageInitScale = Math.min((float) vwidth / (float) dwidth,
				(float) vheight / (float) dheight);
		float dx = (int) ((vwidth - dwidth * mImageInitScale) * 0.5f + 0.5f);
		float dy = (int) ((vheight - dheight * mImageInitScale) * 0.5f + 0.5f);
		int[] loc = new int[2];
		getLocationOnScreen(loc);
		mDrawMatrix.setScale(mImageInitScale, mImageInitScale);
		mDrawMatrix.postTranslate(loc[0], loc[1]);
		mDrawMatrix.postTranslate(dx, dy);
	}
	
	public void setImageDrawable(Drawable drawable) {
		mImage = drawable;
		update();
	}

	public void setImageBitmap(Bitmap bm) {
		setImageDrawable(new BitmapDrawable(getContext().getResources(), bm));
		update();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		mIsFinishMeasure = true;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		if(mImage != null && mDrawMatrix != null) {
			canvas.setMatrix(mDrawMatrix);
			mImage.draw(canvas);

        }
	}
	
	public Bitmap getCurrentImageBitmap() {
    	if(mImage != null) {
    		final Bitmap image = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
    		Canvas canvas = new Canvas(image);
    		canvas.drawColor(Color.BLACK);
    		int[] loc = new int[2];
    		getLocationOnScreen(loc);
    		mDrawMatrix.postTranslate(-loc[0], -loc[1]);
    		canvas.setMatrix(mDrawMatrix);
            mImage.draw(canvas);
            
            return image;
        } else {
        	return null;
        }
    }

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		if(!isEnabled())
			return isClickable();
		boolean spanned = pinchAndSpanMotion(event); 
		boolean scaled = false;
		if(isPinching)
			scaled = mScaleGestureDetector.onTouchEvent(event);
		
		return (scaled || spanned);
	}

	public boolean pinchAndSpanMotion(MotionEvent event) {
		switch(event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN: {

			ViewParent parent = getParent();
			if(null != parent)
				parent.requestDisallowInterceptTouchEvent(true);
			isPanning = true;
			break;
		}
		case MotionEvent.ACTION_POINTER_DOWN: {
			if(event.getPointerCount() == 2) {
				if(mPivotPoint == null)
					mPivotPoint = new PointF();
				midPoint(mPivotPoint, event);
				isPinching = true;
				
				mPrevRotation = rotation(event);
			}

			break;
		}

		case MotionEvent.ACTION_CANCEL: {
			isPinching = false;
			isPanning = false;
			break;
		}

		case MotionEvent.ACTION_POINTER_UP:
			isPinching = false;
			break;
		case MotionEvent.ACTION_UP:
			isPanning = false;
			break;
		case MotionEvent.ACTION_MOVE:
			final int historySize = event.getHistorySize();
			if(event.getHistorySize() == 0)
				break;
			
			final int indexOfLast = historySize - 1;
			if(event.getPointerCount() == 1) {
				float prevX = event.getHistoricalX(indexOfLast);
				float prevY = event.getHistoricalY(indexOfLast);
				
				mDrawMatrix.postTranslate(
						(event.getX() - prevX) * getSkewScaleX(),
						(event.getY() - prevY) * getSkewScaleY());
				
			} else if(event.getPointerCount() == 2) {
				midPoint(mTmpTouchCurrent, event);
				midPointPrev(mTmpTouchPrev, event, indexOfLast);
				mDrawMatrix.postTranslate(
						(mTmpTouchCurrent.x - mTmpTouchPrev.x) * getSkewScaleX(),
						(mTmpTouchCurrent.y - mTmpTouchPrev.y) * getSkewScaleY());
				
				mDrawMatrix.postRotate(rotation(event) - mPrevRotation, mPivotPoint.x, mPivotPoint.y);
				mPrevRotation = rotation(event);
				
			}
			invalidate();
			break;
		}

		return isPanning | isPinching;
	}
	
	private float getSkewScaleX() {
		mDrawMatrix.getValues(mMatrixNineValue);
		float scalex = mMatrixNineValue[Matrix.MSCALE_X];
		float skewy = mMatrixNineValue[Matrix.MSKEW_Y];
		return (float) Math.sqrt(scalex * scalex + skewy * skewy);
	}
	
	private float getSkewScaleY() {
		mDrawMatrix.getValues(mMatrixNineValue);
		float scaley = mMatrixNineValue[Matrix.MSCALE_Y];
		float skewx = mMatrixNineValue[Matrix.MSKEW_X];
		return (float) Math.sqrt(scaley * scaley + skewx * skewx);
	}
	
	/** Mid point of the two fingers */
    private void midPoint(PointF point, MotionEvent event) {
        
        final float count = event.getPointerCount();
        float x = 0f;
        float y = 0f;
        for(int i = 0 ; i < count; i++) {
            x += event.getX(i);
            y += event.getY(i);
        }
        point.set(x / count, y / count);
    }
    
    /** Mid point of the two fingers */
    private void midPointPrev(PointF point, MotionEvent event, int indexOfLast) {
        
    	float prevX0 = event.getHistoricalX(0, indexOfLast);
		float prevY0 = event.getHistoricalY(0, indexOfLast);
		float prevX1 = event.getHistoricalX(1, indexOfLast);
		float prevY1 = event.getHistoricalY(1, indexOfLast);
		
		point.set((prevX0 + prevX1) / 2, (prevY0 + prevY1) / 2);
    }
    
    /** Get rotation from touch */
    private float rotation(MotionEvent event) {
    	final float count = event.getPointerCount();
    	if(count < 2) 
    		return 0;
    	
    	double deltaX;
    	double deltaY;
    	deltaX = (event.getX(0) - event.getX(1));
    	deltaY = (event.getY(0) - event.getY(1));
    	double radians = Math.atan2(deltaY, deltaX);
    	return (float) Math.toDegrees(radians);
    }
}
