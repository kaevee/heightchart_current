package com.dole.heightchart.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class FontTextView extends TextView {
	
	public FontTextView(Context context) {
		super(context);
		FontTextHelper.setFont(getContext(), this);
	}
	
	public FontTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		FontTextHelper.setFont(getContext(), this);
	}

	public FontTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		FontTextHelper.setFont(getContext(), this);
	}
}
