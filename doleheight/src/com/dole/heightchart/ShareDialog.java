package com.dole.heightchart;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;

public class ShareDialog extends DialogFragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		final Dialog d = getDialog();
		d.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		
		int divierId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = getDialog().findViewById(divierId);
		divider.setVisibility(View.GONE);
		
		int titleId = d.getContext().getResources().getIdentifier("android:id/title", null, null);
		View title = getDialog().findViewById(titleId);
		title.setVisibility(View.GONE);
		
		int titleHeight = getResources().getDimensionPixelSize(R.dimen.dialog_title_height);
		int buttonHeight = getResources().getDimensionPixelSize(R.dimen.dialog_button_height);
		int itemHeight = getResources().getDimensionPixelSize(R.dimen.dialog_item_height);
		int totalHeight = titleHeight + buttonHeight + itemHeight;
		
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
	    lp.copyFrom(d.getWindow().getAttributes());
	    lp.height = totalHeight;
	    d.getWindow().setAttributes(lp);
		
		return inflater.inflate(R.layout.share_chooser, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		View facebook = view.findViewById(R.id.share_facebook);
		View button = view.findViewById(R.id.share_cancel_button);
		
		facebook.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
			}
		});
		
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}
}
