package com.dole.heightchart;


public class Constants {
	public static final boolean DEBUG = false;
	public static final boolean USE_STAGING_SERVER = false;
	public static final boolean DEBUG_KEEP_APP_FIRST_LAUNCH = false;
	public static final boolean DEBUG_BYPASS_ALL_QRCODE = false;
	public static final boolean DEBUG_QRCODE_PREVIEW_AREA = false;

	public static final boolean USE_CA_REWARD = true;

	static final String SERVICE_CODE = "SVR002";
	static final String PAYMENT_METHOD_CODE = "PMC003";
	static final String PAYMENT_VALIDITY_PERIOD = "3650";
	static final String COUPON_NUMBER = "CP";
	static final String COUPON_TYPE = "T";
	public static final String OS_STRING = "Android";
	public static final String SNS_CODE = "SNS001";

	public static final String NONAME = "No name";

	static final long UNAVAILABLE_ID = -1;

	public enum Avatars {
		DOLEKEY(0, R.drawable.add_nickname_center_monkey_01, R.string.dolkey),
		RILLA(1, R.drawable.add_nickname_center_monkey_02, R.string.rilla),
		TEENY(2, R.drawable.add_nickname_center_monkey_03, R.string.teeny),
		COCO(3, R.drawable.add_nickname_center_monkey_04, R.string.coco),
		PANZEE(4, R.drawable.add_nickname_center_monkey_05, R.string.panzee);

		private int index;
		private int imageId;
		private int nameId;
		Avatars(int index, int imageResId, int nameResId) {
			this.index = index;
			this.imageId = imageResId;
			this.nameId = nameResId;
		}

		public int getIndex() {
			return  index;
		}

		public int getImageId() {
			return imageId;
		}

		public int getNameId() {
			return nameId;
		}
	}

	public enum Characters {
		CHARACTER_DOLEKEY(0, R.color.name_tag_text_dolekey),
		CHARACTER_RILLA(1, R.color.name_tag_text_rilla),
		CHARACTER_TEENY(2, R.color.name_tag_text_smong),
		CHARACTER_COCO(3, R.color.name_tag_text_faka),
		CHARACTER_PANZEE(4, R.color.name_tag_text_panzee),
		ADD_NICKNAME_OWL(5, 0),
		CHARACTER_OWL(6, 0);

		private int index;
		private int tagNameColor;
		private Characters(int index, int tagNameColor) {
			this.index = index;
			this.tagNameColor = tagNameColor;
		}

		public int getCharacterIndex() {
			return index;
		}

		public int getTagNameColor() {
			return tagNameColor;
		}

		static Characters getCharacterAt(int index) {
			for(Characters character : Characters.values()) {
				if(character.index == index)
					return character;
			}
			return CHARACTER_DOLEKEY; //Default is first value
		}
	}

	enum CouponType {
		LION("A01"),
		BIRD("A02"),
		FOX("A03"),
		ALLIGATOR("A04"),
		RACCOON("A05"),
		;

		String type;
		private CouponType(String t) {
			type = t;
		}

		public static CouponType getType(String typeStr) {
			for(CouponType cType : values()) {
				if(cType.type.equals(typeStr))
					return cType;
			}
			return null;
		}
	}

	public enum GenderType {
		MALE(1),
		FEMALE(2);

		int type;
		GenderType(int type) {
			this.type = type;
		}
	}

	//GCM
	public static final String PROPERTY_REG_ID = "property_reg_id";
	public static final String PROPERTY_APP_VERSION = "property_app_version";

	public static final String NICKNAME_GENDER_BOY = "boy";
	public static final String NICKNAME_GENDER_GIRL = "girl";

	// LocalBroadCast
	public static final String ACTION_LOAD_FINISHED = "action_load_finished";
	public static final String ACTION_ADD_NICK_FINISHED = "action_add_finished";
	public static final String ACTION_DELETE_NICK_FINISHED = "action_delete_nick_finished";
	public static final String ACTION_DELETE_HEIGHT_FINISHED = "action_dekete_height_finished";

	// Extra keys
	public static final String NICKNAME_ID = "nickname_id";
	public static final String HEIGHT_ID = "height_id";
	public static final String HEIGHT = "height";
	public static final String URI = "uri";
	public static final String MODE = "mode";
	public static final String IS_IMPORTED = "is_imported";
	public static final String CLICK_NEXT = "click_next";
	public static final String EVENT_URL = "event_url";
	public static final String EMAIL = "email";
	public static final String QRCODE = "qrcode";
	public static final String PROMOTION_TYPE = "promotion_type";
	public static final String IS_VIEWER_CONFIRM = "is_viewer_confirm";
	public static final String FACEBOOK_USER_ID = "facebook_user_id";
	public static final String REUSE_HEIGHT_ID = "reuse_height_id";
	public static final String BYPASS_DETAIL_FRAG = "bypass_detail_frag";
	public static final String CITY = "city";
	public static final String USED_CODE_WITHIN_24H = "used_code_withing_24h";

	public static final int MAX_HEIGHT = 170;
	public static final int MIN_HEIGHT = 50;

	//Requests through apps
	public static final int REQUEST_CODE_DATE_DIALOG = 1;
	public static final int REQUEST_CODE_HEIGHT = 2;
	public static final int REQUEST_PICK_ALBUM_PHOTO = 3;
	public static final int REQUEST_IMPORT_IMAGE = 4;
	public static final int REQUEST_CODE_AGREE_PERSONAL = 5;
	public static final int REQUEST_USER_CONFIRM = 6;
	public static final int REQUEST_DELETE_USER_INFO = 7;
	public static final int REQUEST_UPDATE_NICK_INFO = 8;
	public static final int REQUEST_FORGOTTEN_PASS_EMAIL = 9;
	public static final int REQUEST_USER_NOTICE_FAILURE = 10;
	public static final int REQUEST_USER_CONFIRM_SHARE = 11;
	public static final int REQUEST_VIEW_GRAPH = 12;
	public static final int REQUEST_SHARE_VIEWER = 13;
	public static final int REQUEST_USER_NOTICE_REUSE_USED_CODE = 14;
	public static final int REQUEST_DOLE_COIN = 15;
	public static final int REQUEST_CITY = 16;
	public static final int REQUEST_CONFIRM_SHARE = 17;
	public static final int REQUEST_DIALOG_UNAVAILABLE_CODE = 18;
	public static final int REQUEST_DIALOG_BACKUP_RESTORE = 19;
	public static final int REQUEST_DIALOG_CHOOSE_RESTORE = 20;
	public static final int REQUEST_CONFIRM_BACKUP = 21;
	public static final int REQUEST_CONFIRM_RESTORE = 22;
	public static final int REQUEST_CONFIRM_RESTORE_COMPLETE = 23;
	public static final int REQUEST_VERSION_CHECK = 24;

	// Fragments
	public static final String TAG_FRAGMENT_SPLASH = "frag_splash";
	public static final String TAG_DIALOG_BIRTHDAY_PICKER = "dialog_birthday_picker";
	public static final String TAG_DIALOG_ADD_NICK = "frag_dialog_choose_nick";
	public static final String TAG_DIALOG_GUIDE= "frag_dialog_guide";
	public static final String TAG_DIALOG_CUSTOM = "frag_dialog_custom";
	public static final String TAG_DIALOG_CITY = "frag_dialog_city";
	public static final String TAG_DIALOG_SHARE = "frag_dialog_share";
	public static final String TAG_DIALOG_BACKUP = "frag_dialog_backup";

	public static final String TAG_FRAGMENT_LOGIN = "frag_login";
	public static final String TAG_FRAGMENT_FORGOT_PASSWORD = "frag_forgot_password";
	public static final String TAG_FRAGMENT_USER_DETAIL = "frag_user_detail";
	public static final String TAG_FRAGMENT_FACEBOOK_LOGIN = "frag_facebook_login";
	public static final String TAG_FRAGMENT_ADD_NICK = "frag_add_nick";
	public static final String TAG_FRAGMENT_LOGIN_BEFORE_OPTION = "frag_login_before_option";

	public static final String TAG_FRAGMENT_DETAIL = "frag_detail";
	public static final String TAG_FRAGMENT_CHART = "frag_chart";
	public static final String TAG_FRAGMENT_CAMERA = "frag_camera";
	public static final String TAG_FRAGMENT_QRCODE = "frag_qrcode";
	public static final String TAG_FRAGMENT_INPUT_HEIGHT = "frag_input_height";
	public static final String TAG_FRAGMENT_IMPORT_PHOTO = "frag_import_photo";
	public static final String TAG_FRAGMENT_ABOUT = "frag_about";
	public static final String TAG_FRAGMENT_HELP = "frag_help";
	public static final String TAG_FRAGMENT_DELETE_ACCOUNT = "frag_delete_account";
	public static final String TAG_FRAGMENT_HEIGHT_VIEWER = "frag_height_viewer";
	public static final String TAG_FRAGMENT_DOLE_COIN = "frag_dole_coin";
	public static final String TAG_FRAGMENT_AGREE_PERSONAL_INFO = "frag_agree_personal_info";
	public static final String TAG_FRAGMENT_INVITE_FACEBOOK = "frag_invite_facebook";
	public static final String TAG_FRAGMENT_EVENT = "frag_event";
	public static final String TAG_FRAGMENT_REQUEST_PAPER = "frag_request_paper";

	// Server api tags
	public static final String TAG_RETURN = "Return";
	public static final String TAG_RETURN_CODE = "ReturnCode";

	public static final String TAG_USER_ID = "UserID";
	public static final String TAG_PASSWORD = "Password";
	public static final String TAG_OLD_PASSWORD = "OldPassword";
	public static final String TAG_NEW_PASSWORD = "NewPassword";
	public static final String TAG_EMAIL = "Email";
	public static final String TAG_BIRTHDAY = "BirthDay";
	public static final String TAG_GENDER = "Gender";
	public static final String TAG_LANGUAGE = "Language";
	public static final String TAG_SNS_CODE = "SnsCode";
	public static final String TAG_SNS_ID = "SnsID";
	public static final String TAG_SNSID = "SNSID";
	public static final String TAG_SNS_USER_NAME = "SnsUserName";
	public static final String TAG_STANDARD_COUNTRY_CODE = "StandardCountryCode";
	public static final String TAG_UUID = "UUID";
	public static final String TAG_ADDRESS_MAIN = "AddressMain";
	public static final String TAG_CLIENT_IP = "ClientIP";
	public static final String TAG_USER_NO = "UserNo";
	public static final String TAG_AUTHKEY = "AuthKey";
	public static final String TAG_REASON = "Reason";
	public static final String TAG_COUNTRY = "Country";
	public static final String TAG_SNS_TYPE = "SNSType";
	public static final String TAG_SERVICE_CODE = "ServiceCode";
	public static final String TAG_MOBILE_OS = "MobileOS";
	public static final String TAG_NICKNAME = "Nickname";
	public static final String TAG_EMAIL_VALIFICATION = "EmailValification";
	public static final String TAG_EMAIL_ACTIVATE_DATE_TIME = "EmailActivateDatetime";
	public static final String TAG_CHANGE_PASSWROD_DATE_TIME = "ChangePasswrodDateTime";
	public static final String TAG_REGISTER_DATE_TIME = "RegisterDatetime";
	public static final String TAG_PROVINCES = "Provinces";
	public static final String TAG_MAJOR_VERSION = "MajorVersion";
	public static final String TAG_DEVICE_TOKEN = "DeviceToken";
	public static final String TAG_USER_TYPE = "UserType";
	public static final String TAG_SERIAL = "Serial";
	public static final String TAG_PROMOTION_INFO = "PromotionInfo";
	public static final String TAG_CAN_USE = "CanUse";
	public static final String TAG_PRODUCTI_NFOS = "ProductInfos";
	public static final String TAG_ORDER_ID = "OrderID";
	public static final String TAG_PAYMENT_METHOD_CODE = "PaymentMethodCode";
	public static final String TAG_PAYMENT_AMOUNT = "PaymentAmount";
	public static final String TAG_CHARGE_AMOUNT = "ChargeAmount";
	public static final String TAG_VALIDITY_PERIOD = "ValidityPeriod";
	public static final String TAG_DESCRIPTION = "Description";
	public static final String TAG_REAL_BALANCE = "RealBalance";
	public static final String TAG_EVENT_BALANCE = "EventBalance";
	public static final String TAG_APPROVAL_ID = "ApprovalID";
	public static final String TAG_CONTENT_TYPE = "ContentType";
	public static final String TAG_CONTENT_GROUP_NO = "ContentGroupNo";
	public static final String TAG_LANGUAGE_NO = "LanguageNo";
	public static final String TAG_CONTENT_GROUP_CLASS_NO = "ContentGroupClassNo";
	public static final String TAG_NOTICE_STATUS = "NoticeStatus";
	public static final String TAG_TOTAL_ROW_COUNT = "TotalRowCount";
	public static final String TAG_NOTICE_ITEMS = "NoticeItems";
	public static final String TAG_PAGE_INDEX = "PageIndex";
	public static final String TAG_ROW_PER_PAGE = "RowPerPage";
	public static final String TAG_IS_RECEIVE_EMAIL = "ISReceiveEmail";
	public static final String TAG_CATEGORY_NO = "CategoryNo";
	public static final String TAG_SUB_CATEGORY_NO = "SubCategoryNo";
	public static final String TAG_INQUIRY_TITLE = "InquiryTitle";
	public static final String TAG_INQUIRY_CONTENT = "InquiryContent";
	public static final String TAG_OCCURRENCE_DATE_TIME = "OccurrenceDateTime";
	public static final String TAG_CS_CATEGORY_ITEMS = "CSCategoryItems";
//	public static final String TAG_SPLASH_ANIMATION = "SplashAnimation";

	// Preference keys
	public static final String PREF_IS_BGM_ON = "is_bgm_on_pref";
	public static final String PREF_IS_FIRST_APP_LAUNCH = "is_first_app_launch";
	public static final String PREF_SAMPLE_QRCODE_USED = "sample_qrcode_used";
	public static final String PREF_USER_ID = TAG_USER_ID;
	public static final String PREF_USER_NO = TAG_USER_NO;
	public static final String PREF_AUTH_KEY = TAG_AUTHKEY;
	public static final String PREF_EMAIL_VALIFICATION = TAG_EMAIL_VALIFICATION;
	public static final String PREF_EMAIL_ACTIVATE_DATE_TIME = TAG_EMAIL_ACTIVATE_DATE_TIME;
	public static final String PREF_CHANGE_PASSWROD_DATE_TIME = TAG_CHANGE_PASSWROD_DATE_TIME;
	public static final String PREF_REGISTER_DATE_TIME = TAG_REGISTER_DATE_TIME;
	public static final String PREF_COUNTRY = TAG_COUNTRY;
	public static final String PREF_LANGUAGE = TAG_LANGUAGE;
	public static final String PREF_GENDER = TAG_GENDER;
	public static final String PREF_BIRTHDAY = TAG_BIRTHDAY;
	public static final String PREF_ADDRESS_MAIN = TAG_ADDRESS_MAIN;
	public static final String PREF_LOGIN_TYPE = "LoginType";
	public static final String PREF_LAST_DOLE_COIN = "LastDoleCoin";
	public static final String PREF_NOT_SHOW_EVENT = "NotShowEvent";
	public static final String PREF_COIN_TO_ADDED = "CoinToAdded";
	public static final String PREF_REQUEST_PAPER = "RequestPaper";
	public static final String PREF_LAST_BACKUP_DATE = "LastBackupDate";


	//login type
	public static final int LOGIN_TYPE_EMAIL = 0;
	public static final int LOGIN_TYPE_FACEBOOK = 1;


	//return code from server
	public static final int ERROR_CODE_PARAM = 90001;	//파라미터 오류
	public static final int ERROR_CODE_NO_RECORD = 90002;	//해당 레코드 없음
	public static final int ERROR_CODE_EXIST_ID = 90101;	//user id 중복 오류
	public static final int ERROR_CODE_EXIST_EMAIL = 90102;	//이메일 중복 오류
	public static final int ERROR_CODE_NO_DATA = 90105;	//사용자데이터 없음
	public static final int ERROR_CODE_CHECK_PWD = 90106;	//비밀번호확인 오류
	public static final int ERROR_CODE_UNREGIST_SNS = 90107;	//등록되지 않은 SNS계정
	public static final int ERROR_CODE_USER_REG = 90108;	//사용자 등록 실패
	public static final int ERROR_CODE_USER_PROF_REG = 90109;	//사용자 프로필 등록 실패
	public static final int ERROR_CODE_USER_LV_REG = 90110;	//사용자 등급 등록 실패
	public static final int ERROR_CODE_SNS_REG = 90111;	//SNS 계정 등록 실패
	public static final int ERROR_CODE_SNS_ID = 90113;	//SNS 로 등록된 계정
	public static final int ERROR_CODE_FACEBOOK_ID = 90114;	//SNS 계정 등록 실패
	public static final int ERROR_CODE_ID_LEN = 90126;	//user id 길이 오류
	public static final int ERROR_CODE_PWD_LEN = 90127;	//password 길이 오류
	public static final int ERROR_CODE_AUTHKEY_EXPIRED = 90202;	//인증키 만료
	public static final int ERROR_CODE_LOGIN_LIMIT = 90211;	//로그인 실패 횟수 초과
	public static final int ERROR_CODE_USELESS_AUTH_KEY = 90213;	//유효하지 않은 인증키
	public static final int ERROR_CODE_INVALID_AUTH_KEY = 90214;	//인증키 정보 불일치

	//custom dialog
	public static final String CUSTOM_DIALOG_IS_TITLE = "custom_dialog_is_title";
	public static final String CUSTOM_DIALOG_TITLE_RES = "custom_dialog_title_res";
	public static final String CUSTOM_DIALOG_IMG_RES = "custom_dialog_img_res";
	public static final String CUSTOM_DIALOG_TEXT_RES = "custom_dialog_text_res";
	public static final String CUSTOM_DIALOG_TEXT_RES_PARAM1 = "custom_dialog_text_res_param1";
	public static final String CUSTOM_DIALOG_TEXT_STRING = "custom_dialog_text_string";
	public static final String CUSTOM_DIALOG_LEFT_BUTTON_RES = "custom_dialog_left_button_res";
	public static final String CUSTOM_DIALOG_RIGHT_BUTTON_RES = "custom_dialog_right_button_res";
	public static final String CUSTOM_DIALOG_LEFT_BUTTON_TEXT_RES = "custom_dialog_left_button_text_res";
	public static final String CUSTOM_DIALOG_RIGHT_BUTTON_TEXT_RES = "custom_dialog_right_button_text_res";
	public static final String CUSTOM_DIALOG_CHANGE_TEXT_YES_NO = "custom_dialog_yes_no";
	public static final String CUSTOM_DIALOG_ALLOW_CANCEL = "custom_dialog_allow_cancel";

	public static final int PASSWORD_MIN_LENGTH = 6;

	public static final String COUNTRY_KR = "KR";
	public static final String COUNTRY_NZ = "NZ";
	public static final String COUNTRY_PH = "PH";
	public static final String COUNTRY_JP = "JP";
	public static final String COUNTRY_SG = "SG";

	public static final String LANGUAGE_KO = "ko";
	public static final String LANGUAGE_JA = "ja";
	public static final String LANGUAGE_EN = "en";

	public static final String FAQ_URL = "http://www.doleapps.com";
}
