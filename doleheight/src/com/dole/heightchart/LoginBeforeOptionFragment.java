package com.dole.heightchart;

import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.dole.heightchart.camera.Storage;
import com.dole.heightchart.ui.CustomDialog;
import com.facebook.Session;
import com.facebook.Session.NewPermissionsRequest;
import com.facebook.SessionState;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class LoginBeforeOptionFragment extends Fragment {
	private static final String TAG = LoginBeforeOptionFragment.class.getName();
	
	private boolean mIsLoggedIn = false;
	private RequestAuthSessionStatusCallback mSessionCallback = new RequestAuthSessionStatusCallback();
	
	public static boolean mDoNotShowToast = false;
	public static String BACKUP_DIR = "HeightChart";
	public static String BACKUP_FILE_NAME = "height_chart_backup_";
	
	private static final List<String> mFriendsPermission = new ArrayList<String>() {
        {
            add("user_friends");
            add("public_profile");
        }
    };
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mIsLoggedIn = HeightChartPreference.getString(Constants.PREF_USER_ID, "").equals("") ? false : true;
		if(mIsLoggedIn)
			return inflater.inflate(R.layout.login_option_menu_after_login, null);
		else
			return inflater.inflate(R.layout.login_option_menu_before_login, null);
	}

	@Override
	public void onViewCreated(View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		Resources res = getResources();
		final Button login = (Button) view.findViewById(R.id.login_before_btn_login);
		final View leave = view.findViewById(R.id.login_option_leave);
		View help = view.findViewById(R.id.login_before_help);
		View about = view.findViewById(R.id.login_before_about);
		TextView emailText = (TextView)view.findViewById(R.id.login_before_email_text);
		TextView lastBackupText = (TextView)view.findViewById(R.id.login_before_text_backup_last);
		TextView backupBtn = (TextView)view.findViewById(R.id.login_before_btn_backup);
		Button facebookInviteButton = (Button) view.findViewById(R.id.login_before_invite_facebook);
		final View requestContainer = view.findViewById(R.id.login_request_paper_container);
		View requestPaper = view.findViewById(R.id.login_request_paper_text);
		View requestX = view.findViewById(R.id.login_request_paper_btn);
		Locale locale = getResources().getConfiguration().locale;
		String country = locale.getCountry();
		boolean isRequestPaper = HeightChartPreference.getBoolean(Constants.PREF_REQUEST_PAPER, false);
		
		if(mDoNotShowToast || !Constants.COUNTRY_NZ.equals(country) || isRequestPaper)
			requestContainer.setVisibility(View.GONE);
		
		facebookInviteButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				Session session = Session.getActiveSession();
				DLog.e(TAG, "session:"+session);
				if (session == null) {
					if (savedInstanceState != null) {
						DLog.e(TAG, "savedInstanceState:"+savedInstanceState);
						session = Session.restoreSession(getActivity(), null, mSessionCallback, savedInstanceState);
					}
					if (session == null) {
						session = new Session(getActivity());
					}
					Session.setActiveSession(session);
				}
				
				if(ensureOpenSession()) {
					requestFriendsList(session);
				}
			}
		});
		
		if(mIsLoggedIn) {
			emailText.setText(HeightChartPreference.getString(Constants.PREF_USER_ID, ""));
			if(HeightChartPreference.getInt(Constants.PREF_LOGIN_TYPE, -1) == Constants.LOGIN_TYPE_FACEBOOK) {
				Drawable facebook = res.getDrawable(R.drawable.login_via_facebook);
				final int imgSize = res.getDimensionPixelSize(R.dimen.login_info_text_drawable_size);
				facebook.setBounds(0, 0, imgSize, imgSize);
				emailText.setCompoundDrawables(facebook, null, null, null);
				emailText.setCompoundDrawablePadding(res.getDimensionPixelSize(R.dimen.login_info_text_drawable_padding));
			}
			leave.setOnClickListener(mViewOnclickListener);
			
			final long lastBackupDate = HeightChartPreference.getLong(Constants.PREF_LAST_BACKUP_DATE, -1);
			String lastDate = "";
			if(lastBackupDate > 0) {
				lastBackupText.setVisibility(View.VISIBLE);
				lastDate = getResources().getString(R.string.last_backup, DateUtils.formatDateTime(getActivity(), lastBackupDate,
						DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_SHOW_DATE |
						DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_TIME));
				lastBackupText.setTextColor(R.color.backup_import_date_text_color);
				lastBackupText.setText(lastDate);
			}
			else {
//				lastDate = getResources().getString(R.string.backup_import_first);
//				SpannableString underLineLink = new SpannableString(lastDate);
//				underLineLink.setSpan(new UnderlineSpan(), 0, underLineLink.length(), 0);
//				underLineLink.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.text_underline_link_normal)), 0, underLineLink.length(), 0);
//				lastBackupText.setText(underLineLink);
				lastBackupText.setVisibility(View.INVISIBLE);
			}
			lastBackupText.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if(lastBackupDate > 0) {
					}
					else {
						Intent faq = new Intent();
						faq.setAction(Intent.ACTION_VIEW);
						faq.setData(Uri.parse(Constants.FAQ_URL));
						getActivity().startActivity(faq);
					}
				}
			});
			
			backupBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Bundle args = new Bundle();
					args.putInt(BackupDialog.KEY_MODE, BackupDialog.MODE_BACKUP_RESTORE);
					Util.showDialogFragment(
							getFragmentManager(), 
							new BackupDialog(), 
							Constants.TAG_DIALOG_BACKUP, 
							args, 
							LoginBeforeOptionFragment.this, 
							Constants.REQUEST_DIALOG_BACKUP_RESTORE);
				}
			});
		}
		
		final boolean isBgmToggled = HeightChartPreference.getBoolean(Constants.PREF_IS_BGM_ON, true);
		
		final Switch bgmOnOff = (Switch) view.findViewById(R.id.login_before_toggle_bgm_on_off);
		bgmOnOff.setChecked(isBgmToggled);
		bgmOnOff.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					((MainActivity)getActivity()).playBgm();
				} else {
					((MainActivity)getActivity()).stopBgm();
				}
				HeightChartPreference.putBoolean(Constants.PREF_IS_BGM_ON, isChecked);
			}
		});
		
		about.setOnClickListener(mViewOnclickListener);
		login.setOnClickListener(mViewOnclickListener);
		help.setOnClickListener(mViewOnclickListener);
		requestPaper.setOnClickListener(mViewOnclickListener);
		requestX.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mDoNotShowToast = true;
				requestContainer.setVisibility(View.GONE);
			}
		});
	}
	
	private OnClickListener mViewOnclickListener = new OnClickListener() {
		
		@Override
		public void onClick(View view) {
			
			final int id = view.getId();
			String tag = null;
			Fragment fragment = null;
			Bundle args = null;
			switch (id) {
			case R.id.login_before_btn_login:
				if(mIsLoggedIn)	{
					tag = Constants.TAG_FRAGMENT_USER_DETAIL;
					args = new Bundle();
					args.putInt(UserDetailFragment.EXTRA_USER_DETAIL_MODE_NAME,
							HeightChartPreference.getInt(Constants.PREF_LOGIN_TYPE, Constants.LOGIN_TYPE_EMAIL));
					fragment = new UserDetailFragment();
				}
				else {
					tag = Constants.TAG_FRAGMENT_LOGIN;
					fragment = new LoginFragment();
				}
				break;
			case R.id.login_before_about:
				tag = Constants.TAG_FRAGMENT_ABOUT;
				fragment = new AboutFragment();
				break;
			case R.id.login_before_help:
				tag = Constants.TAG_FRAGMENT_HELP;
				fragment = new HelpFragment();
				break;
			case R.id.login_option_leave:
				tag = Constants.TAG_FRAGMENT_DELETE_ACCOUNT;
				fragment = new DeleteAccountFragment();
				break;
			case R.id.login_request_paper_text:
				tag = Constants.TAG_FRAGMENT_REQUEST_PAPER;
				fragment = new RequestPaperFragment();
				break;
			default:
				break;
			}
			
			if(tag != null && fragment != null) {
				Util.replaceFragment(getFragmentManager(), fragment, R.id.container, tag, args, true);
			}
			
		}
	};
	
	private boolean ensureOpenSession() {
		DLog.e(TAG, "ensureOpenSession:");
        if (Session.getActiveSession() == null ||
                !Session.getActiveSession().isOpened()) {
            Session.openActiveSession(
                    getActivity(), LoginBeforeOptionFragment.this, 
                    true, 
                    Arrays.asList("user_friends"),
                    mSessionCallback);
            return false;
        }
        return true;
    }
	
	private void onSessionStateChanged(Session session, SessionState state, Exception exception) {
		 if (state.isOpened() && !sessionHasNecessaryPerms(session)) {
			 //Session.openActiveSessionFromCache(getActivity());
			 NewPermissionsRequest req = new NewPermissionsRequest(
                     LoginBeforeOptionFragment.this, 
                     getMissingPermissions(session));
			 req.setCallback(mSessionCallback);
			 session.requestNewReadPermissions(req);
		 } else if(state.isOpened()){
			 requestFriendsList(session);
		 }
	}
	
	private void requestFriendsList(final Session session) {
		 Util.replaceFragment(getFragmentManager(), new InviteFacebookFragment(), 
				 R.id.container, Constants.TAG_FRAGMENT_ABOUT, null, true);
	}
	
	private boolean sessionHasNecessaryPerms(Session session) {
        if (session != null && session.getPermissions() != null) {
            for (String requestedPerm : mFriendsPermission) {
                if (!session.getPermissions().contains(requestedPerm)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
	
	private List<String> getMissingPermissions(Session session) {
        List<String> missingPerms = new ArrayList<String>(mFriendsPermission);
        if (session != null && session.getPermissions() != null) {
            for (String requestedPerm : mFriendsPermission) {
                if (session.getPermissions().contains(requestedPerm)) {
                    missingPerms.remove(requestedPerm);
                }
            }
        }
        return missingPerms;
    }
	
	private class RequestAuthSessionStatusCallback implements Session.StatusCallback {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
        	DLog.d(TAG, " - session state = " + state);
        	
        	onSessionStateChanged(session, state, exception);

        	if(state.isOpened() && sessionHasNecessaryPerms(session))
        		session.removeCallback(this);
        }
    }
	
	private void restore() {
		String path = Environment.getExternalStorageDirectory().getPath() + "/" + BACKUP_DIR;
		File backupDir = new File(path);
		String[] fileList  = backupDir.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String filename) {
				if(filename.startsWith(BACKUP_FILE_NAME))
					return true;
				else
					return false;
			}
		});
		
		if(fileList == null || fileList.length == 0)
			return;
		
		File zipFile = new File(path, fileList[0]);
		FileInputStream fis = null;
        ZipInputStream zis = null;
        ZipEntry zentry = null;
        
        File pictureDir = new File(Storage.IMAGE_DIRECTORY);
        File faceDir = getActivity().getFilesDir();
        File dbDir = getActivity().getDatabasePath(HeightChartProvider.DATABASE_NAME);
        File prefDir = new File(getActivity().getFilesDir().getParent() + "/shared_prefs");
		
        if(!pictureDir.exists())
        	pictureDir.mkdirs();
		else if(!pictureDir.exists() && pictureDir.isFile()) {
			pictureDir.delete();
			pictureDir.mkdirs();
		}

        if(!faceDir.exists())
        	faceDir.mkdirs();
		else if(!faceDir.exists() && faceDir.isFile()) {
			faceDir.delete();
			faceDir.mkdirs();
		}

        if(!dbDir.exists())
        	dbDir.mkdirs();
		else if(!dbDir.exists() && dbDir.isFile()) {
			dbDir.delete();
			dbDir.mkdirs();
		}

        if(!prefDir.exists())
        	prefDir.mkdirs();
		else if(!prefDir.exists() && prefDir.isFile()) {
			prefDir.delete();
			prefDir.mkdirs();
		}
        
        File[] faceList = faceDir.listFiles();
        for(File face : faceList) {
        	face.delete();
        }
        
        try {
        	fis = new FileInputStream(zipFile);
        	zis = new ZipInputStream(fis);
        	
        	while((zentry = zis.getNextEntry()) != null) {
        		String unzipFile = zentry.getName();
        		String ext = unzipFile.substring(unzipFile.lastIndexOf("."));
        		File destDir = null;
        		
        		if(ext.equals(Storage.IMAGE_EXTENSION)) {
        			destDir = pictureDir;
        		}
        		else if(ext.equals(Storage.IMAGE_FACE_EXTENSION)) {
        			destDir = faceDir;
        		}
        		else if(ext.equals(".db")) {
        			destDir = dbDir;
        		}
        		else if(ext.equals(".xml")) {
        			destDir = prefDir;
        		}
        		
        		File targetFile = null;
        		if(ext.equals(".db"))
        			targetFile = new File(destDir.getPath());
        		else
        			targetFile = new File(destDir.getPath(), unzipFile);
        		
        		if(targetFile.exists()) {
        			targetFile.delete();
	        		if(ext.equals(".db"))
	        			targetFile = new File(destDir.getPath());
	        		else
	        			targetFile = new File(destDir.getPath(), unzipFile);
        		}
        		
        		FileOutputStream fos = new FileOutputStream(targetFile);
        		byte[] buffer = new byte[2048];
                int len = 0;
                while ((len = zis.read(buffer)) != -1) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
        	}
        } catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(getActivity(), R.string.error_code_default, Toast.LENGTH_SHORT).show();
			return;
        }
        finally {
			try {
	        	if(zis != null)
					zis.close();
	        	if(fis != null)
	        		fis.close();
			} catch (IOException e) {
				e.printStackTrace();
				Toast.makeText(getActivity(), R.string.error_code_default, Toast.LENGTH_SHORT).show();
			}
        }
		

//		File dbFile = getActivity().getDatabasePath(HeightChartProvider.DATABASE_NAME);
//		File prefFile = new File(getActivity().getFilesDir().getParent() + "/shared_prefs/" + HeightChartPreference.SHARED_PREFERENCES_NAME + ".xml");
//		File picture = new File(Storage.IMAGE_DIRECTORY + "/" + filename + Storage.IMAGE_EXTENSION);
//		File chart = new File(getActivity().getFilesDir() + "/" + filename + Storage.POSTFIX_CHART + Storage.IMAGE_FACE_EXTENSION);
		
		//restore complete
		Bundle args = new Bundle();
		args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
		args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.restore_complete);
		args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
		
		Util.showDialogFragment(
				getFragmentManager(), 
				new CustomDialog(), 
				Constants.TAG_DIALOG_CUSTOM, 
				args, 
				LoginBeforeOptionFragment.this, 
				Constants.REQUEST_CONFIRM_RESTORE_COMPLETE);
	}
	
	private void backup() {
		Model model = ((ApplicationImpl)getActivity().getApplicationContext()).getModel();
		ArrayList<NickNameInfo> nickInfos = model.getNickList();
		long needSpace = 0;
		List<File> faceFileList = new ArrayList<File>();
		List<File> pictureList = new ArrayList<File>();
		
		for(NickNameInfo nick : nickInfos) {
			ArrayList<HeightInfo> heightInfos = nick.getHeightList();
			for(HeightInfo height : heightInfos) {
				String filename  = height.getImagePath();
				File picture = new File(Storage.IMAGE_DIRECTORY + "/" + filename + Storage.IMAGE_EXTENSION);
				if(picture.exists()) {
					needSpace += picture.length();
					pictureList.add(picture);
				}
				
				File left = new File(getActivity().getFilesDir() + "/" + filename + Storage.POSTFIX_LEFT + Storage.IMAGE_FACE_EXTENSION);
				if(left.exists()) {
					needSpace += left.length();
					faceFileList.add(left);
				}
				
				File right = new File(getActivity().getFilesDir() + "/" + filename + Storage.POSTFIX_RIGHT + Storage.IMAGE_FACE_EXTENSION);
				if(right.exists()) {
					needSpace += right.length();
					faceFileList.add(right);
				}
				
				File leftNoArrow = new File(getActivity().getFilesDir() + "/" + filename + Storage.POSTFIX_LEFT_NO_ARROW + Storage.IMAGE_FACE_EXTENSION);
				if(leftNoArrow.exists()) {
					needSpace += leftNoArrow.length();
					faceFileList.add(leftNoArrow);
				}
				
				File rightNoArrow = new File(getActivity().getFilesDir() + "/" + filename + Storage.POSTFIX_RIGHT_NO_ARROW + Storage.IMAGE_FACE_EXTENSION);
				if(rightNoArrow.exists()) {
					needSpace += rightNoArrow.length();
					faceFileList.add(rightNoArrow);
				}
				
				File chart = new File(getActivity().getFilesDir() + "/" + filename + Storage.POSTFIX_CHART + Storage.IMAGE_FACE_EXTENSION);
				if(chart.exists()) {
					needSpace += chart.length();
					faceFileList.add(chart);
				}
			}
		}

		String path = Environment.getExternalStorageDirectory().getPath() + "/" + BACKUP_DIR;
		File backupDir = new File(path);
		if(backupDir.exists() && backupDir.isFile()) {
			backupDir.delete();
			backupDir.mkdirs();
		}
		else if(!backupDir.exists()) {
			backupDir.mkdirs();
		}
		
		StatFs stat = new StatFs(path);
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getAvailableBlocks();
		long freeSpace = availableBlocks * blockSize; 
		
		if(freeSpace < needSpace) {
			Bundle args = new Bundle();
			args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
			args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.not_enough_memory);
			args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
			
			Util.showDialogFragment(
					getFragmentManager(), 
					new CustomDialog(), 
					Constants.TAG_DIALOG_CUSTOM, 
					args);
			return;
		}
		
		long lastBackupDate = Calendar.getInstance().getTimeInMillis();
		String lastDate = DateUtils.formatDateTime(getActivity(), lastBackupDate,
				DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_SHOW_DATE |
				DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_TIME);
		
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		ZipOutputStream zos = null;
		String outputFile = path + "/" + BACKUP_FILE_NAME + lastBackupDate + ".zip";
		try {
			fos = new FileOutputStream(outputFile);
			bos = new BufferedOutputStream(fos);
			zos = new ZipOutputStream(bos);
			zos.setLevel(Deflater.DEFAULT_COMPRESSION);
			
			for(File faceFile : faceFileList) {
				BufferedInputStream bis = new BufferedInputStream(new FileInputStream(faceFile));
				ZipEntry zentry = new ZipEntry(faceFile.getName());
				zentry.setTime(faceFile.lastModified());
				zos.putNextEntry(zentry);
				
				byte[] buffer = new byte[2048];
				int cnt = 0;
				while ((cnt = bis.read(buffer, 0, 2048)) != -1) {
                    zos.write(buffer, 0, cnt);
                }
                zos.closeEntry();
                bis.close();
			}

			for(File pictureFile : pictureList) {
				BufferedInputStream bis = new BufferedInputStream(new FileInputStream(pictureFile));
				ZipEntry zentry = new ZipEntry(pictureFile.getName());
				zentry.setTime(pictureFile.lastModified());
				zos.putNextEntry(zentry);
				
				byte[] buffer = new byte[2048];
				int cnt = 0;
				while ((cnt = bis.read(buffer, 0, 2048)) != -1) {
                    zos.write(buffer, 0, cnt);
                }
                zos.closeEntry();
                bis.close();
			}
			
			File dbFile = getActivity().getDatabasePath(HeightChartProvider.DATABASE_NAME);
			BufferedInputStream bis = new BufferedInputStream(new FileInputStream(dbFile));
			ZipEntry zentry = new ZipEntry(dbFile.getName());
			zentry.setTime(dbFile.lastModified());
			zos.putNextEntry(zentry);
			
			byte[] buffer = new byte[2048];
			int cnt = 0;
			while ((cnt = bis.read(buffer, 0, 2048)) != -1) {
                zos.write(buffer, 0, cnt);
            }
            zos.closeEntry();
            bis.close();

//			File prefFile = new File(getActivity().getFilesDir().getParent() + "/shared_prefs/" + HeightChartPreference.SHARED_PREFERENCES_NAME + ".xml");
//			BufferedInputStream bis2 = new BufferedInputStream(new FileInputStream(prefFile));
//			ZipEntry zentry2 = new ZipEntry(prefFile.getName());
//			zentry2.setTime(prefFile.lastModified());
//			zos.putNextEntry(zentry2);
//			
//			while ((cnt = bis2.read(buffer, 0, 2048)) != -1) {
//                zos.write(buffer, 0, cnt);
//            }
//            zos.closeEntry();
//            bis2.close();
			
			zos.finish();
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(getActivity(), R.string.error_code_default, Toast.LENGTH_SHORT).show();
			return;
		} finally {
			try {
				if(zos != null)
					zos.close();
				if(bos != null)
					bos.close();
				if(fos != null)
					fos.close();
			} catch (IOException e) {
				e.printStackTrace();
				Toast.makeText(getActivity(), R.string.error_code_default, Toast.LENGTH_SHORT).show();
				return;
			}
		}
		
		//backup complete
		Bundle args = new Bundle();
		args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
		args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.saved_file_path);
		args.putString(Constants.CUSTOM_DIALOG_TEXT_STRING, "'"+outputFile+"'");
		args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
		
		Util.showDialogFragment(
				getFragmentManager(), 
				new CustomDialog(), 
				Constants.TAG_DIALOG_CUSTOM, 
				args);
		
		TextView lastBackupText = (TextView)getView().findViewById(R.id.login_before_text_backup_last);
		lastBackupText.setText(getResources().getString(R.string.last_backup, lastDate));
		HeightChartPreference.putLong(Constants.PREF_LAST_BACKUP_DATE, lastBackupDate);
	}
	
	private String checkBackupFile() {
		String path = Environment.getExternalStorageDirectory().getPath() + "/" + BACKUP_DIR;
		File backupDir = new File(path);
		String[] fileList  = backupDir.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String filename) {
				if(filename.startsWith(BACKUP_FILE_NAME))
					return true;
				else
					return false;
			}
		});
		if(fileList != null && fileList.length > 0)
			return fileList[0].substring(fileList[0].lastIndexOf("_")+1, fileList[0].lastIndexOf("."));
		else
			return null;
	}
	
	private void deleteFile() {
		String path = Environment.getExternalStorageDirectory().getPath() + "/" + BACKUP_DIR;
		File backupDir = new File(path);
		File[] fileList  = backupDir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String filename) {
				if(filename.startsWith(BACKUP_FILE_NAME))
					return true;
				else
					return false;
			}
		});
		for(File list : fileList) {
			list.delete();
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		DLog.d(TAG, " requestCode = " + requestCode + " resultCode = " + resultCode + " data = " + data);
		switch (requestCode) {
		case Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE:
			DLog.d(TAG, " Return from facebook login activity");
            Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);
            break;
		case Constants.REQUEST_DIALOG_BACKUP_RESTORE: {
			if(resultCode != Activity.RESULT_OK)
				return;
			
			if(data == null)
				return;
			
			int select = data.getIntExtra(BackupDialog.KEY_SELECT, -1);
			String fileExist = checkBackupFile();
			
			if(select == BackupDialog.SELECT_BACKUP) {
				if(fileExist != null) {
					Bundle args = new Bundle();
					args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
					args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.saved_file_exist);
					args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, R.drawable.popup_cancel_btn);
					args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
					
					Util.showDialogFragment(
							getFragmentManager(), 
							new CustomDialog(), 
							Constants.TAG_DIALOG_CUSTOM, 
							args, 
							LoginBeforeOptionFragment.this, 
							Constants.REQUEST_CONFIRM_BACKUP);
				}
				else {
					backup();
				}
			}
			else if(select == BackupDialog.SELECT_RESTORE) {
				if(fileExist != null) {
					Bundle args = new Bundle();
					args.putInt(BackupDialog.KEY_MODE, BackupDialog.MODE_CHOOSE_RESTORE);
					args.putLong(BackupDialog.KEY_FILE, Long.valueOf(fileExist));
					Util.showDialogFragment(
							getFragmentManager(), 
							new BackupDialog(), 
							Constants.TAG_DIALOG_BACKUP, 
							args, 
							LoginBeforeOptionFragment.this, 
							Constants.REQUEST_DIALOG_CHOOSE_RESTORE);
				}
				else {
					Bundle args = new Bundle();
					args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
					args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.no_saved_file);
					args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
					
					Util.showDialogFragment(
							getFragmentManager(), 
							new CustomDialog(), 
							Constants.TAG_DIALOG_CUSTOM, 
							args);
				}
			}
			break;
		}
		case Constants.REQUEST_DIALOG_CHOOSE_RESTORE: {
			if(resultCode != Activity.RESULT_OK)
				return;
			
			Bundle args = new Bundle();
			args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
			args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.replase_saved_file);
			args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, R.drawable.popup_cancel_btn);
			args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
			
			Util.showDialogFragment(
					getFragmentManager(), 
					new CustomDialog(), 
					Constants.TAG_DIALOG_CUSTOM, 
					args, 
					LoginBeforeOptionFragment.this, 
					Constants.REQUEST_CONFIRM_RESTORE);
			break;
		}
		case Constants.REQUEST_CONFIRM_BACKUP:
			if(resultCode != Activity.RESULT_OK)
				return;
			
			deleteFile();
			backup();
			break;
		case Constants.REQUEST_CONFIRM_RESTORE:
			if(resultCode != Activity.RESULT_OK)
				return;

			restore();
			break;
		case Constants.REQUEST_CONFIRM_RESTORE_COMPLETE:
			ContentResolver resolver = getActivity().getContentResolver();
			ContentProviderClient client = resolver.acquireContentProviderClient(HeightChartProvider.AUTHORITY);
			HeightChartProvider provider = (HeightChartProvider)client.getLocalContentProvider();
			provider.resetDatabase();
			client.release();
			
			((ApplicationImpl)getActivity().getApplicationContext()).reloadDB();
			
			LocalBroadcastManager lbMan = LocalBroadcastManager.getInstance(getActivity().getApplication());
			IntentFilter filter = new IntentFilter();
			filter.addAction(Constants.ACTION_LOAD_FINISHED);
			mDbLoadFinishReceiver = new DbLoadFinishReceiver();
			lbMan.registerReceiver(mDbLoadFinishReceiver, filter);
			break;
		default:
			break;
		}
	}
	
	private DbLoadFinishReceiver mDbLoadFinishReceiver;
	private class DbLoadFinishReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();
			DLog.e(TAG, "onReceive Action : " + action);
			if(Constants.ACTION_LOAD_FINISHED.equals(action)) {
				Intent newActivity = new Intent(getActivity(), MainActivity.class);
				getActivity().startActivity(newActivity);
				getActivity().finish();
				
				if(mDbLoadFinishReceiver != null) {
					LocalBroadcastManager lbMan = LocalBroadcastManager.getInstance(getActivity().getApplication());
					lbMan.unregisterReceiver(mDbLoadFinishReceiver);
					mDbLoadFinishReceiver = null;
				}
			}
		}
	}
	
	@Override
	public void onDestroy() {
		if(mDbLoadFinishReceiver != null) {
			LocalBroadcastManager lbMan = LocalBroadcastManager.getInstance(getActivity().getApplication());
			lbMan.unregisterReceiver(mDbLoadFinishReceiver);
			mDbLoadFinishReceiver = null;
		}
		super.onDestroy();
	}
}
