package com.dole.heightchart;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dole.heightchart.server.DisconnectSNS;
import com.dole.heightchart.server.RequestNewAuthKey;
import com.dole.heightchart.server.ServerTask;
import com.dole.heightchart.server.ServerTask.IServerResponseCallback;
import com.dole.heightchart.server.WithdrawUser;
import com.facebook.Session;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeleteAccountFragment extends Fragment implements IServerResponseCallback {
	
	private static final String TAG = DeleteAccountFragment.class.getName();
	
	private WithdrawUser mWithdrawUser;
	private DisconnectSNS mDisconnectSNS;
	private boolean mIsPasswordFill;
	private ProgressDialog mProgressDialog;
	
	MixpanelAPI mMixpanel;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view =  inflater.inflate(R.layout.delete_main, null);
		
		mMixpanel = MixpanelAPI.getInstance(getActivity(), Util.MIXPANEL_TOKEN);

		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		final Button send = (Button)view.findViewById(R.id.delete_account_send);
		final CheckBox inquiry = (CheckBox)view.findViewById(R.id.delete_account_inquiry);
		ListView list = (ListView)view.findViewById(R.id.delete_account_list);
		final TextView passwordView = (TextView)view.findViewById(R.id.delete_account_pwd);
		final int loginType = HeightChartPreference.getInt(Constants.PREF_LOGIN_TYPE, Constants.LOGIN_TYPE_EMAIL);
		
		if(loginType == Constants.LOGIN_TYPE_EMAIL) {
			passwordView.setEnabled(false);
		}
		else if(loginType == Constants.LOGIN_TYPE_FACEBOOK) {
			passwordView.setVisibility(View.INVISIBLE);
		}

		send.setEnabled(false);
		send.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				sendDelete();
			}
		});
		
		inquiry.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					passwordView.setEnabled(true);
					if(loginType == Constants.LOGIN_TYPE_EMAIL && "".equals(passwordView.getText().toString()))
						send.setEnabled(false);
					else
						send.setEnabled(true);
						
				}
				else {
					send.setEnabled(false);
					passwordView.setEnabled(false);
				}
			}
		});
		
		passwordView.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				String st = s.toString();
				if(st.trim().equals(""))
					mIsPasswordFill = false;
				else
					mIsPasswordFill = true;

				if(mIsPasswordFill && inquiry.isChecked())
					send.setEnabled(true);
				else
					send.setEnabled(false);
			}
		});
		
		DeleteAccountAdapter adapter = new DeleteAccountAdapter();
		list.setAdapter(adapter);
		
		getView().setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent e) {
				Util.hideSoftInput(getActivity(), v);
				passwordView.clearFocus();
				return false;
			}
		});
	}
	
	private void sendDelete() {
		View root = getView();
		TextView passwordView = (TextView)root.findViewById(R.id.delete_account_pwd);
		int loginType = HeightChartPreference.getInt(Constants.PREF_LOGIN_TYPE, Constants.LOGIN_TYPE_EMAIL);
		String password = passwordView.getText().toString();
		
		if(loginType == Constants.LOGIN_TYPE_EMAIL && password.length() < Constants.PASSWORD_MIN_LENGTH) {
			Toast.makeText(getActivity(), getResources().getString(R.string.password_at_least_six_2), Toast.LENGTH_LONG).show();
			return;
		}
		
		HashMap<String, String> request = new HashMap<String, String>();
		if(loginType == Constants.LOGIN_TYPE_EMAIL) {
			request.put(Constants.TAG_USER_NO, String.valueOf(HeightChartPreference.getInt(Constants.PREF_USER_NO, 0)));
			request.put(Constants.TAG_USER_ID, HeightChartPreference.getString(Constants.PREF_USER_ID, ""));
			request.put(Constants.TAG_AUTHKEY, HeightChartPreference.getString(Constants.PREF_AUTH_KEY, ""));
			request.put(Constants.TAG_PASSWORD, password);
			request.put(Constants.TAG_REASON, "");
			request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
			
			mWithdrawUser = new WithdrawUser(getActivity(), request);
			mWithdrawUser.setCallback(this);
			Model.runOnWorkerThread(mWithdrawUser);
		}
		else if(loginType == Constants.LOGIN_TYPE_FACEBOOK) {
			request.put(Constants.TAG_USER_NO, String.valueOf(HeightChartPreference.getInt(Constants.PREF_USER_NO, 0)));
			request.put(Constants.TAG_AUTHKEY, HeightChartPreference.getString(Constants.PREF_AUTH_KEY, ""));
			request.put(Constants.TAG_SNS_CODE, Constants.SNS_CODE);
			request.put(Constants.TAG_SNS_ID, HeightChartPreference.getString(Constants.PREF_USER_ID, ""));
			request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
			
			mDisconnectSNS = new DisconnectSNS(getActivity(), request);
			mDisconnectSNS.setCallback(this);
			Model.runOnWorkerThread(mDisconnectSNS);
		}
		mProgressDialog = Util.openProgressDialog(getActivity());
	}

	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		if(parser instanceof RequestNewAuthKey) {
	    	if(Constants.DEBUG)
	    		Toast.makeText(getActivity(), result.get(Constants.TAG_AUTHKEY), Toast.LENGTH_LONG).show();
			HeightChartPreference.putString(Constants.PREF_AUTH_KEY, result.get(Constants.TAG_AUTHKEY));
			sendDelete();
		}
		else {
			int loginType = HeightChartPreference.getInt(Constants.PREF_LOGIN_TYPE, Constants.LOGIN_TYPE_EMAIL);
			if(loginType == Constants.LOGIN_TYPE_FACEBOOK) {
				Session session = Session.getActiveSession();
				if(session != null) {
					if (!session.isClosed()) {
			            session.closeAndClearTokenInformation();
			        }
				}
				else {
					session = new Session(getActivity());
			        Session.setActiveSession(session);
			        session.closeAndClearTokenInformation();
				}
			}
			Util.clearUserInfo();
			
			//longzhe cui added
			mMixpanel.track("delete account", null);
			mMixpanel.getPeople().set("Deleted", "true");
			
			mProgressDialog.dismiss();
			getFragmentManager().popBackStack(Constants.TAG_FRAGMENT_SPLASH, 0);
			Toast.makeText(getActivity(), getResources().getString(R.string.thanks_for_height_chart), Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onFailed(ServerTask parser, Map<String, String> result, int returnCode) {
		if((parser instanceof WithdrawUser || parser instanceof DisconnectSNS)
				&& returnCode == Constants.ERROR_CODE_AUTHKEY_EXPIRED) {
			Util.requestNewAuthKey(getActivity(), this);
		}
		else if(parser instanceof RequestNewAuthKey) {
			Toast.makeText(getActivity(), R.string.error_code_default, Toast.LENGTH_SHORT).show();
			Util.clearUserInfo();
			getActivity().finish();
		}
		else {
			Toast.makeText(getActivity(), Util.switchErrorCode(returnCode, getResources()), Toast.LENGTH_SHORT).show();
			mProgressDialog.dismiss();
		}
	}
	
	private class DeleteAccountItem {
		int resId;
		String service;
		String info;
		
		public DeleteAccountItem(int r, String s, String i) {
			resId = r;
			service = s;
			info = i;
		}
	}

	
	private class DeleteAccountAdapter extends BaseAdapter {
		List<DeleteAccountItem> mData;
		
		public DeleteAccountAdapter() {
			//TODO : define at xml
			mData = new ArrayList<DeleteAccountItem>();
			mData.add(new DeleteAccountItem(
					R.drawable.delete_account_image_dole_coin, 
					"Dole Coin", 
					getResources().getString(R.string.earn_usage_info)));
			mData.add(new DeleteAccountItem(
					R.drawable.delete_account_image_height_chart, 
					"Height chart", 
					getResources().getString(R.string.user_info) + ", " + getResources().getString(R.string.sync_info)));
		}
		
		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public int getCount() {
			return mData.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView = getActivity().getLayoutInflater().inflate(R.layout.delete_account_item, null);
			}
			View v = convertView;
			ImageView img = (ImageView)v.findViewById(R.id.delete_account_item_image);
			TextView service = (TextView)v.findViewById(R.id.delete_account_item_service);
			TextView info = (TextView)v.findViewById(R.id.delete_account_item_info);
			
			DeleteAccountItem cur = mData.get(position);
			DLog.e(TAG, "cur:"+cur+",pos:"+position);
			img.setImageResource(cur.resId);
			service.setText(cur.service);
			info.setText(cur.info);
			return v;
		}
		
	}
}
