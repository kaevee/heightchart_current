package com.dole.heightchart;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.support.v4.util.LruCache;

public class ImageCache {
	private static final int MAX_COUNT = 50;
	private LruCache<String, Bitmap> mImageCache;

	private class BitmapCache extends LruCache<String, Bitmap> {
        public BitmapCache(int maxSize) {
            super(maxSize);
        }
        
        @Override
        protected void entryRemoved(boolean evicted, String key, Bitmap oldValue, Bitmap newValue) {
            super.entryRemoved(evicted, key, oldValue, newValue);
            if (oldValue.isMutable()) {
                oldValue.recycle();
            }
        }
	}

	public ImageCache() {
		mImageCache = new BitmapCache(MAX_COUNT);
	}
	
	public Bitmap getImage(String filename) { 
		Bitmap ret = mImageCache.get(filename);
		if (ret != null)
			return ret;
		
		return setImage(filename);
	}

	public Bitmap setImage(String fileName) { 
		final BitmapFactory.Options opt = new Options();
		opt.inPreferredConfig = Config.ARGB_8888;
		
		Bitmap bmImage = BitmapFactory.decodeFile(fileName, opt);
		if(bmImage != null) {
			mImageCache.put(fileName, bmImage);
		}
		return bmImage;
	}

	public void clearCache() {
		mImageCache.evictAll();
	}
}