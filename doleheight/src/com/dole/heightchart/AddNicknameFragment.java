package com.dole.heightchart;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dole.heightchart.Constants.Avatars;
import com.dole.heightchart.Constants.Characters;
import com.dole.heightchart.ui.SoftCoverflow;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class AddNicknameFragment extends Fragment {
	private static final String TAG = AddNicknameFragment.class.getName();

	boolean mIsInitialSelection = true;
	private boolean mIsInputName = false;
	private boolean mIsInputBirth = false;
	
	private NickNameInfo mNickNameInfo = null;

	MixpanelAPI mMixpanel;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view =  inflater.inflate(R.layout.add_nickname, null);
		
		mMixpanel = MixpanelAPI.getInstance(getActivity(),Util.MIXPANEL_TOKEN);
		
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		final Button saveBtn = (Button) view.findViewById(R.id.add_nickname_btn_save);
		saveBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				save();
				
				mMixpanel.track("add child", null);
			}
		});
		
		final TextView birthDate = (TextView) view.findViewById(R.id.add_nickname_edit_birthday);
		final Calendar calendar = Calendar.getInstance();
		
		birthDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				
				Fragment prevFrag = getFragmentManager().findFragmentByTag(Constants.TAG_DIALOG_BIRTHDAY_PICKER);
				if(prevFrag != null) {
					ft.remove(prevFrag);
				}
				ft.addToBackStack(null);
				
				BirthDayPickerDialog dialogFragment = new BirthDayPickerDialog();
				final Bundle args = new Bundle();
				args.putBoolean(BirthDayPickerDialog.EXTRA_BIRTH_DATE_ONLY_YEAR, false);
				final String birthday = birthDate.getText().toString();
				try {
					Date date = new SimpleDateFormat("yyyy-MM-dd").parse(birthday);
					calendar.setTime(date);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				args.putInt(BirthDayPickerDialog.EXTRA_YEAR, calendar.get(Calendar.YEAR));
				args.putInt(BirthDayPickerDialog.EXTRA_MONTH, calendar.get(Calendar.MONTH) + 1);
				args.putInt(BirthDayPickerDialog.EXTRA_DAY, calendar.get(Calendar.DAY_OF_MONTH));
				dialogFragment.setArguments(args);
				dialogFragment.setTargetFragment(AddNicknameFragment.this, Constants.REQUEST_CODE_DATE_DIALOG);
				dialogFragment.show(ft, Constants.TAG_DIALOG_BIRTHDAY_PICKER);
			}
		});

		final SoftCoverflow softCoverFlow = (SoftCoverflow) view.findViewById(R.id.add_nickname_avatar_coverflow);
		final AvatarAdapter adapter = new AvatarAdapter();
		softCoverFlow.setAdapter(adapter);
		
		final int avatarsSize = Avatars.values().length;
		final int modBase = Integer.MAX_VALUE / 2 / avatarsSize;
        final int middlePosition = (modBase * avatarsSize); //  + avatarsSize / 2;
        
        softCoverFlow.setCallbackDuringFling(false);
        softCoverFlow.setSelection(middlePosition);
        
        final EditText editNickName = (EditText) view.findViewById(R.id.add_nickname_edit_nickname);
        final Bundle args = getArguments();
		if(args != null) {
			final long nickNameId = args.getLong(Constants.NICKNAME_ID);
			final Model model = ((ApplicationImpl)getActivity().getApplication()).getModel();
			mNickNameInfo = model.getNickNameInfoById(nickNameId);
			
			//TODO set current values
			editNickName.setText(mNickNameInfo.getNickName());
			birthDate.setText(mNickNameInfo.mBirthday);
			final int genderViewId;
			if(Constants.NICKNAME_GENDER_BOY.equals(mNickNameInfo.mGender)) {
				genderViewId = R.id.add_nickname_select_gender_boy;
			} else {
				genderViewId = R.id.add_nickname_select_gender_girl;
			}
			
			final RadioButton genderBtn = (RadioButton)view.findViewById(genderViewId);
			genderBtn.setChecked(true);
			softCoverFlow.setSelection(middlePosition + mNickNameInfo.getCharacter().getCharacterIndex());
			saveBtn.setEnabled(true);
			mIsInputName = true;
			mIsInputBirth = true;
		}
		else
			saveBtn.setEnabled(false);
        
        final Activity activity = getActivity();
        
        softCoverFlow.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(view != null) {
					if(softCoverFlow.isTouchedDown()) {
						return;
					}
					
					final View titleView = view.findViewById(R.id.add_nickname_avatar_name);
					final View spotLight = view.findViewById(R.id.add_nickname_avatar_spot_light);
					if(titleView != null)
						titleView.setVisibility(View.VISIBLE);

					if(spotLight != null)
						spotLight.setVisibility(View.VISIBLE);
					view.invalidate();
					
					if(mIsInitialSelection) {
						mIsInitialSelection = false;
						return;
					}
					
					AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(activity,
							R.anim.popup_and_down);
					set.setTarget(view);
					set.start();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});
        
        softCoverFlow.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
        	    final View view = softCoverFlow.getSelectedView();
            	final View name = view.findViewById(R.id.add_nickname_avatar_name);
            	if(name != null) {
            		name.setVisibility(View.VISIBLE);
            	}
             }
        });
        
        editNickName.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				String str = s.toString();
				if(str.trim().equals(""))
					mIsInputName = false;
				else
					mIsInputName = true;
				
				if(mIsInputName && mIsInputBirth)
					saveBtn.setEnabled(true);
				else
					saveBtn.setEnabled(false);
			}
		});
        
        birthDate.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				String str = s.toString();
				if(str.trim().equals(""))
					mIsInputBirth = false;
				else
					mIsInputBirth = true;
				
				if(mIsInputName && mIsInputBirth)
					saveBtn.setEnabled(true);
				else
					saveBtn.setEnabled(false);
			}
		});
		
		getView().setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent e) {
				Util.hideSoftInput(getActivity(), v);
				editNickName.clearFocus();
				return false;
			}
		});
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(resultCode != Activity.RESULT_OK) 
			return;
		
		switch (requestCode) {
		case Constants.REQUEST_CODE_DATE_DIALOG:
			TextView birthText = (TextView) getView().findViewById(R.id.add_nickname_edit_birthday);
			final int year = data.getIntExtra(BirthDayPickerDialog.EXTRA_YEAR, 1970);
			final int month = data.getIntExtra(BirthDayPickerDialog.EXTRA_MONTH, 1);
			final int day = data.getIntExtra(BirthDayPickerDialog.EXTRA_DAY, 1);
			
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, year);
			calendar.set(Calendar.MONTH, month - 1);
			calendar.set(Calendar.DAY_OF_MONTH, day);
			final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			birthText.setText(format.format(calendar.getTime()));
			break;
			
		default:
			break;
		}
	}	
	
	private void save() {
		final Activity activity = getActivity();
		EditText nameView = (EditText)getView().findViewById(R.id.add_nickname_edit_nickname);
		String name = nameView.getText().toString();
		Model model = ((ApplicationImpl)getActivity().getApplication()).getModel();
		ArrayList<NickNameInfo> nickList = model.getNickList();
		DLog.d(TAG, "name:"+name);
		for(NickNameInfo nick : nickList) {
			DLog.e(TAG, "nick:"+nick.mNickName);
			if(nick.mNickName.equals(name) && (mNickNameInfo == null || !mNickNameInfo.mNickName.equals(name))) {
				Toast.makeText(getActivity(), getResources().getString(R.string.nickname_already_registered), Toast.LENGTH_SHORT).show();
				return;
			}
		}
		Model.runOnWorkerThread(new Runnable() {
			@Override
			public void run() {
				EditText nameView = (EditText)getView().findViewById(R.id.add_nickname_edit_nickname);
				TextView birthView = (TextView)getView().findViewById(R.id.add_nickname_edit_birthday);
				RadioGroup genderView = (RadioGroup)getView().findViewById(R.id.add_nickname_select_gender);
				SoftCoverflow softCoverFlow = (SoftCoverflow)getView().findViewById(R.id.add_nickname_avatar_coverflow);
				int gender = genderView.getCheckedRadioButtonId();
				String name = nameView.getText().toString();
				String birth = birthView.getText().toString();
				if(name.trim().equals("") || birth.trim().equals("") || gender < 0) {
					return;
				}
				AvatarAdapter adapter = (AvatarAdapter)softCoverFlow.getAdapter();
				Avatars avatars = (Avatars)adapter.getItem(softCoverFlow.getSelectedItemPosition());
				String genderText = gender == R.id.add_nickname_select_gender_boy ? Constants.NICKNAME_GENDER_BOY : Constants.NICKNAME_GENDER_GIRL;
				Characters character = Characters.getCharacterAt(avatars.getIndex());
				
				if(mNickNameInfo == null) {
					NickNameInfo info = NickNameInfo.insert(activity, name, character, birth, genderText);
					if(info == null) {
						//TODO : insert fail
						return;
					}

					final Model model = ((ApplicationImpl) activity.getApplication()).getModel();
					model.addNickName(info);
				} else {
					int update = NickNameInfo.update(activity, name, character, birth, genderText, mNickNameInfo.mNickNameId);
					
					if(update > 0) {
						mNickNameInfo.mBirthday = birth;
						mNickNameInfo.mGender = genderText;
						mNickNameInfo.mNickName = name;
						mNickNameInfo.mCharacter = character;
						final Fragment target = getTargetFragment();
						if(target != null) {
							target.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
						}
					}
					else {
						//TODO : update fail
						return;
					}
				}
				getFragmentManager().popBackStack();
			}
		});
	}
	
	class AvatarAdapter extends BaseAdapter {
		
		private static final int sCount = Integer.MAX_VALUE;
		private Avatars[] mAvatars;
		private Resources mResources;
		
		public AvatarAdapter() {
			super();
			mAvatars = Avatars.values();
			mResources = getActivity().getResources();
		}

		@Override
		public int getCount() {
			return sCount;
		}

		@Override
		public Object getItem(int position) {
            if(mAvatars == null)
                return null;

            return mAvatars[position % mAvatars.length];
		}

		@Override
		public long getItemId(int position) {
            if(mAvatars == null)
                return -1;
            return position % mAvatars.length;

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				final LayoutInflater inflater =  LayoutInflater.from(getActivity());
				convertView = inflater.inflate(R.layout.add_nickname_avatar_item, null);
			}
			
			Object tag = getItem(position);
			convertView.setTag(tag);
			
			final Avatars avatar = (Avatars) tag;
			final ImageView avatarIcon = (ImageView) convertView.findViewById(R.id.add_nickname_avatar_img);
			final TextView avatarName = (TextView) convertView.findViewById(R.id.add_nickname_avatar_name);
			
			avatarIcon.setImageDrawable(mResources.getDrawable(avatar.getImageId()));
			avatarName.setText(mResources.getString(avatar.getNameId()));
			
			return convertView;
		}
	}
}
