package com.dole.heightchart;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dole.heightchart.GuideDialog.GuideMode;
import com.dole.heightchart.server.GetMobileProfile;
import com.dole.heightchart.server.RequestAuthKey;
import com.dole.heightchart.server.RequestSNSLogin;
import com.dole.heightchart.server.ServerTask;
import com.dole.heightchart.server.ServerTask.IServerResponseCallback;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginFragment extends Fragment implements IServerResponseCallback {
	
	private static final String TAG = "LoginFragment";
	
	private GetMobileProfile mGetMobileProfile;
	private RequestAuthKey mRequestAuthKey;
	private String mTempUserId;
	private String mTempUserNo;
	private String mTempAuthKey;
	private String mTempCountry;
	private String mTempCity;
	private String mTempLanguage;
	private int mTempGender;
	private String mTempBirth;
	private String mTempName;
	private boolean mIsFirst;
	private boolean mIsEmailFill = false;
	private boolean mIsPasswordFill = false;
	
	private boolean mIsFacebookLogin = false;
	
	private String mForgottenPassEmail = null;
	private ProgressDialog mProgressDialog;
	
	private List<String> facebookPermissions = Arrays.asList(
			"email",
			"user_birthday",
			"user_location"
			);
	
	private Session.StatusCallback mFacebookLoginStatusCallback = new LoginSessionStatusCallback();
	
	MixpanelAPI mMixpanel;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		mMixpanel = MixpanelAPI.getInstance(getActivity(),Util.MIXPANEL_TOKEN);
		
		return inflater.inflate(R.layout.login, null);
	}
	
	@Override
	public void onViewCreated(View view, final Bundle savedInstanceState) {

		super.onViewCreated(view, savedInstanceState);
		
		int userNo = HeightChartPreference.getInt(Constants.PREF_USER_NO, -1);
		if(userNo > 0)
			getFragmentManager().popBackStack();
		
		final View linkLoginForgot = view.findViewById(R.id.login_link_forgot_password);
		final View linkSignUp = view.findViewById(R.id.login_link_sign_up);
		final View linkFacebookLogin = view.findViewById(R.id.login_link_login_facebook);
		final View loginSkip = view.findViewById(R.id.login_skip);
		final View loginBtn = view.findViewById(R.id.login_send_btn);
		final View backBtn = view.findViewById(R.id.common_btn_cancel);
		final EditText emailView = (EditText) getView().findViewById(R.id.login_edit_email);
		final TextView pwdView = (TextView) getView().findViewById(R.id.login_edit_password);
		
		linkLoginForgot.setOnClickListener(mViewOnclickListener);
		linkSignUp.setOnClickListener(mViewOnclickListener);
		linkFacebookLogin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Session session = Session.getActiveSession();
				if(session != null && !session.isOpened()) {
					session.closeAndClearTokenInformation();
					Session.setActiveSession(null);
					getView().post(new Runnable() {
						@Override
						public void run() {
							signInWithFacebook(savedInstanceState);							
						}
					});
					return;
				}
				signInWithFacebook(savedInstanceState);
			}
		});
		
		mIsFirst = HeightChartPreference.getBoolean(Constants.PREF_IS_FIRST_APP_LAUNCH, true);
		if(mIsFirst) {
			// No more login after first launch
			HeightChartPreference.putBoolean(Constants.PREF_IS_FIRST_APP_LAUNCH, false);
			loginSkip.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					((MainActivity)getActivity()).onBackPressed();
				}
			});
			backBtn.setVisibility(View.GONE);
		}
		else {
			loginSkip.setVisibility(View.GONE);
		}
		
		loginBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
				requestLogin();
			}
		});
		
		boolean isGuideShown = HeightChartPreference.getBoolean(GuideMode.GUIDE_LOGIN.name(), false);
		final FragmentManager fragmentManager = getFragmentManager();
		if(!isGuideShown) {
			getView().getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
				@Override
				public boolean onPreDraw() {
					
					final int IDX_CNT = 3; 
					final int[] posX = new int[IDX_CNT];
					final int[] posY = new int[IDX_CNT];
					final int[] loc = new int[2];
					linkSignUp.getLocationOnScreen(loc);
					posX[0] = loc[0];
					posY[0] = loc[1];
					
					posX[2] = -1;
					posY[2] = loc[1] - linkSignUp.getHeight() / 2;

					linkFacebookLogin.getLocationOnScreen(loc);
					posX[1] = loc[0];
					posY[1] = loc[1];
					
					final Bundle guideArgs = new Bundle();
					guideArgs.putString(GuideDialog.MODE_GUIDE, GuideMode.GUIDE_LOGIN.name());
					guideArgs.putInt(GuideDialog.ITEM_COUNT, IDX_CNT);
					guideArgs.putIntArray(GuideDialog.ITEM_IDS, new int[]{R.id.guide_text4, R.id.guide_text5, R.id.guide_image_frame}); 
					guideArgs.putIntArray(GuideDialog.ITEM_LEFTS, posX);
					guideArgs.putIntArray(GuideDialog.ITEM_TOPS, posY);

					Util.showDialogFragment(fragmentManager, new GuideDialog(), Constants.TAG_DIALOG_GUIDE, guideArgs);
					
					getView().getViewTreeObserver().removeOnPreDrawListener(this);
					HeightChartPreference.putBoolean(GuideMode.GUIDE_LOGIN.name(), true);
					return true;
				}
			});
		}
		
		loginBtn.setEnabled(false);
		emailView.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				String st = s.toString();
				if(st.trim().equals(""))
					mIsEmailFill = false;
				else
					mIsEmailFill = true;
				
				if(mIsEmailFill && mIsPasswordFill)
					loginBtn.setEnabled(true);
				else
					loginBtn.setEnabled(false);
			}
		});
		
		pwdView.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				String st = s.toString();
				if(st.trim().equals(""))
					mIsPasswordFill = false;
				else
					mIsPasswordFill = true;
				
				if(mIsEmailFill && mIsPasswordFill)
					loginBtn.setEnabled(true);
				else
					loginBtn.setEnabled(false);
			}
		});
		
		getView().setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent e) {
				Util.hideSoftInput(getActivity(), v);
				emailView.clearFocus();
				pwdView.clearFocus();
				return false;
			}
		});
	}
	
	@Override
	public void onResume() {
		super.onResume();
		final EditText emailView = (EditText) getView().findViewById(R.id.login_edit_email);
		if(mForgottenPassEmail != null && !"".equals(mForgottenPassEmail)) {
			emailView.setText(mForgottenPassEmail);		
		}
	}
	
	private OnClickListener mViewOnclickListener = new OnClickListener() {
		
		@Override
		public void onClick(View view) {
			
			final int id = view.getId();
			String tag = null;
			Fragment fragment = null;
			Fragment target = null;
			Bundle args = null;
			int requestCode = 0;
			switch (id) {
			case R.id.login_link_forgot_password:
				tag = Constants.TAG_FRAGMENT_FORGOT_PASSWORD;
				fragment = new LoginForgotPasswordFragment();
				target = LoginFragment.this;
				requestCode = Constants.REQUEST_FORGOTTEN_PASS_EMAIL;
				break;
			case R.id.login_link_sign_up:
				tag = Constants.TAG_FRAGMENT_USER_DETAIL;
				args = new Bundle();
				args.putInt(UserDetailFragment.EXTRA_USER_DETAIL_MODE_NAME, UserDetailFragment.SIGN_UP_EMAIL);
				fragment = new UserDetailFragment();
				
				//longzhe cui added
				mMixpanel.track("Landing", null);
				
				break;
			default:
				break;
			}
			
			if(tag == null || fragment == null)
				return;
			
			Util.replaceFragment(getFragmentManager(), fragment, R.id.container, tag, args, target, requestCode, true);
			
		}
	};
	
	private void requestLogin() {
		final TextView emailView = (TextView) getView().findViewById(R.id.login_edit_email);
		final TextView passView = (TextView) getView().findViewById(R.id.login_edit_password);
		
		String emailAdd = emailView.getText().toString();
		String password = passView.getText().toString();
		if(!Util.checkEmail(emailAdd)) {
			Toast.makeText(getActivity(), getResources().getString(R.string.enter_full_email), Toast.LENGTH_SHORT).show();
			return;
		}
		if(password.length() < Constants.PASSWORD_MIN_LENGTH) {
			Toast.makeText(getActivity(), getResources().getString(R.string.password_at_least_six_2), Toast.LENGTH_SHORT).show();
			return;
		}
		
		mTempUserId = emailView.getText().toString();
		
		final HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_ID, mTempUserId);
		request.put(Constants.TAG_PASSWORD, passView.getText().toString());
		request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
		request.put(Constants.TAG_MOBILE_OS, Constants.OS_STRING);

		String registrationId = HeightChartPreference.getString(Constants.PROPERTY_REG_ID, "");

		request.put(Constants.TAG_DEVICE_TOKEN, registrationId);
		request.put(Constants.TAG_UUID, Util.getUUID(getActivity()));
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
		
		mRequestAuthKey = new RequestAuthKey(getActivity(), request);
		mRequestAuthKey.setCallback(this);
		Model.runOnWorkerThread(mRequestAuthKey);

		mProgressDialog = Util.openProgressDialog(getActivity());
	}
	
	private void onLoginSessionStateChange(Session session, SessionState state, Exception exception) {
	    if (state.isOpened()) {
	        DLog.d(TAG, "Logged in...");
	        Request.newMeRequest(session, new Request.GraphUserCallback() {
	        	@Override
	        	public void onCompleted(GraphUser user, Response response) {
	        		DLog.d(TAG, " user = " + user); 
	        		
 	        		if (user != null) {
	        			final Locale current = Locale.getDefault(); //getResources().getConfiguration().locale;
	        			
	        			mTempUserId = (String) user.getProperty("email");
 	        			mTempCountry = Util.getCountryIsoForFacebook(getActivity(), user);
 	        			mTempLanguage = current.getLanguage();
	        			String gender = user.asMap().get("gender").toString(); 
 	        			mTempGender = Constants.GenderType.MALE.name().equalsIgnoreCase(gender)?
    							Constants.GenderType.MALE.type:
    								Constants.GenderType.FEMALE.type;
 	        			mTempBirth = user.getBirthday();
 	        			mTempName = user.getName();
 	        			mTempCity = Util.getCityForFacebook(user);
	        			
	        			DLog.d(TAG, 
	        					"after onLoginSessionStateChange\n name = " + user.getName() +
	        					"\n -- email = " + mTempUserId +
	        					"\n -- gender = " + gender + 
	        					"\n -- birth = " + mTempBirth +
	        					"\n -- gender = " + Constants.GenderType.MALE.name().toLowerCase() + " | real gender = " + gender +
	        					"\n -- city = " + mTempCity +
	        					"\n -- country = " + mTempCountry
	        					);
	        			requestSNSLogin();
	        		}
	        	}
	        }).executeAsync();
	    } else if (state.isClosed()) {
	    	DLog.d(TAG, "onLoginSessionStateChange out...");
	    } else {
	    	DLog.d(TAG, "somethin else??? stat = " + state.toString());
	    }
	}
	
	private int getAge(String dateOfBirth) {
		Calendar cal1 = new GregorianCalendar();
		Calendar cal2 = new GregorianCalendar();
		int age = 0;
		int factor = 0; 
		Date birth = null;
		try {
			birth = new SimpleDateFormat("MM/dd/yyyy").parse(dateOfBirth);
		} catch (NullPointerException e) {
			birth = new Date();
		} catch (java.text.ParseException e) {
			DLog.e(TAG, "ParseException ", e);
		}
		Date date2 = new Date();
		cal1.setTime(birth);
		cal2.setTime(date2);
		if(cal2.get(Calendar.DAY_OF_YEAR) < cal1.get(Calendar.DAY_OF_YEAR)) {
			factor = -1; 
		}
		age = cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR) + factor;
		DLog.d(TAG, "Your age is: "+age);

		return age;
	}
	
	private void signInWithFacebook(Bundle savedInstanceState) {
		Session session = Session.getActiveSession();
		if (session == null) {
			if (savedInstanceState != null) {
				session = Session.restoreSession(getActivity(), null, mFacebookLoginStatusCallback, savedInstanceState);
			}
			if (session == null) {
				session = new Session(getActivity());
			}
			Session.setActiveSession(session);
		}

	    if (!session.isOpened() && !session.isClosed()) {
	        Session.OpenRequest openRequest = null;
	        openRequest = new Session.OpenRequest(LoginFragment.this);

	        if (openRequest != null) {
	            //openRequest.setDefaultAudience(SessionDefaultAudience.FRIENDS);
	            openRequest.setPermissions(facebookPermissions);
	            openRequest.setLoginBehavior(SessionLoginBehavior.SSO_WITH_FALLBACK);
	            openRequest.setCallback(mFacebookLoginStatusCallback);
	            session.openForRead(openRequest);
	        }
	    }else {
	    	onLoginSessionStateChange(session, session.getState(), null);
	    }
	}

//    private void onClickFacebookLogout() {
//        Session session = Session.getActiveSession();
//        if (!session.isClosed()) {
//            session.closeAndClearTokenInformation();
//        }
//    }

    private class LoginSessionStatusCallback implements Session.StatusCallback {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
        	DLog.d(TAG, " login callback");
        	onLoginSessionStateChange(session, state, exception);
        }
    }
	
	private void requestSNSLogin() {
		mIsFacebookLogin = true;
		final HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_UUID, Util.getUUID(getActivity()));
		request.put(Constants.TAG_SNS_TYPE, Constants.SNS_CODE);
		request.put(Constants.TAG_SNSID, mTempUserId);
		request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);

		String registrationId = HeightChartPreference.getString(Constants.PROPERTY_REG_ID, "");

		request.put(Constants.TAG_DEVICE_TOKEN, registrationId);
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
		
		RequestSNSLogin snsLogin = new RequestSNSLogin(getActivity(), request);
		snsLogin.setCallback(this);
		Model.runOnWorkerThread(snsLogin);
		mProgressDialog = Util.openProgressDialog(getActivity());
	}
	
	private void requestTerms() {
    	Bundle args = new Bundle();
    	args.putBoolean(AgreePersonalInfoFragment.FROM_FACEBOOK_LOGIN, true);
    	args.putString(Constants.TAG_USER_ID, mTempUserId);
    	args.putString(Constants.TAG_PASSWORD, "");
    	args.putString(Constants.TAG_EMAIL, mTempUserId);
    	args.putString(Constants.TAG_STANDARD_COUNTRY_CODE, mTempCountry);
    	args.putString(Constants.TAG_UUID, Util.getUUID(getActivity()));
    	args.putString(Constants.TAG_ADDRESS_MAIN, mTempCity);
    	args.putString(Constants.TAG_BIRTHDAY, mTempBirth);
    	args.putString(Constants.TAG_GENDER, String.valueOf(mTempGender));
    	args.putString(Constants.TAG_LANGUAGE, mTempLanguage);
    	args.putString(Constants.TAG_SNS_CODE, Constants.SNS_CODE);
    	args.putString(Constants.TAG_SNS_ID, mTempUserId);
    	args.putString(Constants.TAG_SNS_USER_NAME, mTempName);
    	args.putString(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
    	Util.replaceFragment(getFragmentManager(), new AgreePersonalInfoFragment(), R.id.container,
    			Constants.TAG_FRAGMENT_AGREE_PERSONAL_INFO, args, true);
	}
	
	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		if(parser instanceof RequestAuthKey || parser instanceof RequestSNSLogin) {
			DLog.e(TAG, "RequestAuthKey");
			mTempUserNo = result.get(Constants.TAG_USER_NO);
			mTempAuthKey = result.get(Constants.TAG_AUTHKEY);
			
			final HashMap<String, String> request = new HashMap<String, String>();
			request.put(Constants.TAG_USER_NO, mTempUserNo);
			request.put(Constants.TAG_AUTHKEY, mTempAuthKey);
			request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
			
			mGetMobileProfile = new GetMobileProfile(getActivity(), request);
			mGetMobileProfile.setCallback(this);
			Model.runOnWorkerThread(mGetMobileProfile);
			
		}
		else if(parser instanceof GetMobileProfile) {
			HeightChartPreference.putString(Constants.PREF_USER_ID, mTempUserId);
			HeightChartPreference.putInt(Constants.PREF_USER_NO, Integer.valueOf(mTempUserNo));
			HeightChartPreference.putString(Constants.PREF_AUTH_KEY, mTempAuthKey);
			HeightChartPreference.putBoolean(Constants.PREF_EMAIL_VALIFICATION, Boolean.valueOf(result.get(Constants.TAG_EMAIL_VALIFICATION)));
			HeightChartPreference.putString(Constants.PREF_EMAIL_ACTIVATE_DATE_TIME, result.get(Constants.TAG_EMAIL_ACTIVATE_DATE_TIME));
			HeightChartPreference.putString(Constants.PREF_CHANGE_PASSWROD_DATE_TIME, result.get(Constants.TAG_CHANGE_PASSWROD_DATE_TIME));
			HeightChartPreference.putString(Constants.PREF_REGISTER_DATE_TIME, result.get(Constants.TAG_REGISTER_DATE_TIME));
			HeightChartPreference.putString(Constants.PREF_COUNTRY, result.get(Constants.TAG_COUNTRY));
			HeightChartPreference.putString(Constants.PREF_LANGUAGE, result.get(Constants.TAG_LANGUAGE));
			HeightChartPreference.putInt(Constants.PREF_GENDER, Integer.valueOf(result.get(Constants.TAG_GENDER)));
			HeightChartPreference.putString(Constants.PREF_BIRTHDAY, result.get(Constants.TAG_BIRTHDAY));
			HeightChartPreference.putString(Constants.PREF_ADDRESS_MAIN, result.get(Constants.TAG_ADDRESS_MAIN));
			HeightChartPreference.putInt(Constants.PREF_LOGIN_TYPE, mIsFacebookLogin?Constants.LOGIN_TYPE_FACEBOOK:Constants.LOGIN_TYPE_EMAIL);
			
			getFragmentManager().popBackStackImmediate();
			mProgressDialog.dismiss();
			
			JSONObject obj = new JSONObject();
			
			try {
				obj.put("Authentication", mIsFacebookLogin?"Facebook":"Email");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			mMixpanel.track("Direct Login", obj);
			mMixpanel.identify(mTempUserId);
			mMixpanel.getPeople().identify(mTempUserId);
			mMixpanel.getPeople().set("$email", mTempUserId);
			mMixpanel.getPeople().set("Deleted", "false");
			mMixpanel.getPeople().initPushHandling(Util.GCM_SENDERID);
		}
	}
	
	@Override
	public void onFailed(ServerTask parser, Map<String, String> result, int returnCode) {
		DLog.d(TAG, " onFailed parser = " + parser + " | ret = " + returnCode);
		if(parser instanceof RequestSNSLogin && returnCode == Constants.ERROR_CODE_UNREGIST_SNS) {
			mProgressDialog.dismiss();
			requestTerms();
		} else {
			Toast.makeText(getActivity(), Util.switchErrorCode(returnCode, getResources()), Toast.LENGTH_SHORT).show();
			mProgressDialog.dismiss();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		DLog.e(TAG, " requestCode = " + requestCode + " resultCode = " + resultCode + " data = " + data);
		switch (requestCode) {
		case Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE:
			DLog.d(TAG, " Return from facebook login activity");
            Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);
            break;
            
		case Constants.REQUEST_FORGOTTEN_PASS_EMAIL:
			mForgottenPassEmail = data.getStringExtra(Constants.EMAIL);
			break;
		default:
			break;
		}
	}
}
