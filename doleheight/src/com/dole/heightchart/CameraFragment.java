package com.dole.heightchart;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dole.heightchart.GuideDialog.GuideMode;
import com.dole.heightchart.camera.CameraManager;
import com.dole.heightchart.camera.CameraManager.CameraMode;
import com.dole.heightchart.camera.Exif;
import com.dole.heightchart.camera.Storage;
import com.dole.heightchart.server.DisconnectSNS;
import com.dole.heightchart.server.RequestNewAuthKey;
import com.dole.heightchart.server.RequestUseQRCode;
import com.dole.heightchart.server.ServerTask;
import com.dole.heightchart.server.WithdrawUser;
import com.dole.heightchart.server.ServerTask.IServerResponseCallback;
import com.dole.heightchart.ui.CustomDialog;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

public class CameraFragment extends Fragment implements IServerResponseCallback {
	
	private static final String TAG = "CameraFragment";
	
	enum ViewMode {
		TAKE_PICTURE,
		CONFIRM_PICTURE
	}
	
	enum HeightSelection {
		
		JUNGLE(0, R.drawable.frame_01, R.id.record_height_selection_jungle, true),
		BANANA_FARM(1, R.drawable.frame_02, R.id.record_height_selection_banana_farm, true),
		FRUIT(2, R.drawable.frame_03, R.id.record_height_selection_fruit, true),
		PARK(3, R.drawable.frame_04_lock, R.id.record_height_selection_park, false),
		SEAFLOOR(4, R.drawable.frame_05_lock, R.id.record_height_selection_seafloor, false),
		SEA(5, R.drawable.frame_06_lock, R.id.record_height_selection_sea, false),
		CITY_OF_ROBOT(6, R.drawable.frame_07_lock, R.id.record_height_selection_city_of_robot, false),
		SPACE(7, R.drawable.frame_08_lock, R.id.record_height_selection_space, false);
		
		private int index;
		private int frameImageId;
		private int bottomViewId;
		private boolean enabled;
		private HeightSelection(int index, int frameResId, int bottomViewId, boolean isEnabled) {
			this.index = index;
			this.frameImageId = frameResId;
			this.bottomViewId = bottomViewId;
			this.enabled = isEnabled;
		}
	}

	private NickNameInfo mNickInfo;
	private float mHeight;
	private CameraManager mCameraManager;
	private ViewMode mMode;
	private Bitmap mTakenPicture;
	private boolean mIsLoggedIn;
	private ViewPager mViewPager;
	private boolean mIsUsedCodeAndWithin24Hour = false;
	
	private String mQrStringValueCp;
	private String mQrPromotionCode;
	private long mDeleteHeightIdForCodeReuse = Constants.UNAVAILABLE_ID;
	
	private ProgressDialog mProgressDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.camera_record_height, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		mIsLoggedIn = (0 != HeightChartPreference.getInt(Constants.PREF_USER_NO, 0)) ? true: false;

		Bundle args = getArguments();
		if(args != null) {
			long nickId = args.getLong(Constants.NICKNAME_ID);
			mNickInfo = Util.getNickNameInfoById(getActivity(), nickId);
			mHeight = args.getFloat(Constants.HEIGHT);
			
			String mode = args.getString(Constants.MODE);
			if(mMode == null) {
				mMode = ViewMode.valueOf(mode);
			}

			mQrStringValueCp = args.getString(Constants.QRCODE);
			mQrPromotionCode = args.getString(Constants.PROMOTION_TYPE);
			mIsUsedCodeAndWithin24Hour = args.getBoolean(Constants.USED_CODE_WITHIN_24H, false);
			
			mDeleteHeightIdForCodeReuse = args.getLong(Constants.REUSE_HEIGHT_ID, Constants.UNAVAILABLE_ID);
		}

		mViewPager = (ViewPager) view.findViewById(R.id.record_height_pager_bg);
		final HeightFrameSelectionListener listener = new HeightFrameSelectionListener();

		mViewPager.setAdapter(new HeightFrameAdapter());		
		mViewPager.setOnPageChangeListener(listener);
		mViewPager.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
			@Override
			public boolean onPreDraw() {
				listener.onPageSelected(mViewPager.getCurrentItem());
				mViewPager.getViewTreeObserver().removeOnPreDrawListener(this);
				return true;
			}
		});
		
		initView(view);
	}
	
	private void initView(final View view) {

		final ImageView albumBtn = (ImageView) view.findViewById(R.id.record_height_btn_album);
		final View retakeBtn = view.findViewById(R.id.record_height_btn_retake);
		final TextView btnText = (TextView) view.findViewById(R.id.record_height_shutter_btn_text);
		final ImageView shutterBtn = (ImageView) view.findViewById(R.id.record_height_shutter_btn);
		final ImageView confirmImage = (ImageView) view.findViewById(R.id.record_height_confirm_image);
		final ImageView guideFrame = (ImageView) view.findViewById(R.id.record_height_standing_guide);
		final ImageView focusArea = (ImageView) view.findViewById(R.id.record_height_center_focus);
		final View cancelBtn = view.findViewById(R.id.camera_btn_cancel);
		
		for(HeightSelection selection : HeightSelection.values()) {
			final View bottomView = view.findViewById(selection.bottomViewId);
			bottomView.setOnClickListener(mBottomItemClickListener);
			bottomView.setTag(selection);
		}
		
		cancelBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getFragmentManager().popBackStack(Constants.TAG_FRAGMENT_DETAIL, 0);
			}
		});
		
		switch (mMode) {
		case TAKE_PICTURE:
			final View focusView = view.findViewById(R.id.record_height_center_focus);
			if(mCameraManager == null) {
				mCameraManager = new CameraManager(getActivity(), getView(), CameraMode.RECORD_HEIGHT, focusView);
				mCameraManager.setCallback(null, new RecordHeightJpegPictureCallback());
			} else {
				mCameraManager.initCamera(getView(), focusView);
			}
			
			albumBtn.setOnClickListener(mAlBumButtonClickListener);

			albumBtn.setVisibility(View.VISIBLE);
			retakeBtn.setVisibility(View.GONE);
			btnText.setVisibility(View.GONE);
			confirmImage.setVisibility(View.GONE);
			shutterBtn.setImageResource(R.drawable.camera_shutter);
			guideFrame.setVisibility(View.VISIBLE);
			focusArea.setVisibility(View.VISIBLE);
			
			// Guide dialog
			final GuideMode mode = GuideMode.GUIDE_CAMERA;
			boolean isGuideShown = HeightChartPreference.getBoolean(mode.name(), false);
			if(!isGuideShown) {
				final View v = getView();
				v.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {

					@Override
					public boolean onPreDraw() {
						final Bundle args = new Bundle();
						args.putString(GuideDialog.MODE_GUIDE, mode.name());
						Util.showDialogFragment(getFragmentManager(), new GuideDialog(), Constants.TAG_DIALOG_GUIDE, args);
						v.getViewTreeObserver().removeOnPreDrawListener(this);
						return true;
					}
				});
			}
			mCameraManager.setOnTakeButtonClickListener();
			break;
			
		case CONFIRM_PICTURE:
			
			albumBtn.setVisibility(View.GONE);
			guideFrame.setVisibility(View.GONE);
			focusArea.setVisibility(View.GONE);
			
			confirmImage.setVisibility(View.VISIBLE);
			confirmImage.setImageBitmap(mTakenPicture);
			
			shutterBtn.setImageResource(R.drawable.camera_btn_save);
			retakeBtn.setVisibility(View.VISIBLE);
			btnText.setVisibility(View.VISIBLE);
			btnText.setText(R.string.save_camera);
			btnText.setOnClickListener(mConfirmBtnClickListener);
			
			retakeBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mMode = ViewMode.TAKE_PICTURE;
					initView(view);
					updateLayout(mViewPager.getCurrentItem());
				}
			});
			break;

		default:
			break;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if(mMode == ViewMode.TAKE_PICTURE && mCameraManager != null)
			mCameraManager.onResume();
		if(mMode == ViewMode.CONFIRM_PICTURE) {
			DLog.e(TAG, "onResume mTakenPicture:"+mTakenPicture);
			final ImageView confirmImage = (ImageView)getView().findViewById(R.id.record_height_confirm_image);
			confirmImage.setImageBitmap(mTakenPicture);
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if(mCameraManager != null)
			mCameraManager.onPause();
	}
	
	private OnClickListener mAlBumButtonClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			final Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
			photoPickerIntent.setType("image/*");
			startActivityForResult(photoPickerIntent, Constants.REQUEST_PICK_ALBUM_PHOTO);   
		}
	};
	
	private void updateLayout(int position) {
		final HeightSelection[] selections = HeightSelection.values();
		final ImageView shutterBtn = (ImageView) getView().findViewById(R.id.record_height_shutter_btn);
		final ImageView loginBtn = (ImageView) getView().findViewById(R.id.record_height_login_btn);
		final TextView shutterText = (TextView) getView().findViewById(R.id.record_height_shutter_btn_text);
		switch (mMode) {
		case TAKE_PICTURE:
			if(!mIsLoggedIn && !selections[position].enabled) {
				loginBtn.setVisibility(View.VISIBLE);
				shutterBtn.setVisibility(View.INVISIBLE);
				shutterText.setVisibility(View.VISIBLE);
				shutterText.setText(R.string.login);
				shutterText.setOnClickListener(mLoginBtnClickListener);
				Toast.makeText(getActivity(), getResources().getString(R.string.need_log_in), Toast.LENGTH_SHORT).show();
			} else {
				loginBtn.setVisibility(View.INVISIBLE);
				shutterBtn.setVisibility(View.VISIBLE);
				shutterText.setVisibility(View.INVISIBLE);
				shutterText.setText("");
				shutterText.setOnClickListener(null);
			}
			break;
		case CONFIRM_PICTURE:
			if(!mIsLoggedIn && !selections[position].enabled) {
				loginBtn.setVisibility(View.VISIBLE);
				shutterBtn.setVisibility(View.INVISIBLE);
				shutterText.setText(R.string.login);
				Toast.makeText(getActivity(), getResources().getString(R.string.need_log_in), Toast.LENGTH_SHORT).show();
				shutterText.setOnClickListener(mLoginBtnClickListener);
			}
			else {
				loginBtn.setVisibility(View.INVISIBLE);
				shutterBtn.setVisibility(View.VISIBLE);
				shutterText.setText(R.string.save_camera);
				shutterText.setOnClickListener(mConfirmBtnClickListener);
			}
			break;
		}
	}
	
	private class HeightFrameSelectionListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int position) {
			
		}

		@Override
		public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels){
			
		}

		@Override
		public void onPageSelected(int position) {
			final HeightSelection[] selections = HeightSelection.values();
			for(int i = 0 ; i < selections.length; i++) {
				HeightSelection selected = selections[i];
				View bottomView = getView().findViewById(selected.bottomViewId);
				if(bottomView != null) {
					if(i == position) {
						bottomView.setSelected(true);
					} else {
						bottomView.setSelected(false);
					}
				}
				if(!mIsLoggedIn)
					bottomView.setEnabled(selected.enabled);
			}

			updateLayout(position);
		}
	}
	
	class HeightFrameAdapter extends PagerAdapter {
		
		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			final LayoutInflater inflater = LayoutInflater.from(getActivity());
			View page = inflater.inflate(R.layout.camera_record_height_bg_frame, null);
			
			final ImageView frameBg = (ImageView) page;
			HeightSelection selection = HeightSelection.values()[position];
			page.setTag(selection);
			frameBg.setBackgroundResource(selection.frameImageId);
			
			container.addView(page);
			return page;
		}
		
		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return HeightSelection.values().length;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}
	}
	
	private void scaleViewRectToCenterInside(Size pictureSize, Rect viewRect) {
		final int imageWidth = pictureSize.width;
		final int imageHeight = pictureSize.height;
		
		final int viewWidth;
        final int viewHeight;
		
		if(pictureSize.width > pictureSize.height) { 
			viewWidth = viewRect.height();
			viewHeight = viewRect.width();
		} else {
			viewWidth = viewRect.width();
			viewHeight = viewRect.height();
		}
		
		DLog.d(TAG, "  imageWidth = " + imageWidth + " imageHeight = " + imageHeight + " viewWidth = " + viewWidth + "  viewHeight = " + viewHeight);
		
		float scale;
        float dx = 0, dy = 0;
        int left;
        int top;
        int right;
        int bottom;
        if (imageWidth * viewHeight > viewWidth * imageHeight) {
        	scale = (float) viewHeight / (float) imageHeight;
        	left = (int)((imageWidth - viewWidth / scale) * 0.5f);
        	top = 0;
        	right = imageWidth - left;
        	bottom = imageHeight;
        } else {
        	scale = (float) viewWidth / (float) imageWidth;
        	left = 0;
        	top = (int)((imageHeight - viewHeight / scale) * 0.5f);
        	right = imageWidth;
        	bottom = imageHeight - top;
        }
        viewRect.set(left, top, right, bottom);
        
        DLog.d(TAG, "  scale = " + scale + " dx = " + dx + " dy = " + dy + "  viewRect2 = " + viewRect);
        
    }

	private final class RecordHeightJpegPictureCallback implements PictureCallback {
		
		@Override
		public void onPictureTaken(final byte[] data, final Camera camera) {
			final View rootView = getView();
			final Activity activity = getActivity();
								
			Model.runOnWorkerThread(new Runnable() {

				@Override
				public void run() {
					Parameters parameters = camera.getParameters();
					Size pictureSize = parameters.getPictureSize();
					final View previewView = rootView.findViewById(R.id.camera_preview);
					Rect clipRect = new Rect(0, 0, previewView.getWidth(), previewView.getHeight());
					scaleViewRectToCenterInside(pictureSize, clipRect);
					
					Bitmap clipBitmap = null;
					BitmapRegionDecoder decoder;
					try {
						decoder = BitmapRegionDecoder.newInstance(data, 0, data.length, true);
						BitmapFactory.Options options = new BitmapFactory.Options();
						options.inDither = false;
			            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
						clipBitmap = decoder.decodeRegion(clipRect, options);
						decoder.recycle();
					} catch (IOException e) {
						DLog.e(TAG, "Bitmap crop failed", e);
					}
					DLog.d(TAG, " clipBitmap w = " + clipBitmap.getWidth() + " h = " + clipBitmap.getHeight());
					//TODO ScaleDown image
					final DisplayMetrics dm = new DisplayMetrics();
					activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
					final int screenWidth = dm.widthPixels;
					final int screenHeight = dm.heightPixels;
					DLog.d(TAG, " w = " + screenWidth + " h = " + screenHeight);
					final Bitmap resizedBitmap = Bitmap.createScaledBitmap(clipBitmap, screenHeight, screenWidth, false);
					clipBitmap.recycle();
										
					android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
		            android.hardware.Camera.getCameraInfo(mCameraManager.getCameraId(), info);
		            mTakenPicture = rotate(resizedBitmap, 90); //Exif.getOrientation(data));
		            clipBitmap.recycle();
		            DLog.d(TAG, " mTakenPicture w = " + mTakenPicture.getWidth() + " h = " + mTakenPicture.getHeight() + " orien = " + Exif.getOrientation(data));
					mMode = ViewMode.CONFIRM_PICTURE;
					mCameraManager.releaseCamera();
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							initView(getView());
						}
					});
				}
			});
		}
	}
	
	public static Bitmap rotate(Bitmap bitmap, int degree) {
	    int w = bitmap.getWidth();
	    int h = bitmap.getHeight();

	    Matrix mtx = new Matrix();
	    mtx.postRotate(degree);
	    
	    Bitmap rotatedImg = Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
	    bitmap.recycle();

	    return rotatedImg;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode != Activity.RESULT_OK) 
			return;
		
		switch (requestCode) {
		case Constants.REQUEST_DOLE_COIN:
			saveUserImage();
			break;
		case Constants.REQUEST_PICK_ALBUM_PHOTO:
			Uri selectedImage = data.getData();
			Bundle args = new Bundle();
			args.putString(Constants.URI, selectedImage.toString());
			Util.replaceFragment(getFragmentManager(), new ImportPhotoFragment(), 
					R.id.container, Constants.TAG_FRAGMENT_IMPORT_PHOTO, args, this, Constants.REQUEST_IMPORT_IMAGE, true);

			break;
		case Constants.REQUEST_IMPORT_IMAGE:
			boolean isImported = data.getBooleanExtra(Constants.IS_IMPORTED, false);
			if(isImported) {
				final Model model = ((ApplicationImpl)getActivity().getApplication()).getModel();
				mTakenPicture = model.getImportedImage();
				mMode = ViewMode.CONFIRM_PICTURE;
			}
			break;
		default:
			break;
		}
	}
	
	private void saveUserImage() {
		DLog.e(TAG, "mConfirmBtnClickListener click");
		final Activity activity = getActivity();
		final Model model = ((ApplicationImpl)activity.getApplication()).getModel();
		final HeightInfo heightInfo = new HeightInfo();
		final int selectionIdx = mViewPager.getCurrentItem();
		final HeightSelection selection = HeightSelection.values()[selectionIdx];
		heightInfo.mHeight = mHeight;
		heightInfo.setQrCode(mQrStringValueCp);
		heightInfo.setCouponType(mQrPromotionCode);

		Model.runOnWorkerThread(new Runnable() {
			@Override
			public void run() {
				final HeightInfo deleteHeightInfo = model.getHeightInfoById(mDeleteHeightIdForCodeReuse);
				if(deleteHeightInfo != null) {
					final NickNameInfo nickInfoForDelete = model.getNickNameInfoById(deleteHeightInfo.getNickId());
					// Delete used data
					if(nickInfoForDelete != null) {
						Model.deleteHeights(getActivity(), nickInfoForDelete, 
								Arrays.asList(new HeightInfo[]{deleteHeightInfo}));
					}
				}
				
				Uri uri = Storage.addNewHeight(
						getActivity(),
						mNickInfo, 
						heightInfo,
						mTakenPicture,
						selection.frameImageId);

				mTakenPicture.recycle();
				mTakenPicture = null;

				// Parse height id and assign to HeightInfo
				if(uri != null) {
					final long heightId = ContentUris.parseId(uri);
					heightInfo.setHeightId(heightId);

					// Add to managed List
					mNickInfo.addHeight(heightInfo);
				}
				
				Display display = activity.getWindowManager().getDefaultDisplay();
				Point size = new Point();
				display.getSize(size);
				final int maxDecodePixelSize = size.x * size.y;
				
				try {
					mTakenPicture = Util.decodeFile(heightInfo.getImageFullPath(), maxDecodePixelSize);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}

				final FragmentManager manager = getFragmentManager();
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						manager.popBackStack(Constants.TAG_FRAGMENT_DETAIL, 0);
						mCameraManager.releaseCamera();
						mCameraManager = null;
						
						Bundle args = new Bundle();
						args.putLong(Constants.HEIGHT_ID, heightInfo.mHeightId);
						args.putBoolean(Constants.IS_VIEWER_CONFIRM, true);
						Util.replaceFragment(manager, new HeightViewerFragment(), 
								R.id.container, Constants.TAG_FRAGMENT_HEIGHT_VIEWER, args, true);
					}
				});
			}
		});
	}
	
	private OnClickListener mBottomItemClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			final Object tag = v.getTag();
			if(tag instanceof HeightSelection) {
				final HeightSelection selection = (HeightSelection)tag;
				mViewPager.setCurrentItem(selection.index, true);
			}
		}
	};
	
	private OnClickListener mLoginBtnClickListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			DLog.e(TAG, "login click");
			Util.replaceFragment(
					getFragmentManager(), 
					new LoginFragment(), 
					R.id.container, 
					Constants.TAG_FRAGMENT_LOGIN, 
					null, 
					true);
		}
	};
	
	private OnClickListener mConfirmBtnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if(mQrStringValueCp == null) {
				HeightChartPreference.putBoolean(Constants.PREF_SAMPLE_QRCODE_USED, true);
				saveUserImage();
			} else {
				if(mIsUsedCodeAndWithin24Hour) {
					saveUserImage();
				} else {
					Util.requestUseQrCode(getActivity(), CameraFragment.this, mQrStringValueCp);
					mProgressDialog = Util.openProgressDialog(getActivity());
				}
			}
		}
	};

	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		if(parser instanceof RequestNewAuthKey) {
	    	if(Constants.DEBUG)
	    		Toast.makeText(getActivity(), result.get(Constants.TAG_AUTHKEY), Toast.LENGTH_LONG).show();
			HeightChartPreference.putString(Constants.PREF_AUTH_KEY, result.get(Constants.TAG_AUTHKEY));
			Util.requestUseQrCode(getActivity(), CameraFragment.this, mQrStringValueCp);
		}
		else if(parser instanceof RequestUseQRCode) {
			int userNo = HeightChartPreference.getInt(Constants.PREF_USER_NO, 0);
			
			if(userNo > 0) {
				Bundle args = new Bundle();
				args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_coin);
				args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.congratulation_for_coin);
				args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES_PARAM1, 500);
				args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);

				Util.showDialogFragment(
						getFragmentManager(), 
						new CustomDialog(), 
						Constants.TAG_DIALOG_CUSTOM, 
						args, 
						CameraFragment.this, 
						Constants.REQUEST_DOLE_COIN);
			} else {
				saveUserImage();
			}
			
			if(mProgressDialog != null)
				mProgressDialog.dismiss();
		}
	}

	@Override
	public void onFailed(ServerTask parser, Map<String, String> result, int returnCode) {
		if(parser instanceof RequestUseQRCode && returnCode == Constants.ERROR_CODE_AUTHKEY_EXPIRED) {
			Util.requestNewAuthKey(getActivity(), this);
		}
		else if(parser instanceof RequestNewAuthKey) {
			Toast.makeText(getActivity(), R.string.error_code_default, Toast.LENGTH_SHORT).show();
			Util.clearUserInfo();
			getActivity().finish();
		}
		else {
			//TODO Show toast and Retry save
			if(mProgressDialog != null)
				mProgressDialog.dismiss();
			Toast.makeText(getActivity(), Util.switchErrorCode(returnCode, getResources()), Toast.LENGTH_SHORT).show();
		}
	}
}
