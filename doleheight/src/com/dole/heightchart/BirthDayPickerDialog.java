package com.dole.heightchart;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.Calendar;

public class BirthDayPickerDialog extends DialogFragment {
	
	public static final String EXTRA_DAY = "day";
	public static final String EXTRA_MONTH = "month";
	public static final String EXTRA_YEAR = "year";

	public static final String EXTRA_BIRTH_DATE_ONLY_YEAR = "extra_birth_date";
	
	private boolean mIsUseYearOnly = false;
	
	private static final int UNAVAILBLE_VALUE = -1;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		final Dialog d = getDialog();
		d.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		int divierId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = getDialog().findViewById(divierId);
		divider.setBackgroundDrawable(null); // earlier defined color-int
		
		return inflater.inflate(R.layout.birthdate_dialog, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		DatePicker datePicker = (DatePicker) view.findViewById(R.id.dialog_date_picker);
		Calendar calendar = Calendar.getInstance();
		
		Bundle args = getArguments();
		int year = UNAVAILBLE_VALUE;
		int month = UNAVAILBLE_VALUE;
		int day = UNAVAILBLE_VALUE;
		if(args != null) {
			mIsUseYearOnly = args.getBoolean(EXTRA_BIRTH_DATE_ONLY_YEAR);
			
			year = args.getInt(EXTRA_YEAR, UNAVAILBLE_VALUE);
			month = args.getInt(EXTRA_MONTH, UNAVAILBLE_VALUE);
			day = args.getInt(EXTRA_DAY, UNAVAILBLE_VALUE);
		}

		if(year == UNAVAILBLE_VALUE)
			year = calendar.get(Calendar.YEAR);
		
		if(month == UNAVAILBLE_VALUE)
			month = calendar.get(Calendar.MONTH) + 1;
		
		if(day == UNAVAILBLE_VALUE)
			day = calendar.get(Calendar.DAY_OF_MONTH);
		
		datePicker.init(year, month - 1, day, null);
		datePicker.setMaxDate(Calendar.getInstance().getTimeInMillis());
		
		if(mIsUseYearOnly) {
			TextView tv = (TextView) view.findViewById(R.id.dialog_title);
			tv.setText(R.string.enter_year_of_birth);
			hideDayAndMonth();
		}
		
		//TODO Needs null check
		view.findViewById(R.id.dialog_button_ok).setOnClickListener(mBottomBtnClickListener);
		view.findViewById(R.id.dialog_button_cancel).setOnClickListener(mBottomBtnClickListener);
	}
	
	private OnClickListener mBottomBtnClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			dismiss();
			switch (v.getId()) {
			case R.id.dialog_button_ok:
				if(getTargetFragment() != null && getView() != null) {
					DatePicker datePicker = (DatePicker) getView().findViewById(R.id.dialog_date_picker);
					datePicker.clearFocus();
					Intent i = new Intent();
					Bundle extras=new Bundle();
					if(!mIsUseYearOnly) {
						extras.putInt(EXTRA_DAY, datePicker.getDayOfMonth());
						extras.putInt(EXTRA_MONTH, datePicker.getMonth()+1);
					}
					extras.putInt(EXTRA_YEAR, datePicker.getYear());
					i.putExtras(extras);
					getTargetFragment().onActivityResult(getTargetRequestCode(),Activity.RESULT_OK,i);
				}
				break;
			case R.id.dialog_button_cancel:
				break;
			default:
				break;
			}
		}
	};
	
	@Override
	public void show(FragmentManager manager, String tag) {
		if(mIsUseYearOnly)
			hideDayAndMonth();
		super.show(manager, tag);
	}

	@Override
	public int show(FragmentTransaction transaction, String tag) {
		if(mIsUseYearOnly)
			hideDayAndMonth();
		return super.show(transaction, tag);
	}
	
	private void hideDayAndMonth() {
		int dayId = getResources().getIdentifier("android:id/day", null, null);
		int monthId = getResources().getIdentifier("android:id/month", null, null);
		View dayPicker = getView().findViewById(dayId);
		dayPicker.setVisibility(View.GONE);
		
		View monthPicker = getView().findViewById(monthId);
		monthPicker.setVisibility(View.GONE);
	}
}
