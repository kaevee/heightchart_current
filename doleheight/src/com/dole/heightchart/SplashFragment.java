package com.dole.heightchart;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentManager.BackStackEntry;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dole.heightchart.Constants.Avatars;
import com.dole.heightchart.Constants.Characters;
import com.dole.heightchart.GuideDialog.GuideMode;
import com.dole.heightchart.server.GetAppVersion;
import com.dole.heightchart.server.GetBalance;
import com.dole.heightchart.server.GetEventNoticeList;
import com.dole.heightchart.server.RequestChargeCoin;
import com.dole.heightchart.server.RequestNewAuthKey;
import com.dole.heightchart.server.ServerTask;
import com.dole.heightchart.server.ServerTask.IServerResponseCallback;
import com.dole.heightchart.ui.BgTransitionHelper;
import com.dole.heightchart.ui.BgTransitionHelper.OnTransitionListener;
import com.dole.heightchart.ui.CharacterContainer;
import com.dole.heightchart.ui.CharacterContainer.CharacterLoadListener;
import com.dole.heightchart.ui.CloudBg;
import com.dole.heightchart.ui.CustomDialog;
import com.dole.heightchart.ui.SplashBottom;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class SplashFragment extends Fragment implements IServerResponseCallback, CharacterLoadListener, OnTransitionListener {
	
	private static final String TAG = "SplashFragment";
	
	private boolean mIsIndelete = false;
	private ArrayList<NickNameInfo> mDeleteList = new ArrayList<NickNameInfo>();
	private BroadcastReceiver mAddNewNickReceiver;
	private boolean mIsMainAnimationFinished = false;
	private boolean mIsCharacterLoadFinished = false;
	private String mEventUrl = null;
	
	private BgTransitionHelper mDrawableAniHelper;
	
	private ProgressDialog mProgressDialog;
	
	MixpanelAPI mMixpanel;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		mMixpanel = MixpanelAPI.getInstance(getActivity(), Util.MIXPANEL_TOKEN);
		
		return inflater.inflate(R.layout.splash_main, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		final Activity activity = getActivity();
		if(Util.isInternetAvailable(activity) && savedInstanceState == null) {
    		final HashMap<String, String> request = new HashMap<String, String>();
    		request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
    		request.put(Constants.TAG_MOBILE_OS, Constants.OS_STRING);
    		request.put(Constants.TAG_CLIENT_IP, Util.getIp(activity));
    		
    		final GetAppVersion appVersionTask = new GetAppVersion(activity, request);
    		appVersionTask.setCallback(this);
    		Model.runOnWorkerThread(appVersionTask);
    		mProgressDialog = Util.openProgressDialog(activity);
    	} else {
    		initSplashFragment(view, savedInstanceState);
    	}
	}
	
	private void initSplashFragment(View view, Bundle savedInstanceState) {
		final SplashBottom splashBottom = (SplashBottom) getView().findViewById(R.id.splash_bottom);
		final ImageView title = (ImageView)view.findViewById(R.id.splash_title);
		final AnimationDrawable titleAnim = (AnimationDrawable)title.getDrawable();
		
		if(Util.isInternetAvailable(getActivity()) && savedInstanceState == null) {
			String countryCode = Util.getCountryIsoFromTelephonyManager(getActivity());
			String contentGroupClassNo = Util.getContentGroupClassNo(countryCode);
			Map<String, String> request = new HashMap<String, String>();
	    	request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
	    	request.put(Constants.TAG_CONTENT_GROUP_NO, "4");
	    	request.put(Constants.TAG_LANGUAGE_NO, "1");
	    	request.put(Constants.TAG_CONTENT_GROUP_CLASS_NO, contentGroupClassNo);
	    	request.put(Constants.TAG_NOTICE_STATUS, "20");
	    	request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
	    	
			ServerTask task = new GetEventNoticeList(getActivity(), request);
	    	task.setCallback(this);
	    	Model.runOnWorkerThread(task);
		}
    	
		// Start titleAnimation
		titleAnim.start();

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				splashBottom.startSplash(true);
				titleAnim.stop();
				TranslateAnimation upAni = new TranslateAnimation(0,0,0,-title.getBottom()*2);
				upAni.setStartOffset(1000);
				upAni.setDuration(1000);
				upAni.setRepeatCount(0);
				upAni.setAnimationListener(mListener);
				title.startAnimation(upAni);
			}
		}, 500);
		
		LocalBroadcastManager lbMan = LocalBroadcastManager.getInstance(getActivity().getApplication());
		IntentFilter filter = new IntentFilter();
		filter.addAction(Constants.ACTION_LOAD_FINISHED);
		filter.addAction(Constants.ACTION_DELETE_NICK_FINISHED);
		mAddNewNickReceiver = new AddNewNickReceiver();
		lbMan.registerReceiver(mAddNewNickReceiver, filter);
		
		Model model = ((ApplicationImpl)getActivity().getApplication()).getModel();
		if(model.isDbLoadFinished())
			loadNicknames();
		
		view.findViewById(R.id.btn_bottom_delete).setEnabled(false);
		int userNo = HeightChartPreference.getInt(Constants.PREF_USER_NO, 0);
		if(userNo > 0 && Util.isInternetAvailable(getActivity()) && savedInstanceState == null)
			Util.requestNewAuthKey(getActivity(), this);
		else
			setCoinInfo();
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		DLog.e(TAG, "splash onHiddenChanged :" + hidden);
		super.onHiddenChanged(hidden);
		if(hidden == false) {
			loadNicknames();
			setCoinInfo();
			boolean isGuideShown = HeightChartPreference.getBoolean(GuideMode.GUIDE_SPLASH.name(), false);
			
			DLog.e(TAG, "isGuideShown:"+isGuideShown);
			if(popupEvent());
			else if(!isGuideShown) {
				showGuide();
			}
		}
	}
	
	private void showGuide() {
		String country = Locale.getDefault().getCountry();
		final GuideMode mode = (GuideDialog.COUNTRY_NEW_ZEALAND.equals(country))?
				GuideMode.GUIDE_SPLASH_PAPER:
					GuideMode.GUIDE_SPLASH;
		final CharacterContainer container = (CharacterContainer) getView().findViewById(R.id.character_container);
		if(container.getChildCount() > 1)
			return;
		
		final ImageView addButton = (ImageView)getView().findViewById(R.id.character_add_button);
		final int[] pos = new int[2];
		addButton.getLocationInWindow(pos);

		final Bundle args = new Bundle();
		args.putString(GuideDialog.MODE_GUIDE, mode.name());
		args.putInt(GuideDialog.ITEM_COUNT, 1);
		args.putIntArray(GuideDialog.ITEM_IDS, new int[]{R.id.guide_image});
		args.putIntArray(GuideDialog.ITEM_LEFTS, new int[]{pos[0]});
		args.putIntArray(GuideDialog.ITEM_TOPS, new int[]{pos[1]});

		Util.showDialogFragment(getFragmentManager(), new GuideDialog(), Constants.TAG_DIALOG_GUIDE, args);

		HeightChartPreference.putBoolean(GuideMode.GUIDE_SPLASH.name(), true);
	}
	
	private void showNewNickGuide() {
		final CharacterContainer container = (CharacterContainer) getView().findViewById(R.id.character_container);
		if(container.getChildCount() != 2)
			return;
		
		final View firstCharacter = container.getChildAt(0);
		final ImageView animalView = (ImageView) firstCharacter.findViewById(R.id.character_animal);
		final int[] pos = new int[2];
		animalView.getLocationInWindow(pos);
		
		final Bundle args = new Bundle();
		args.putString(GuideDialog.MODE_GUIDE, GuideMode.GUIDE_SPLASH_NEW_NICK.name());
		args.putInt(GuideDialog.ITEM_COUNT, 1);
		args.putIntArray(GuideDialog.ITEM_IDS, new int[]{R.id.guide_image});
		args.putIntArray(GuideDialog.ITEM_LEFTS, new int[]{pos[0] + 20});
		args.putIntArray(GuideDialog.ITEM_TOPS, new int[]{pos[1] + animalView.getHeight() / 2 - 100});

		Util.showDialogFragment(getFragmentManager(), new GuideDialog(), Constants.TAG_DIALOG_GUIDE, args);

		HeightChartPreference.putBoolean(GuideMode.GUIDE_SPLASH_NEW_NICK.name(), true);
		
	}
	
	private boolean popupEvent() {
		int count = getFragmentManager().getBackStackEntryCount();
		if(count > 0) {
			BackStackEntry entry = getFragmentManager().getBackStackEntryAt(count - 1);
			if(!Constants.TAG_FRAGMENT_SPLASH.equals(entry.getName())) {
				return false;
			}
		}

		if(mEventUrl != null) {
			DLog.e(TAG, "mEventUrl:"+mEventUrl);
			
			long time = HeightChartPreference.getLong(Constants.PREF_NOT_SHOW_EVENT, 0);
			
			Date date = new Date();
			Calendar cur = Calendar.getInstance();
			cur.setTimeInMillis(date.getTime());
			
			Calendar notShow = Calendar.getInstance();
			notShow.setTimeInMillis(time);
			
			DLog.e(TAG, "cur:"+cur.toString());
			DLog.e(TAG, "notShow:"+notShow.toString());
			
			notShow.set(Calendar.DATE, notShow.get(Calendar.DATE)+1);
	
			DLog.e(TAG, "cur:"+cur.toString());
			DLog.e(TAG, "notShow:"+notShow.toString());
			
			if(notShow.after(cur))
				return false;
		
			final Bundle args = new Bundle();
			args.putString(Constants.EVENT_URL, mEventUrl);
			
			Util.replaceFragment(getFragmentManager(), new EventFragment(), 
					R.id.container, Constants.TAG_FRAGMENT_EVENT, args, true);
			mEventUrl = null;
			return true;
		}
		return false;
	}
	
	private void toggleDeleteState(boolean isInDelete) {
		final View view = getView();
		if(getView() == null)
			return;
		
		final View btnGetCoin = view.findViewById(R.id.btn_get_coin);
		final View btnAddNickname = view.findViewById(R.id.btn_bottom_add_height);
		final View btnDelete = view.findViewById(R.id.btn_bottom_delete);
		final View optionMenuBtn = view.findViewById(R.id.btn_bottom_option_menu);
		final View btnDeleteBack = view.findViewById(R.id.btn_bottom_return);
		final View btnConfirmDelete = view.findViewById(R.id.btn_bottom_confirm_delete);
		HorizontalScrollView scroll = (HorizontalScrollView)view.findViewById(R.id.main_character_container_frame);
		CharacterContainer container = (CharacterContainer)scroll.getChildAt(0);
		
		if(isInDelete) {
			AnimUtil.getFadeOutAnimation(btnGetCoin);
			AnimUtil.getFadeOutAnimation(btnAddNickname);
			AnimUtil.getFadeOutAnimation(btnDelete);
			AnimUtil.getFadeOutAnimation(optionMenuBtn);
			
			AnimUtil.getFadeInAnimation(btnDeleteBack);
			AnimUtil.getFadeInAnimation(btnConfirmDelete);
			container.setDeleteState(true);
		} else {
			AnimUtil.getFadeInAnimation(btnGetCoin);
			AnimUtil.getFadeInAnimation(btnAddNickname);
			AnimUtil.getFadeInAnimation(btnDelete);
			AnimUtil.getFadeInAnimation(optionMenuBtn);
			
			AnimUtil.getFadeOutAnimation(btnDeleteBack);
			AnimUtil.getFadeOutAnimation(btnConfirmDelete);
			container.setDeleteState(false);
		}
		btnConfirmDelete.setEnabled(false);
		
		AnimUtil.startAnimators(300);
	}
	
	
	private AnimationListener mListener = new AnimationListener() {
		@Override
		public void onAnimationStart(Animation animation) {
		}
		
		@Override
		public void onAnimationRepeat(Animation animation) {
		}
		
		@Override
		public void onAnimationEnd(Animation animation) {
			ImageView title = (ImageView)getActivity().findViewById(R.id.splash_title);
			ImageView copyright = (ImageView)getActivity().findViewById(R.id.splash_copyright);
			copyright.setVisibility(View.GONE);
			title.setImageDrawable(getResources().getDrawable(R.drawable.title_height_chart));
			FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)title.getLayoutParams();
			params.gravity = Gravity.CENTER_HORIZONTAL;
			int margin = getResources().getDimensionPixelSize(R.dimen.main_title_top_margin);
			params.setMargins(0, margin, 0, 0);
			title.setLayoutParams(params);
			
			TranslateAnimation anim = new TranslateAnimation(0,0,-title.getBottom()*2,0);
			anim.setDuration(1000);
			anim.setRepeatCount(0);
			title.startAnimation(anim);
			
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					mIsMainAnimationFinished = true;
					DLog.e(TAG, "mIsMainAnimationFinished");
					startCharacterAnimation(null);
				}
			}, 1000);
		}
	};
	
	public void onDestroy() {
		LocalBroadcastManager lbMan = LocalBroadcastManager.getInstance(getActivity().getApplication());
		lbMan.unregisterReceiver(mAddNewNickReceiver);
		mAddNewNickReceiver = null;
		super.onDestroy();
	};
	
	private class AddNewNickReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();
			DLog.e(TAG, "onReceive Action : " + action);
			if(Constants.ACTION_LOAD_FINISHED.equals(action)) {
				loadNicknames();
			} else if(Constants.ACTION_DELETE_NICK_FINISHED.equals(action)) {
				loadNicknames();
			}
		}
	}
	
	private void loadNicknames() {
		DLog.e(TAG, "loadNicknames getView:"+getView());
		if(getView() == null)
			return;
		final HorizontalScrollView scroll = (HorizontalScrollView)getView().findViewById(R.id.main_character_container_frame);
		final CharacterContainer container = (CharacterContainer) scroll.findViewById(R.id.character_container);
		
		container.setCharacterLoadListener(this);
		container.initNicks(mNickAnimalClickListener, mNickAnimalLongClickListener);
	}
	
	private void resetCharacterDrawable() {
		HorizontalScrollView scroll = (HorizontalScrollView)getView().findViewById(R.id.main_character_container_frame);
		CharacterContainer container = (CharacterContainer)scroll.getChildAt(0);
		int i, len = container.getChildCount();
		
		for(i = 0; i < len; i++) {
			View characterView = container.getChildAt(i);
			NickNameInfo tag = (NickNameInfo)characterView.getTag();
			ImageView imgView = (ImageView)characterView.findViewById(R.id.character_animal);
			int drawableId = CharacterContainer.getDrawableId(tag.mCharacter, mIsIndelete);
			if(drawableId < 0) {
				characterView.setVisibility(View.GONE);
			}
			else {
				characterView.setVisibility(View.VISIBLE);
				characterView.findViewById(R.id.character_text).setEnabled(true);
				Drawable draw = getResources().getDrawable(drawableId);
				if(!mIsIndelete)
					imgView.setSelected(false);
				imgView.setImageDrawable(draw);
				if(draw instanceof AnimationDrawable)
					((AnimationDrawable)draw).start();
			}
		}
	}
	
	private View.OnClickListener mNickAnimalClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(getView() == null)
				return;
			
			View btnConfirmDelete = getView().findViewById(R.id.btn_bottom_confirm_delete);
			Object tag = ((View)v.getParent()).getTag();
			if(mIsIndelete) {
				if(v.isSelected()) {
					v.setSelected(false);
					mDeleteList.remove(tag);
					((ViewGroup)v.getParent()).findViewById(R.id.character_text).setEnabled(true);
				}
				else {
					v.setSelected(true);
					mDeleteList.add((NickNameInfo)tag);
					((ViewGroup)v.getParent()).findViewById(R.id.character_text).setEnabled(false);
				}
				if(mDeleteList.size() > 0)
					btnConfirmDelete.setEnabled(true);
				else
					btnConfirmDelete.setEnabled(false);
			}
			else {
				if(tag instanceof NickNameInfo && ((NickNameInfo)tag).getNickNameId() != NickNameInfo.ADDNICK_OWL_ID) {
					final NickNameInfo nickInfo = (NickNameInfo) tag;
					
					final Bundle args = new Bundle();
					args.putLong(Constants.NICKNAME_ID, nickInfo.getNickNameId());
					
					Util.replaceFragment(getFragmentManager(), new DetailFragment(), 
							R.id.container, Constants.TAG_FRAGMENT_DETAIL, args, true);
				}
				else {
					Util.replaceFragment(getFragmentManager(), new AddNicknameFragment(), 
							R.id.container, Constants.TAG_FRAGMENT_ADD_NICK, null, true);
				}
			}
		}
	};
	
	private View.OnLongClickListener mNickAnimalLongClickListener = new View.OnLongClickListener() {
		@Override
		public boolean onLongClick(View v) {
			Object tag = ((View)v.getParent()).getTag();
			if(!mIsIndelete && tag instanceof NickNameInfo && ((NickNameInfo)tag).getNickNameId() != NickNameInfo.ADDNICK_OWL_ID) {
				View nightBg = getView().findViewById(R.id.splash_night_bg);
				View dayBg = getView().findViewById(R.id.splash_day_bg);
				if(mDrawableAniHelper == null)
					mDrawableAniHelper = new BgTransitionHelper(dayBg, nightBg, SplashFragment.this);
				mDrawableAniHelper.start();
				
				v.performClick();
			}
			return true;
		}
	};
	
	private void setCoinInfo() {
		if(getView() == null)
			return;
		
		View num = getView().findViewById(R.id.splash_dole_coin_num);
		View before = getView().findViewById(R.id.splash_dole_coin_before);
		final int userNo = HeightChartPreference.getInt(Constants.PREF_USER_NO, 0);
		final View coinBtn = getView().findViewById(R.id.btn_get_coin);
		
		coinBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(userNo > 0)
					Util.replaceFragment(getFragmentManager(), new DoleCoinFragment(), R.id.container, Constants.TAG_FRAGMENT_DOLE_COIN, null, true);
				else {
					Bundle args = new Bundle();
					args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
					args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.login_for_coin);
					args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_TEXT_RES, R.string.login);
					args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, R.drawable.popup_cancel_btn);
					args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
					Util.showDialogFragment(
							getFragmentManager(), 
							new CustomDialog(), 
							Constants.TAG_DIALOG_CUSTOM, 
							args, 
							SplashFragment.this, 
							Constants.REQUEST_USER_CONFIRM);
				}
			}
		});
		
		if(userNo > 0) {
			before.setVisibility(View.GONE);
			num.setVisibility(View.VISIBLE);
			getCoin();
		}
		else {
			num.setVisibility(View.GONE);
			before.setVisibility(View.VISIBLE);
		}
	}
	
	private void getCoin() {
		int failAmount = HeightChartPreference.getInt(Constants.PREF_COIN_TO_ADDED, 0);
		if(failAmount > 0) {
			Util.requestChargeCoin(getActivity(), SplashFragment.this, failAmount);
		}
		else {
			requestGetBalance();
		}
	}
	
	private void requestGetBalance() {
		HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_NO, String.valueOf(HeightChartPreference.getInt(Constants.PREF_USER_NO, 0)));
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
		
		GetBalance getBalance = new GetBalance(getActivity(), request);
		getBalance.setCallback(this);
		Model.runOnWorkerThread(getBalance);
	}
	
	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		if(mProgressDialog != null)
			mProgressDialog.dismiss();
		
		if(parser instanceof GetBalance) {
			String balance = result.get(Constants.TAG_EVENT_BALANCE);
			HeightChartPreference.putInt(Constants.PREF_LAST_DOLE_COIN, Integer.valueOf(balance));
			if(getView() != null) {
				TextView num = (TextView)getView().findViewById(R.id.splash_dole_coin_num);
				num.setText(balance);
			}
			
			//longzhe cui added
			mMixpanel.getPeople().set("coins", balance);
		}
		else if(parser instanceof RequestChargeCoin) {
			HeightChartPreference.putInt(Constants.PREF_COIN_TO_ADDED, 0);
			requestGetBalance();
		}
		else if(parser instanceof GetEventNoticeList) {
			String item = result.get(Constants.TAG_NOTICE_ITEMS);
			
			JSONArray jsonArray;
			try {
				jsonArray = new JSONArray(item);
				if(jsonArray == null || jsonArray.length() == 0)
					return;
				JSONObject firstItem = jsonArray.getJSONObject(0);
				mEventUrl = firstItem.getString("Summary");
				DLog.e(TAG, mEventUrl);
			} catch (JSONException e1) {
				DLog.d(TAG, " json parse error! ", e1);
			}
		} 
		else if(parser instanceof GetAppVersion) {
			final String appVersion = getString(R.string.app_version);
			final String serverAppVersion = result.get(Constants.TAG_MAJOR_VERSION);
			
			if(Util.isCriticalVersionUpdateRequired(appVersion, serverAppVersion)) {
				Bundle args = new Bundle();
				args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.notify_new_update);
				args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
				
				Util.showDialogFragment(
						getFragmentManager(), 
						new CustomDialog(), 
						Constants.TAG_DIALOG_CUSTOM, 
						args, 
						SplashFragment.this, 
						Constants.REQUEST_VERSION_CHECK);
			} else {
				initSplashFragment(getView(), null);		
			}
		}
		else if(parser instanceof RequestNewAuthKey) {
	    	if(Constants.DEBUG)
	    		Toast.makeText(getActivity(), result.get(Constants.TAG_AUTHKEY), Toast.LENGTH_LONG).show();
			HeightChartPreference.putString(Constants.PREF_AUTH_KEY, result.get(Constants.TAG_AUTHKEY));
			setCoinInfo();
		}
	}

	@Override
	public void onFailed(ServerTask parser, Map<String, String> result,
			int returnCode) {
		if(mProgressDialog != null)
			mProgressDialog.dismiss();

		if(parser instanceof GetBalance) {
			if(getView() != null) {
				TextView num = (TextView)getView().findViewById(R.id.splash_dole_coin_num);
				int coin = HeightChartPreference.getInt(Constants.PREF_LAST_DOLE_COIN, 0);
				int failAmount = HeightChartPreference.getInt(Constants.PREF_COIN_TO_ADDED, 0);
				num.setText(String.valueOf(coin+failAmount));
			}
		}
		else if(parser instanceof RequestChargeCoin) {
			requestGetBalance();
		}
		else if(parser instanceof GetAppVersion) {
			Toast.makeText(getActivity(), Util.switchErrorCode(returnCode, getResources()), Toast.LENGTH_SHORT).show();
		}
		else if(parser instanceof RequestNewAuthKey) {
			Toast.makeText(getActivity(), R.string.error_code_default, Toast.LENGTH_SHORT).show();
			Util.clearUserInfo();
			getActivity().finish();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		switch (requestCode) {
		case Constants.REQUEST_USER_CONFIRM:
			if(resultCode == Activity.RESULT_OK)
				Util.replaceFragment(
						getFragmentManager(), 
						new LoginFragment(), 
						R.id.container, 
						Constants.TAG_FRAGMENT_LOGIN, 
						null, 
						true);
			else
				Util.replaceFragment(
						getFragmentManager(), 
						new DoleCoinFragment(), 
						R.id.container, 
						Constants.TAG_FRAGMENT_DOLE_COIN, 
						null, 
						true);
			break;
			
		case Constants.REQUEST_DELETE_USER_INFO:
			if(resultCode == Activity.RESULT_OK)
				//TODO
				if(mDeleteList != null) {
					Model.runOnWorkerThread(new Runnable() {
						public void run() {
							Model.deleteNicks(getActivity(), mDeleteList);
							mDeleteList.clear();
							//TODO
							
							final View view = getView();
							// After delete
							getActivity().runOnUiThread(new Runnable() {
								@Override
								public void run() {
									View nightBg = view.findViewById(R.id.splash_night_bg);
									View dayBg = view.findViewById(R.id.splash_day_bg);
									if(mDrawableAniHelper == null)
										mDrawableAniHelper = new BgTransitionHelper(dayBg, nightBg, SplashFragment.this);
									mDrawableAniHelper.start();

									Toast.makeText(getActivity(), getResources().getString(R.string.deleted), Toast.LENGTH_LONG).show();
								}
							});
						}
					});
				}
			break;
		case Constants.REQUEST_VERSION_CHECK:
			if(resultCode == Activity.RESULT_OK) {
				final Activity activity = getActivity();
						
				final String appPackageName = activity.getPackageName(); 
				try {
				    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
				} catch (android.content.ActivityNotFoundException anfe) {
				    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
				}
				
				activity.finish();
			}
		default:
			break;
		}
	}
	
	private void startCharacterAnimation(List<View> views) {
		DLog.e(TAG, "startCharacterAnimation :"+mIsCharacterLoadFinished+","+mIsMainAnimationFinished);
		if(!mIsCharacterLoadFinished || !mIsMainAnimationFinished)
			return;
		final boolean isFirstLaunch = HeightChartPreference.getBoolean(Constants.PREF_IS_FIRST_APP_LAUNCH, true);

		if(isDetached() || getView() == null)
			return;
		
		Resources res = getResources();
		
		HorizontalScrollView scroll = (HorizontalScrollView)getView().findViewById(R.id.main_character_container_frame);
		CharacterContainer container = (CharacterContainer) getView().findViewById(R.id.character_container);
		int scrollPos = container.getInitPos();
		scroll.scrollTo(scrollPos, 0);
		
		final int count = getFragmentManager().getBackStackEntryCount();
		BackStackEntry entry = getFragmentManager().getBackStackEntryAt(count - 1);
		if(Constants.TAG_FRAGMENT_SPLASH.equals(entry.getName())) {
			container.setVisibility(View.VISIBLE);
		} else {
			container.setVisibility(View.INVISIBLE);	
		}
		
		int len;
		if(views == null)
			len = container.getChildCount();
		else {
			len = views.size();
			
			View addView = container.getChildAt(container.getChildCount()-1);
			NickNameInfo tag = (NickNameInfo)addView.getTag();
			if(tag.mCharacter == Characters.ADD_NICKNAME_OWL && container.getChildCount() > 1) {
				FrameLayout addButton = (FrameLayout)addView.findViewById(R.id.character_add_button_container);
				ImageView animalView = (ImageView)addView.findViewById(R.id.character_animal);
				ImageView treeView = (ImageView)addView.findViewById(R.id.character_tree);
	
				RelativeLayout.LayoutParams treeParams = (RelativeLayout.LayoutParams)treeView.getLayoutParams();
				RelativeLayout.LayoutParams animalParams = (RelativeLayout.LayoutParams)animalView.getLayoutParams();
				RelativeLayout.LayoutParams buttonParams = (RelativeLayout.LayoutParams)addButton.getLayoutParams();
				
				getView().findViewById(R.id.btn_bottom_delete).setEnabled(true);
				tag.setCharacter(Characters.CHARACTER_OWL);
				addView.setTag(tag);
				animalView.setImageResource(R.drawable.main_owl_ani);
				treeParams.leftMargin = 0;
				animalParams.leftMargin = res.getDimensionPixelSize(R.dimen.main_character_owl_margin_left);
				animalParams.bottomMargin = res.getDimensionPixelSize(R.dimen.main_character_owl_margin_bottom);
				buttonParams.bottomMargin = res.getDimensionPixelSize(R.dimen.main_character_add_button_main_bottom_margin);
				addView.setPadding(0, 0, 0, 0);
				
				Drawable draw = animalView.getDrawable();
				((AnimationDrawable)draw).stop();
				((AnimationDrawable)draw).start();
			}
		}
		
		int treeMaxHeight = res.getDimensionPixelSize(R.dimen.main_character_tree_max_height);
		for(int i = 0; i < len; i++) {
			final TranslateAnimation ani = new TranslateAnimation(0, 0, treeMaxHeight, 0);
			ani.setDuration(1000);
			ani.setRepeatCount(0);
			final View v;
			if(views == null)
				v = container.getChildAt(i);
			else
				v = views.get(i);
			
			ImageView imgView = (ImageView)v.findViewById(R.id.character_animal);
			final Drawable draw = imgView.getDrawable();

			v.startAnimation(ani);

			Object tag = v.getTag();
			if(tag instanceof NickNameInfo) {
				final NickNameInfo info = (NickNameInfo) tag;

				//if(info.mCharacter == Characters.ADD_NICKNAME_OWL || info.mCharacter == Characters.CHARACTER_OWL) {
				ani.setAnimationListener(new AnimationListener() {
					@Override
					public void onAnimationStart(Animation animation) {
						boolean isNewNickGuideShown = HeightChartPreference.getBoolean(GuideMode.GUIDE_SPLASH_NEW_NICK.name(), false);
						FragmentManager manager = getFragmentManager();
						int stackCount = manager.getBackStackEntryCount();
						final BackStackEntry entry = getFragmentManager().getBackStackEntryAt(stackCount - 1);
						if(Constants.TAG_FRAGMENT_SPLASH.equals(entry.getName()) && !isNewNickGuideShown) {
							final CharacterContainer container = (CharacterContainer) getView().findViewById(R.id.character_container);
							final int childCount = container.getChildCount();
							if(childCount == 2)
								showNewNickGuide();
						}
					}

					@Override
					public void onAnimationRepeat(Animation animation) {

					}

					@Override
					public void onAnimationEnd(Animation animation) {
						boolean isGuideShown = HeightChartPreference.getBoolean(GuideMode.GUIDE_SPLASH.name(), false);
						if(info.getNickNameId() == NickNameInfo.ADDNICK_OWL_ID && isFirstLaunch) {
							Util.replaceFragment(getFragmentManager(), new LoginFragment(), 
									R.id.container, Constants.TAG_FRAGMENT_LOGIN, null, false);
						}
						else if(info.getNickNameId() == NickNameInfo.ADDNICK_OWL_ID && popupEvent()) {

						}
						else if(info.mCharacter == Characters.ADD_NICKNAME_OWL && !isGuideShown) {
							showGuide();
						}

						if(/*applyAnimation && */draw instanceof AnimationDrawable) {
							((AnimationDrawable)draw).stop();
							((AnimationDrawable)draw).start();
						}
					}
				});
			}
		}

		if(container.getChildCount() == 1) {
			getView().findViewById(R.id.btn_bottom_delete).setEnabled(false);
		}
		else {
			getView().findViewById(R.id.btn_bottom_delete).setEnabled(true);
		}
		
		View root = getView();
		final CloudBg cloud = (CloudBg)root.findViewById(R.id.splash_cloud_container);
		final View mainContainer = root.findViewById(R.id.splash_main);

		final ImageButton btnAddHeight = (ImageButton) root.findViewById(R.id.btn_bottom_add_height);
		final ImageButton btnDelete = (ImageButton) root.findViewById(R.id.btn_bottom_delete);
		final ImageButton optionMenuBtn = (ImageButton) root.findViewById(R.id.btn_bottom_option_menu);
		final ImageButton btnDeleteBack = (ImageButton) root.findViewById(R.id.btn_bottom_return);
		final ImageButton btnConfirmDelete = (ImageButton) root.findViewById(R.id.btn_bottom_confirm_delete);
		
		btnAddHeight.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Model model = ((ApplicationImpl)getActivity().getApplication()).getModel();
				final List<NickNameInfo> nickList = model.getNickList();
				final int nickCount = (nickList == null)?0:nickList.size();
				if(nickCount == 0) {
					// Create Noname nick and move to QRCode
					createNonameNickInfo();
				} else {
					Util.showDialogFragment(getFragmentManager(), new ChooseNickNameDialog(), Constants.TAG_DIALOG_ADD_NICK, null);	
				}
			}
		});
		
		btnDelete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				View nightBg = getView().findViewById(R.id.splash_night_bg);
				View dayBg = getView().findViewById(R.id.splash_day_bg);
				if(mDrawableAniHelper == null)
					mDrawableAniHelper = new BgTransitionHelper(dayBg, nightBg, SplashFragment.this);
				
				if(mDrawableAniHelper.isOnTransition())
					return;
				
				mDrawableAniHelper.start();
			}
		});
		
		btnDeleteBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				View nightBg = getView().findViewById(R.id.splash_night_bg);
				View dayBg = getView().findViewById(R.id.splash_day_bg);
				if(mDrawableAniHelper == null)
					mDrawableAniHelper = new BgTransitionHelper(dayBg, nightBg, SplashFragment.this);
				
				if(mDrawableAniHelper.isOnTransition())
					return;
				
				mDrawableAniHelper.start();
			}
		});
		
		btnConfirmDelete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Bundle args = new Bundle();
				args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
				args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.selected_nickname_deletion);
				args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, R.drawable.popup_cancel_btn);
				args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
				
				Util.showDialogFragment(
						getFragmentManager(), 
						new CustomDialog(), 
						Constants.TAG_DIALOG_CUSTOM, 
						args, 
						SplashFragment.this, 
						Constants.REQUEST_DELETE_USER_INFO);
			}
		});
		
		optionMenuBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Util.replaceFragment(getFragmentManager(), new LoginBeforeOptionFragment(), 
						R.id.container, Constants.TAG_FRAGMENT_LOGIN_BEFORE_OPTION, null, true);
			}
		});
	}

	@Override
	public void onTransitionStarted(boolean isReverse) {
		toggleDeleteState(isReverse);
		mIsIndelete = isReverse;
		if(mIsIndelete) {
			mDeleteList.clear();
		}
	}

	@Override
	public void onTransitionFinished(boolean isDelete) {
		final CloudBg cloud = (CloudBg)getView().findViewById(R.id.splash_cloud_container);

		if(mIsIndelete) {
			cloud.setDeleteMode();
		} else {
			cloud.setNormalMode();
		}
		
		resetCharacterDrawable();
	}

	@Override
	public void onCharacterLoadFinished(List<View> views) {
		DLog.e(TAG, "onCharacterLoadFinished");
		DLog.e(TAG, "getView():"+getView());
		DLog.e(TAG, "views:"+views.size());
		if(getView() == null)
			return;
		
		final CharacterContainer container = (CharacterContainer) getView().findViewById(R.id.character_container);
		container.setVisibility(View.INVISIBLE);
		int len = views.size();
		
		View addNickView = null;
		final int childCount = container.getChildCount();
		for(int i = 0; i < childCount ; i++) {
			final View view = container.getChildAt(i);
			Object tag = view.getTag();
			if(tag instanceof NickNameInfo && ((NickNameInfo)tag).getNickNameId() == NickNameInfo.ADDNICK_OWL_ID) { 
				addNickView = view;
			}
		}
		
		final int addNickIdx = container.indexOfChild(addNickView);
		
		for(int i = 0; i < len; i++) {
			final View v = views.get(i);
			if(addNickView != null)
				container.addView(v, addNickIdx);	
			else
				container.addView(v);
		}
		mIsCharacterLoadFinished = true;
		DLog.e(TAG, "onCharacterLoadFinished");
		startCharacterAnimation(views);
	}

	@Override
	public void startCharacterDelete(List<View> views) {
		DLog.e(TAG, "startCharacterDelete");
		DLog.e(TAG, "getView():"+getView());
		DLog.e(TAG, "views:"+views.size());
		if(getView() == null)
			return;
		
		final CharacterContainer container = (CharacterContainer) getView().findViewById(R.id.character_container);
		View addView = container.getChildAt(container.getChildCount()-1);
		FrameLayout addButton = (FrameLayout)addView.findViewById(R.id.character_add_button_container);
		ImageView animalView = (ImageView)addView.findViewById(R.id.character_animal);
		ImageView treeView = (ImageView)addView.findViewById(R.id.character_tree);

		RelativeLayout.LayoutParams treeParams = (RelativeLayout.LayoutParams)treeView.getLayoutParams();
		RelativeLayout.LayoutParams animalParams = (RelativeLayout.LayoutParams)animalView.getLayoutParams();
		RelativeLayout.LayoutParams buttonParams = (RelativeLayout.LayoutParams)addButton.getLayoutParams();
		int len = views.size();

		if(container.getChildCount()-len == 1) {
			getView().findViewById(R.id.btn_bottom_delete).setEnabled(false);
			NickNameInfo tag = (NickNameInfo)addView.getTag();
			tag.setCharacter(Characters.ADD_NICKNAME_OWL);
			addView.setTag(tag);
			animalView.setImageResource(R.drawable.main_add_nickname_ani);
			treeParams.leftMargin = getResources().getDimensionPixelSize(R.dimen.main_character_tree_margin_left);
			animalParams.leftMargin = 0;
			animalParams.bottomMargin = getResources().getDimensionPixelSize(R.dimen.main_character_add_owl_bottom_margin);
			buttonParams.bottomMargin = getResources().getDimensionPixelSize(R.dimen.main_character_add_button_add_bottom_margin);
			addView.setPadding(getResources().getDimensionPixelSize(R.dimen.main_character_add_owl_left_padding), 0, 0, 0);
		}
		
		for(int i = 0; i < len; i++) {
			final View v = views.get(i);
			final TranslateAnimation ani = new TranslateAnimation(0, 0, 0, v.getHeight());
			ani.setDuration(1000);
			ani.setRepeatCount(0);
			
			ani.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
				}

				@Override
				public void onAnimationEnd(Animation animation) {
					container.removeView(v);
				}
			});
			v.startAnimation(ani);
		}
	}

	@Override
	public void startCharacterUpdate(List<View> views) {
		DLog.e(TAG, "startCharacterUpdate");
		DLog.e(TAG, "getView():"+getView());
		DLog.e(TAG, "views:"+views.size());
		if(getView() == null)
			return;
		
		final CharacterContainer container = (CharacterContainer) getView().findViewById(R.id.character_container);
		int len = views.size();
		int treeMaxHeight = getResources().getDimensionPixelSize(R.dimen.main_character_tree_max_height);
		
		Resources res = getResources();
		for(int i = 0; i < len; i++) {
			final View v = views.get(i);
			NickNameInfo info = (NickNameInfo)v.getTag();
			
			TextView nameView = (TextView)v.findViewById(R.id.character_text);
			ImageView fruitView = (ImageView)v.findViewById(R.id.character_fruit);
			ImageView characterView = (ImageView)v.findViewById(R.id.character_animal);
			ImageView treeView = (ImageView)v.findViewById(R.id.character_tree);
			nameView.setText(info.mNickName);
			Characters character = info.getCharacter(); 
			characterView.setTag(character);
			
			treeView.setImageDrawable(res.getDrawable(R.drawable.main_monkey_tree_01 + character.getCharacterIndex()));
			fruitView.setImageDrawable(res.getDrawable(R.drawable.main_monkey_fruit_01 + character.getCharacterIndex()));
			nameView.setTextColor(res.getColorStateList(character.getTagNameColor()));
			characterView.setImageDrawable(res.getDrawable(CharacterContainer.getDrawableId(info.getCharacter(), false)));
			
			final LinearLayout.LayoutParams viewParams = (LinearLayout.LayoutParams)v.getLayoutParams();
			final int oldMargin = viewParams.bottomMargin;
			final int newMargin = container.computeViewHeight(info.getLastHeight()) - treeMaxHeight;
			viewParams.bottomMargin = newMargin;
			v.setLayoutParams(viewParams);
			final TranslateAnimation ani = new TranslateAnimation(0, 0, newMargin-oldMargin, 0);
			ani.setDuration(1000);
			ani.setRepeatCount(0);
			v.startAnimation(ani);
		}
	}
	
	private void createNonameNickInfo() {
		final Activity activity = getActivity();
		final View rootView = getView();
		final FragmentManager manager = getFragmentManager();

		Model.runOnWorkerThread(new Runnable() {
			@Override
			public void run() {
				String name = Constants.NONAME;
				String birth = android.text.format.DateFormat.format("yyyy", new java.util.Date()).toString();
				Avatars avatars = Avatars.DOLEKEY;
				String genderText = Constants.NICKNAME_GENDER_BOY;
				Characters character = Characters.getCharacterAt(avatars.getIndex());
				
				final NickNameInfo nickInfo = NickNameInfo.insert(activity, name, character, birth, genderText);
				if(nickInfo == null) {
					// insert fail
					return;
				}
				
				final Model model = ((ApplicationImpl) activity.getApplication()).getModel();
				model.addNickName(nickInfo);

				rootView.post(new Runnable() {
					@Override
					public void run() {
						final Bundle args = new Bundle();
						args.putLong(Constants.NICKNAME_ID, nickInfo.getNickNameId());
						args.putBoolean(Constants.BYPASS_DETAIL_FRAG, true);
						
						Util.replaceFragment(manager, new DetailFragment(), 
								R.id.container, Constants.TAG_FRAGMENT_DETAIL, args, true);
					}
				});
			}
		});
	}
}
