package com.dole.heightchart;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.dole.heightchart.camera.CameraManager;
import com.dole.heightchart.camera.CameraManager.CameraMode;
import com.dole.heightchart.server.RequestCheckQRCode;
import com.dole.heightchart.server.ServerTask;
import com.dole.heightchart.server.ServerTask.IServerResponseCallback;
import com.dole.heightchart.ui.CustomDialog;
import com.dole.heightchart.ui.QRCodeDetectedBoundView;
import com.dole.heightchart.ui.QRCodeDetectedBoundView.OnRectAnimationCallback;
import com.dole.heightchart.ui.SimpleAnimator;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class QRCodeFragment extends Fragment implements IServerResponseCallback, OnRectAnimationCallback {

	private static final String TAG = "QRCodeFragment";
	private static final String QR_TAG_T = "T";
	private static final String QR_TAG_CP = "CP";
	private static final int QRCODE_LENGTH = 16;
	private static final String PREFIX_SCHEME = "http";
	
	private HandlerThread mWorkerThread; //= new HandlerThread("qrcode-scan");
    private Handler mWorker;// = new Handler(mWorkerThread.getLooper());
	
	private QRCodeReader mCodeReader;
	private NickNameInfo mNickInfo;
	private CameraManager mCameraManager;
	private PreviewCallback mPreviewCallback;
	
	private String mQrcodeValueT = null;
	private String mQrStringValueCp = null;
	private String mQrPromotionCode = null;
	private boolean mIsUsedCodeAndWithin24Hour = false;
	
	private int mTmpInputTextCount = 0;
	private boolean mIsScanning = false;
	private long mDeleteHeightIdForCodeReuse = Constants.UNAVAILABLE_ID;

	private ProgressDialog mProgressDialog;
	private FutureTask<String> mCurrentTask;
	private RequestCheckQRCode mCheckQRCodeTask;
	
	private Bitmap mDebugImage;
	
	MixpanelAPI mMixpanel;
	
	boolean bIsScanned;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		//longzhe cui added
		mMixpanel = MixpanelAPI.getInstance(getActivity(), Util.MIXPANEL_TOKEN);

		mMixpanel.track("Open QRCode", null);
		mMixpanel.timeEvent("Scanned QRCode");
		mMixpanel.timeEvent("Recorded Height");
		
		return inflater.inflate(R.layout.camera_qrcode, null);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		
		super.onViewCreated(view, savedInstanceState);
		Bundle args = getArguments();
		if(args != null) {
			long nickId = args.getLong(Constants.NICKNAME_ID);
			mNickInfo = Util.getNickNameInfoById(getActivity(), nickId);
		}
		mCodeReader = new QRCodeReader();
		
		final EditText directInputCode = (EditText) view.findViewById(R.id.qrcode_direct_input);
		directInputCode.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}
			
			@Override
			public void afterTextChanged(Editable s) {
				mTmpInputTextCount = s.length();
				if(mTmpInputTextCount  == QRCODE_LENGTH) {
					
					bIsScanned = false;
					
					mQrStringValueCp = s.toString();
					requestQrCodeValidation();
				}
			}
		});
		
		directInputCode.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				switch (keyCode) {
				case KeyEvent.KEYCODE_ENTER:
				case KeyEvent.KEYCODE_BACK:

					if(mTmpInputTextCount != QRCODE_LENGTH) {
						Toast.makeText(getActivity(), R.string.enter_n_length_code, Toast.LENGTH_SHORT).show();
					}
				default:
					break;
				}

				return false;
			}
		});

		final View focusView = view.findViewById(R.id.qrcode_center_focus);
		mPreviewCallback = new QRCodePreviewCallback();
 		mCameraManager = new CameraManager(getActivity(), getView(), CameraMode.QRCODE, focusView);
 		mCameraManager.setBarcodeSceneMode();
 		mCameraManager.setBestExposure(false);
 		mCameraManager.setBestPreviewFPS();
		
		final QRCodeDetectedBoundView qrCodeBound = (QRCodeDetectedBoundView) getView().findViewById(R.id.qrcode_code_bound);
		qrCodeBound.setCameraManager(mCameraManager);
		qrCodeBound.setOnRectAnimationCallback(this);
		
		final boolean sampleQrcodeUsed = HeightChartPreference.getBoolean(Constants.PREF_SAMPLE_QRCODE_USED, false);
		if(!sampleQrcodeUsed) {
			Bundle dialogArgs = new Bundle();
			dialogArgs.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_tutorial);
			dialogArgs.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.code_as_image_required);
			dialogArgs.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);

			Util.showDialogFragment(
					getFragmentManager(), 
					new CustomDialog(), 
					Constants.TAG_DIALOG_CUSTOM, 
					dialogArgs, 
					QRCodeFragment.this, 
					Constants.REQUEST_USER_CONFIRM);
		} else {
			mCameraManager.setCallback(mPreviewCallback, null);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if(mCameraManager != null) {
			if(mWorkerThread == null)
				mWorkerThread = new HandlerThread("qrcode-scan");
			
			mWorkerThread.start();
			if(mWorker == null)
				mWorker = new Handler(mWorkerThread.getLooper());
			mCameraManager.onResume();
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if(mCameraManager != null) {
			mWorkerThread.quit();
			mWorkerThread = null;
			
			mIsScanning = false;
			mCameraManager.onPause();
		}
	}

	private final class QRCodePreviewCallback implements PreviewCallback {

		@Override
		public void onPreviewFrame(final byte[] data, final Camera camera) {
			DLog.d(TAG, " qr preview : isScanning " + mIsScanning + " | isReleased = " + mCameraManager.isCameraReleased() + " isFocused = " + mCameraManager.isFocused());
			if(!mIsScanning && !mCameraManager.isCameraReleased() && mCameraManager.isFocused()) {
				mIsScanning = true;
				scanQRCode(data, camera);
			}
		}
    }
	
	private class QRCodeScanTask implements Callable<String> {
		private Camera mCamera;
		private byte[] mData;
		
		public QRCodeScanTask(byte[] data, Camera camera) {
			mData = data;
			mCamera = camera;
		}
		
		@Override
		public String call() throws Exception {
			
			if(mCameraManager.isCameraReleased()) {
				mIsScanning = false;
				return null;
			}	
			
			final Parameters parameters = mCamera.getParameters();
	    	final Size previewSize = parameters.getPreviewSize();
	    	
	    	int width = previewSize.width;
	    	int height = previewSize.height;
	    	
	    	final RectF focusRectInPreview = mCameraManager.getFramingRectInPreview();
	    	if(Constants.DEBUG_QRCODE_PREVIEW_AREA) {
	    		getActivity().runOnUiThread(new Runnable() {
	    			public void run() {
	    				if(getView() == null)
	    					return;
	    				
	    				final QRCodeDetectedBoundView qrCodeBound = (QRCodeDetectedBoundView) getView().findViewById(R.id.qrcode_code_bound);
	    				qrCodeBound.setDebugRect(focusRectInPreview);
	    				qrCodeBound.invalidate();
					}
				});
	    	}
	    	DLog.d(TAG, " focus rect in preview = " + focusRectInPreview);
	    	DLog.d(TAG, " width, height = " + width + ", " + height);
	    	DLog.d(TAG, " focusRectInPreview.left, focusRectInPreview.top = " + focusRectInPreview.left + ", " + focusRectInPreview.top);
	    	
	    	if(Constants.DEBUG_QRCODE_PREVIEW_AREA) {
	    		ByteArrayOutputStream out = new ByteArrayOutputStream();
	    		final YuvImage yuvImage = new YuvImage(mData, parameters.getPreviewFormat(), width, height, null);
	    		yuvImage.compressToJpeg(
	    				new Rect(
	    						(int) focusRectInPreview.left, 
	    						(int) focusRectInPreview.top, 
	    						(int) focusRectInPreview.right, 
	    						(int) focusRectInPreview.bottom),
	    						//	    				new Rect(0, 0, width, height), 
	    						100, out);
	    		byte[] imageBytes = out.toByteArray();
	    		if(mDebugImage != null && !mDebugImage.isRecycled())
	    			mDebugImage.recycle();

	    		mDebugImage = BitmapFactory.decodeByteArray(imageBytes, 0, out.size());

	    		getActivity().runOnUiThread(new Runnable() {
	    			public void run() {
	    				View rootView = getView();
	    				if(rootView != null) {
	    					ImageView debugView = (ImageView) rootView.findViewById(R.id.qrcode_debug);
	    					if(debugView.getVisibility() != View.VISIBLE)
	    						debugView.setVisibility(View.VISIBLE);
	    					debugView.setImageBitmap(mDebugImage);
	    				}
	    			}
	    		});
	    	}
	    	
	    	PlanarYUVLuminanceSource source = 
	    			new PlanarYUVLuminanceSource(mData,
	    					width, height,
	    					(int) focusRectInPreview.left, (int) focusRectInPreview.top, 
	    					(int) focusRectInPreview.width(), (int) focusRectInPreview.height(), 
	    					false);

	    	Result result = null;
	    	if (source != null) {
	    		BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
	    		try {
	    			result = mCodeReader.decode(bitmap);
	    		} catch (ReaderException re) {
	    			DLog.e(TAG, "QRCode not decoded", re);
	    		} finally {
	    			mCodeReader.reset();
	    		}
	    	}
	    	
	    	if(result == null) {
	    		mIsScanning = false;
	    		return null;
	    	}
	    	
	    	final ResultPoint[] points = result.getEnclosingPoints();
			final View rootView = getView();
	    	getActivity().runOnUiThread(new Runnable() {
				public void run() {
					final View focusView = rootView.findViewById(R.id.qrcode_center_focus);
					final QRCodeDetectedBoundView qrCodeBound = (QRCodeDetectedBoundView) rootView.findViewById(R.id.qrcode_code_bound);
					qrCodeBound.setResultPoint(focusView, focusRectInPreview, points, previewSize);
					qrCodeBound.setCleared(false);
			    	qrCodeBound.invalidate();
				}
			});
	    	
	    	List<NameValuePair> params = null;
			try {
				String resultStr =  result.toString();
				if(resultStr == null) {
					mIsScanning = false;
					return null;
				}
				
				int urlPos = result.toString().indexOf(PREFIX_SCHEME);
				if(urlPos < 0) {
					mIsScanning = false;
					return null;
				}
				
				params = URLEncodedUtils.parse(new URI(resultStr.substring(urlPos)), "UTF-8");
				for (NameValuePair param : params) {
					final String key = param.getName();
					final String value = param.getValue();
					if(QR_TAG_T.equals(key)) {
						mQrcodeValueT = value;
					} else if(QR_TAG_CP.equals(key)) {
						mQrStringValueCp = value;
					}
				}
				DLog.d(TAG, " decoded code = " + mQrStringValueCp);
				
				return mQrStringValueCp;
			} catch (URISyntaxException e) {
				DLog.d(TAG, "URISyntax error");
			} finally {
				mIsScanning = false;
			}

			return null;
		}
	}
	
	private boolean scanQRCode(byte[] data, final Camera camera) {
		if(mCameraManager.isCameraReleased())
			return false;
		
		QRCodeScanTask task = new QRCodeScanTask(data, camera);
		mCurrentTask = new FutureTask<String>(task);
		mWorker.post(mCurrentTask);
		
		Model.runOnWorkerThread(new Runnable() {
			
			@Override
			public void run() {
				try {
					mCurrentTask.get();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				} finally {
					if(mCameraManager.isPreviewing())
						mCameraManager.startOneShotPreviewCallbackLoop();
				}
			}
		});
		
		return (mQrStringValueCp != null);
	}
	
	private void setPendingFocusCallback() {
		View view = getView();
		if(view != null) {
			view.postDelayed(new Runnable() {

				@Override
				public void run() {
					//mIsScanning = false;
					if(!mCameraManager.isCameraReleased()) {
						mCameraManager.pausePreview(false);
		    		}			
				}
			}, 3000);
		}
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
	}
	
	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		if(mProgressDialog != null) {
			mProgressDialog.dismiss();
			mProgressDialog = null;
		}
		
		if(parser instanceof RequestCheckQRCode) {
			boolean canUse = Boolean.valueOf(result.get(Constants.TAG_CAN_USE));
			if(canUse) {
				DLog.d(TAG, " result = " + result);
				JSONObject jsonObj;
				try {
					jsonObj = new JSONObject(result.get("Promotion"));
					String code = (String)jsonObj.get("Name");
					mQrPromotionCode = code.substring(code.indexOf("-") + 1);
					DLog.d(TAG, " code = " + mQrPromotionCode);
					
					replaceToInputFragment();
					
                	JSONObject scanObj = new JSONObject();
                    if (bIsScanned)
                    {
                    	scanObj.put("type", "scan");
                    	mMixpanel.track("Scanned QRCode", scanObj);
                    }
                    else
                    {
                    	scanObj.put("type", "direct");
                    	mMixpanel.track("Scanned QRCode", scanObj);
                    }

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			
			if(mCheckQRCodeTask != null)
				mCheckQRCodeTask = null;
		}
	}

	@Override
	public void onFailed(ServerTask parser, Map<String, String> result, int returnCode) {
		if(mProgressDialog != null) {
			mProgressDialog.dismiss();
			mProgressDialog = null;
		}
		
		if(parser instanceof RequestCheckQRCode) {
			if(result == null) {
				Toast.makeText(getActivity(), Util.switchErrorCode(returnCode, getResources()), Toast.LENGTH_SHORT).show();
				getFragmentManager().popBackStack();
				return;
			}
				
			boolean canUse = Boolean.valueOf(result.get(Constants.TAG_CAN_USE));
			final Activity activity = getActivity();
			if(activity == null)
				return;
			
			if(!canUse) {
				final Model model = ((ApplicationImpl)activity.getApplication()).getModel();
				HeightInfo usedInfo = null;
				if(mQrStringValueCp != null) {
					for(NickNameInfo nickInfo : model.getNickList()) {
						for(HeightInfo info : nickInfo.getHeightList()) {
							DLog.d(TAG, " code = " + info.getQrCode());
							if(mQrStringValueCp.equals(info.getQrCode())) {
								usedInfo = info;
								mDeleteHeightIdForCodeReuse = info.getHeightId();
								break;
							}
						}
					}
				}

				int warningId = R.string.unavailable_code; 
				int requestCode = Constants.REQUEST_USER_NOTICE_FAILURE;
				if(usedInfo != null) {
					long timeDiff = new Date().getTime() - usedInfo.getInputDate().getTime();
					long diffHours = timeDiff / (60 * 60 * 1000);
					DLog.d(TAG, " diffHours = " + diffHours);

					if(diffHours <= 24) {
						DLog.d(TAG, " cannot! = " + result + " ! but reuse!!");
						warningId = R.string.used_code;
						requestCode = Constants.REQUEST_USER_NOTICE_REUSE_USED_CODE;
					}
				}
				
				DLog.d(TAG, " cannot! = " + result);
				final Bundle dialogArgs = new Bundle();
				dialogArgs.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
				dialogArgs.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, warningId);
				dialogArgs.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
				if(requestCode == Constants.REQUEST_USER_NOTICE_REUSE_USED_CODE)
					dialogArgs.putBoolean(Constants.CUSTOM_DIALOG_ALLOW_CANCEL, true);

				Util.showDialogFragment(
						getFragmentManager(), 
						new CustomDialog(), 
						Constants.TAG_DIALOG_CUSTOM, 
						dialogArgs, 
						QRCodeFragment.this, 
						requestCode);
			}
			
			if(mCheckQRCodeTask != null)
				mCheckQRCodeTask = null;
		}
	}
	
	private void requestQrCodeValidation() {
		if(mCheckQRCodeTask != null)
			return;
		
		final Activity activity = getActivity();
		if(activity == null)
			return;
		
		//Request qr code validity
		Map<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_NO, String.valueOf(HeightChartPreference.getInt(Constants.TAG_USER_NO, 0)));
		request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
		request.put(Constants.TAG_SERIAL, mQrStringValueCp);
		request.put(Constants.TAG_PROMOTION_INFO, "1");
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(activity));

		mCheckQRCodeTask = new RequestCheckQRCode(activity, request);
		mCheckQRCodeTask.setCallback(this);
		Model.runOnWorkerThread(mCheckQRCodeTask);
		
		mProgressDialog = Util.openProgressDialog(activity);
	}
	
	private void onQrcodeDetected() {
		final boolean sampleQrcodeUsed = HeightChartPreference.getBoolean(Constants.PREF_SAMPLE_QRCODE_USED, false);
		
		if(!sampleQrcodeUsed) {
			//TODO
			final View rootView = getView();
			if(rootView == null)
				return;
			
			final View qrSample = rootView.findViewById(R.id.qrcode_sample);
			final Animation fadeOutAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
			fadeOutAnim.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
				}

				@Override
				public void onAnimationEnd(Animation animation) {
					qrSample.setVisibility(View.GONE);
					replaceToInputFragment();
				}
			});

			qrSample.startAnimation(fadeOutAnim);
		}
	}

	@Override
	public void onRectAnimationStarted() {
		final View rootView = getView();
		rootView.postDelayed(new Runnable() {
			@Override
			public void run() {
				final boolean sampleQrcodeUsed = HeightChartPreference.getBoolean(Constants.PREF_SAMPLE_QRCODE_USED, false);
				
				if(!sampleQrcodeUsed) {
					final View rootView = getView();
					if(rootView == null)
						return;
					
					final View qrSample = rootView.findViewById(R.id.qrcode_sample);
					
					qrSample.setVisibility(View.GONE);
					replaceToInputFragment();
				} else {
					if(mQrStringValueCp == null || mQrcodeValueT == null) {

			    		final Bundle dialogArgs = new Bundle();
						dialogArgs.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
						dialogArgs.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.unavailable_code);
						dialogArgs.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);

						Util.showDialogFragment(
								getFragmentManager(), 
								new CustomDialog(), 
								Constants.TAG_DIALOG_CUSTOM, 
								dialogArgs, 
								QRCodeFragment.this, 
								Constants.REQUEST_DIALOG_UNAVAILABLE_CODE);
			    		return;
			    	}
					
					if(Constants.DEBUG_QRCODE_PREVIEW_AREA) {
						mIsScanning = false;
						final Activity activity = getActivity();
						if(activity != null) {
							activity.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									//setPendingFocusCallback();
									mCameraManager.resumeQrcodeAutoFocus();
								}
							});
						}
					} else {
						
						bIsScanned = true;

						requestQrCodeValidation();
					}
				}
			}
		}, SimpleAnimator.DEFAULT_DURATION);
	}

	@Override
	public void onRectAnimationFinished() {
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		switch (requestCode) {
		case Constants.REQUEST_USER_NOTICE_FAILURE:
			if(Constants.DEBUG_BYPASS_ALL_QRCODE) {
				Toast.makeText(getActivity(), "DEBUG MODE - BYPASS QRCODE ERROR", Toast.LENGTH_SHORT).show();
				replaceToInputFragment();
			} else {
	    		mIsScanning = false;
	    		mCameraManager.pausePreview(false);
			}
			break;
		case Constants.REQUEST_USER_NOTICE_REUSE_USED_CODE:
			if(resultCode == Activity.RESULT_OK) {
				mIsUsedCodeAndWithin24Hour = true;
				//Move to InputHeight
				replaceToInputFragment();
			} else {
				mCameraManager.resumeQrcodeAutoFocus();
			}
			break;
		case Constants.REQUEST_USER_CONFIRM: 
			if(resultCode == Activity.RESULT_OK) {
				final View rootView = getView();
				final View qrSample = rootView.findViewById(R.id.qrcode_sample);
				qrSample.setVisibility(View.VISIBLE);
				
				final QRCodeDetectedBoundView qrCodeBound = (QRCodeDetectedBoundView) rootView.findViewById(R.id.qrcode_code_bound);
		    	getActivity().runOnUiThread(new Runnable() {
					public void run() {

						final int width = getResources().getDimensionPixelSize(R.dimen.qrcode_sample_rect_width);
						final int height = getResources().getDimensionPixelSize(R.dimen.qrcode_sample_rect_height);
						final int left = qrSample.getLeft();
						final int bottom = qrSample.getBottom();
						final int top = bottom - height;
						final int right = left + width;
						
						final RectF target = new RectF(left, top, right, bottom);
						final RectF focusRectInPreview = mCameraManager.getFramingRectInPreview();
						
						Matrix m = mCameraManager.getPreviewConvertMatrix();
						m.mapRect(target);
						
						ResultPoint[] points = new ResultPoint[4]; 
						points[0] = new ResultPoint(target.left, target.top);
						points[1] = new ResultPoint(target.left, target.bottom);
						points[2] = new ResultPoint(target.right, target.bottom);
						points[3] = new ResultPoint(target.right, target.top);
						
						qrCodeBound.setResultPoint(
								focusRectInPreview,
								target);
						
						qrCodeBound.setCleared(false);
				    	qrCodeBound.invalidate();
					}
				});
			}
			break;
			
		case Constants.REQUEST_DIALOG_UNAVAILABLE_CODE:
			mIsScanning = false;
			final Activity activity = getActivity();
			if(activity != null) {
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						setPendingFocusCallback();						
					}
				});
			}

			break;

		default:
			break;
		}
	}
	
	private void replaceToInputFragment() {
		final Bundle args = new Bundle();
		args.putLong(Constants.NICKNAME_ID, mNickInfo.mNickNameId);
		args.putString(Constants.QRCODE, mQrStringValueCp);
		args.putString(Constants.PROMOTION_TYPE, mQrPromotionCode);
		args.putBoolean(Constants.USED_CODE_WITHIN_24H, mIsUsedCodeAndWithin24Hour);
		DLog.e(TAG, "QR CODE:"+mQrPromotionCode);
		
		if(mDeleteHeightIdForCodeReuse > 0)
			args.putLong(Constants.REUSE_HEIGHT_ID, mDeleteHeightIdForCodeReuse);
		
		Util.replaceFragment(getFragmentManager(), new InputHeightFragment(), 
				R.id.container, Constants.TAG_FRAGMENT_INPUT_HEIGHT, args, false);
	}
}
