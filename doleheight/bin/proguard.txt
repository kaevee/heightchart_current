# view res/layout/camera_record_height.xml #generated:21
# view res/layout/height_viewer.xml #generated:6
-keep class android.support.v4.view.ViewPager { <init>(...); }

# view AndroidManifest.xml #generated:30
-keep class com.dole.heightchart.ApplicationImpl { <init>(...); }

# view AndroidManifest.xml #generated:61
-keep class com.dole.heightchart.ChartActivity { <init>(...); }

# view AndroidManifest.xml #generated:38
-keep class com.dole.heightchart.HeightChartProvider { <init>(...); }

# view AndroidManifest.xml #generated:43
-keep class com.dole.heightchart.MainActivity { <init>(...); }

# view AndroidManifest.xml #generated:91
-keep class com.dole.heightchart.push.GcmIntentService { <init>(...); }

# view AndroidManifest.xml #generated:71
-keep class com.dole.heightchart.push.PushReceiver { <init>(...); }

# view res/layout/height_viewer.xml #generated:23
-keep class com.dole.heightchart.ui.AnimationView { <init>(...); }

# view res/layout/detail_height_tree.xml #generated:33
# view res/layout/detail_height_tree_item.xml #generated:15
-keep class com.dole.heightchart.ui.AverageHeightView { <init>(...); }

# view res/layout/splash_main.xml #generated:33
-keep class com.dole.heightchart.ui.CharacterContainer { <init>(...); }

# view res/layout/chart_cloud_bg.xml #generated:1
-keep class com.dole.heightchart.ui.ChartCloudBg { <init>(...); }

# view res/layout/chart.xml #generated:58
-keep class com.dole.heightchart.ui.ChartView { <init>(...); }

# view res/layout/detail_cloud_bg.xml #generated:1
# view res/layout/input_height_cloud_bg.xml #generated:1
# view res/layout/splash_cloud_bg.xml #generated:1
-keep class com.dole.heightchart.ui.CloudBg { <init>(...); }

# view res/layout/detail_height_tree_item_left.xml #generated:6
# view res/layout/detail_height_tree_item_right.xml #generated:6
-keep class com.dole.heightchart.ui.DetailIconView { <init>(...); }

# view res/layout/user_detail.xml #generated:134
-keep class com.dole.heightchart.ui.DoleCheckBox { <init>(...); }

# view res/layout/about_main.xml #generated:69
# view res/layout/add_nickname.xml #generated:97
# view res/layout/agree_personal_main.xml #generated:78
# view res/layout/agree_personal_main.xml #generated:89
# view res/layout/backup_main.xml #generated:64
# view res/layout/birthdate_dialog.xml #generated:40
# view res/layout/birthdate_dialog.xml #generated:51
# view res/layout/city_chooser.xml #generated:25
# view res/layout/custom_dialog.xml #generated:68
# view res/layout/custom_dialog.xml #generated:80
# view res/layout/delete_main.xml #generated:104
# view res/layout/dole_coin_main.xml #generated:193
# view res/layout/event_main.xml #generated:41
# view res/layout/facebook_invite.xml #generated:16
# view res/layout/help_main.xml #generated:27
# view res/layout/help_main.xml #generated:99
# view res/layout/import_photo.xml #generated:34
# view res/layout/login.xml #generated:43
# view res/layout/login_forgot_your_password.xml #generated:50
# view res/layout/login_option_menu_after_login.xml #generated:109
# view res/layout/login_option_menu_after_login.xml #generated:163
# view res/layout/login_option_menu_after_login.xml #generated:182
# view res/layout/login_option_menu_after_login.xml #generated:193
# view res/layout/login_option_menu_after_login.xml #generated:28
# view res/layout/login_option_menu_before_login.xml #generated:138
# view res/layout/login_option_menu_before_login.xml #generated:157
# view res/layout/login_option_menu_before_login.xml #generated:168
# view res/layout/login_option_menu_before_login.xml #generated:53
# view res/layout/nickname_chooser.xml #generated:25
# view res/layout/request_paper_main.xml #generated:134
# view res/layout/share_chooser.xml #generated:38
# view res/layout/user_detail.xml #generated:12
-keep class com.dole.heightchart.ui.FontButton { <init>(...); }

# view res/layout/agree_personal_main.xml #generated:25
# view res/layout/agree_personal_main.xml #generated:55
# view res/layout/chart.xml #generated:41
# view res/layout/delete_main.xml #generated:73
# view res/layout/event_main.xml #generated:23
# view res/layout/facebook_invite.xml #generated:53
# view res/layout/facebook_invite_item.xml #generated:16
-keep class com.dole.heightchart.ui.FontCheckBox { <init>(...); }

# view res/layout/add_nickname.xml #generated:35
# view res/layout/camera_qrcode.xml #generated:12
# view res/layout/delete_main.xml #generated:90
# view res/layout/guide_input_height.xml #generated:55
# view res/layout/help_main.xml #generated:42
# view res/layout/help_main.xml #generated:55
# view res/layout/input_height_main.xml #generated:81
# view res/layout/login.xml #generated:19
# view res/layout/login.xml #generated:31
# view res/layout/login_forgot_your_password.xml #generated:39
# view res/layout/request_paper_main.xml #generated:109
# view res/layout/request_paper_main.xml #generated:117
# view res/layout/request_paper_main.xml #generated:48
# view res/layout/request_paper_main.xml #generated:62
# view res/layout/request_paper_main.xml #generated:76
# view res/layout/request_paper_main.xml #generated:90
# view res/layout/user_detail.xml #generated:38
# view res/layout/user_detail.xml #generated:48
# view res/layout/user_detail.xml #generated:58
# view res/layout/user_detail.xml #generated:68
# view res/layout/user_detail.xml #generated:78
# view res/layout/user_detail.xml #generated:90
-keep class com.dole.heightchart.ui.FontEditText { <init>(...); }

# view res/layout/add_nickname.xml #generated:68
# view res/layout/add_nickname.xml #generated:81
# view res/layout/user_detail.xml #generated:105
# view res/layout/user_detail.xml #generated:118
-keep class com.dole.heightchart.ui.FontRadioButton { <init>(...); }

# view res/layout/about_main.xml #generated:39
# view res/layout/about_main.xml #generated:55
# view res/layout/add_nickname.xml #generated:17
# view res/layout/add_nickname.xml #generated:46
# view res/layout/add_nickname_avatar_item.xml #generated:19
# view res/layout/backup_main.xml #generated:25
# view res/layout/backup_main.xml #generated:35
# view res/layout/backup_main.xml #generated:47
# view res/layout/backup_main.xml #generated:6
# view res/layout/birthdate_dialog.xml #generated:12
# view res/layout/btn_get_coin.xml #generated:30
# view res/layout/camera_qrcode.xml #generated:43
# view res/layout/camera_record_height.xml #generated:78
# view res/layout/character_view.xml #generated:34
# view res/layout/city_chooser.xml #generated:6
# view res/layout/city_list_item.xml #generated:1
# view res/layout/custom_dialog.xml #generated:35
# view res/layout/custom_dialog.xml #generated:43
# view res/layout/custom_dialog.xml #generated:7
# view res/layout/delete_account_item.xml #generated:18
# view res/layout/delete_account_item.xml #generated:32
# view res/layout/delete_main.xml #generated:39
# view res/layout/detail_height_tree.xml #generated:100
# view res/layout/detail_height_tree.xml #generated:143
# view res/layout/detail_height_tree.xml #generated:38
# view res/layout/detail_height_tree.xml #generated:66
# view res/layout/detail_height_tree_item.xml #generated:20
# view res/layout/detail_height_tree_item.xml #generated:41
# view res/layout/detail_height_tree_item_left.xml #generated:14
# view res/layout/detail_height_tree_item_left.xml #generated:26
# view res/layout/detail_height_tree_item_right.xml #generated:14
# view res/layout/detail_height_tree_item_right.xml #generated:25
# view res/layout/dole_coin_main.xml #generated:115
# view res/layout/dole_coin_main.xml #generated:149
# view res/layout/dole_coin_main.xml #generated:171
# view res/layout/dole_coin_main.xml #generated:40
# view res/layout/dole_coin_main.xml #generated:93
# view res/layout/facebook_invite.xml #generated:30
# view res/layout/facebook_invite_item.xml #generated:31
# view res/layout/guide_input_height.xml #generated:71
# view res/layout/guide_input_height.xml #generated:81
# view res/layout/guide_login.xml #generated:25
# view res/layout/guide_login.xml #generated:47
# view res/layout/guide_login.xml #generated:6
# view res/layout/guide_login.xml #generated:69
# view res/layout/guide_login.xml #generated:82
# view res/layout/guide_login.xml #generated:95
# view res/layout/height_viewer.xml #generated:64
# view res/layout/help_main.xml #generated:70
# view res/layout/help_main.xml #generated:87
# view res/layout/input_height_item.xml #generated:14
# view res/layout/input_height_main.xml #generated:107
# view res/layout/input_height_main.xml #generated:57
# view res/layout/input_height_main.xml #generated:97
# view res/layout/login.xml #generated:101
# view res/layout/login.xml #generated:118
# view res/layout/login.xml #generated:71
# view res/layout/login.xml #generated:86
# view res/layout/login_forgot_your_password.xml #generated:26
# view res/layout/login_option_menu_after_login.xml #generated:144
# view res/layout/login_option_menu_after_login.xml #generated:17
# view res/layout/login_option_menu_after_login.xml #generated:211
# view res/layout/login_option_menu_after_login.xml #generated:238
# view res/layout/login_option_menu_after_login.xml #generated:57
# view res/layout/login_option_menu_after_login.xml #generated:78
# view res/layout/login_option_menu_after_login.xml #generated:89
# view res/layout/login_option_menu_before_login.xml #generated:106
# view res/layout/login_option_menu_before_login.xml #generated:200
# view res/layout/login_option_menu_before_login.xml #generated:38
# view res/layout/nickname_chooser.xml #generated:6
# view res/layout/nickname_list_item.xml #generated:1
# view res/layout/request_paper_main.xml #generated:17
# view res/layout/share_chooser.xml #generated:17
# view res/layout/share_chooser.xml #generated:6
-keep class com.dole.heightchart.ui.FontTextView { <init>(...); }

# view res/layout/detail_height_tree.xml #generated:8
-keep class com.dole.heightchart.ui.FullHeightTree { <init>(...); }

# view res/layout/import_photo.xml #generated:6
-keep class com.dole.heightchart.ui.ImportImageView { <init>(...); }

# view res/layout/camera_qrcode.xml #generated:78
-keep class com.dole.heightchart.ui.QRCodeDetectedBoundView { <init>(...); }

# view res/layout/camera_record_height.xml #generated:110
# view res/layout/camera_record_height.xml #generated:119
# view res/layout/camera_record_height.xml #generated:128
# view res/layout/camera_record_height.xml #generated:137
# view res/layout/camera_record_height.xml #generated:158
# view res/layout/camera_record_height.xml #generated:167
# view res/layout/camera_record_height.xml #generated:176
# view res/layout/camera_record_height.xml #generated:185
-keep class com.dole.heightchart.ui.SelectOnDisableImageView { <init>(...); }

# view res/layout/add_nickname.xml #generated:8
-keep class com.dole.heightchart.ui.SoftCoverflow { <init>(...); }

# view res/layout/splash_main.xml #generated:43
-keep class com.dole.heightchart.ui.SplashBottom { <init>(...); }

# view res/layout/detail_height_tree.xml #generated:18
-keep class com.dole.heightchart.ui.ZoomHeightTree { <init>(...); }

# view AndroidManifest.xml #generated:67
-keep class com.facebook.LoginActivity { <init>(...); }

# view res/layout/com_facebook_usersettingsfragment.xml #generated:48
-keep class com.facebook.widget.LoginButton { <init>(...); }

# view AndroidManifest.xml #generated:79
-keep class com.mixpanel.android.mpmetrics.GCMReceiver { <init>(...); }

# view res/layout/com_mixpanel_android_question_card.xml #generated:29
-keep class com.mixpanel.android.surveys.AlwaysSubmittableEditText { <init>(...); }

# view res/layout/com_mixpanel_android_activity_survey.xml #generated:48
-keep class com.mixpanel.android.surveys.CardCarouselLayout { <init>(...); }

# view res/layout/com_mixpanel_android_activity_survey.xml #generated:70
-keep class com.mixpanel.android.surveys.FadeOnPressButton { <init>(...); }

# view land/res/layout-land/com_mixpanel_android_activity_notification_full.xml #generated:30
# view res/layout/com_mixpanel_android_activity_notification_full.xml #generated:82
-keep class com.mixpanel.android.surveys.FadingImageView { <init>(...); }

# view res/layout/com_mixpanel_android_activity_notification_mini.xml #generated:10
-keep class com.mixpanel.android.surveys.MiniCircleImageView { <init>(...); }

# view res/layout/com_mixpanel_android_first_choice_answer.xml #generated:3
# view res/layout/com_mixpanel_android_last_choice_answer.xml #generated:3
# view res/layout/com_mixpanel_android_middle_choice_answer.xml #generated:3
-keep class com.mixpanel.android.surveys.SurveyChoiceView { <init>(...); }

# view AndroidManifest.xml #generated:94
-keep class jp.co.CAReward_Ack.CARAnalytics { <init>(...); }

# view AndroidManifest.xml #generated:97
-keep class jp.co.CAReward_Receiver.CARReceiver { <init>(...); }

# onClick res/layout/btn_cancel.xml #generated:3
# onClick res/layout/chart.xml #generated:25
-keepclassmembers class * { *** backPress(...); }

# onClick res/layout/com_mixpanel_android_activity_survey.xml #generated:70
-keepclassmembers class * { *** completeSurvey(...); }

# onClick res/layout/com_mixpanel_android_activity_survey.xml #generated:37
-keepclassmembers class * { *** goToNextQuestion(...); }

# onClick res/layout/com_mixpanel_android_activity_survey.xml #generated:18
-keepclassmembers class * { *** goToPreviousQuestion(...); }

